


create table article (id bigint not null auto_increment, contenu_html varchar(255) not null, contenu_raw varchar(255) not null, date_ecriture datetime not null, date_edition datetime, date_suppression datetime, est_publie bit not null, type_contenu_id bigint, utilisateur_id bigint, primary key (id)) engine=InnoDB
create table categorie (id bigint not null auto_increment, description varchar(255), titre varchar(50) not null, primary key (id)) engine=InnoDB
create table categorie_articles (categories_id bigint not null, articles_id bigint not null) engine=InnoDB
create table commentaire (id bigint not null auto_increment, contenu varchar(1000) not null, date_creation datetime, date_publication datetime, date_suppression datetime, article_id bigint, reaction_commentaire_id bigint, utilisateur_id bigint not null, primary key (id)) engine=InnoDB
create table commentaire_reactions (commentaire_id bigint not null, reactions_id bigint not null) engine=InnoDB
create table fichier (id bigint not null auto_increment, chemin_image varchar(255) not null, titre varchar(50) not null, article_id bigint, primary key (id)) engine=InnoDB
create table message (id bigint not null auto_increment, contenu_message varchar(4000) not null, date datetime, object_message varchar(100), destinataire_id bigint not null, expediteur_id bigint not null, message_id bigint, primary key (id)) engine=InnoDB
create table niveau (id bigint not null auto_increment, role varchar(50) not null, primary key (id)) engine=InnoDB
create table niveau_utilisateurs (roles_id bigint not null, utilisateurs_id bigint not null) engine=InnoDB
create table reference (id bigint not null auto_increment, description varchar(4000), lien varchar(255) not null, titre varchar(255) not null, type_reference_id bigint, primary key (id)) engine=InnoDB
create table reference_categories (references_id bigint not null, categories_id bigint not null) engine=InnoDB
create table type_contenu (id bigint not null auto_increment, titre varchar(50) not null, primary key (id)) engine=InnoDB
create table type_reference (id bigint not null auto_increment, titre varchar(50) not null, primary key (id)) engine=InnoDB
create table utilisateur (id bigint not null auto_increment, date_inscription datetime, date_suppression datetime, mail varchar(255) not null, mot_de_passe varchar(255), nom varchar(50) not null, prenom varchar(50) not null, pseudo varchar(50) not null, primary key (id)) engine=InnoDB
alter table commentaire_reactions add constraint UK_5dey1pakypb7hm14ue0oixotp unique (reactions_id)
alter table fichier add constraint UK_o4l5j1rqwx2uu5rr4s5inxwri unique (chemin_image)
alter table reference add constraint UK_1cavylo5e8x30y5uhaehgsmf8 unique (lien)
alter table type_contenu add constraint UK_onbsytbb4fm8xh98nfjlbrw2s unique (titre)
alter table type_reference add constraint UK_aglo3s9ahrss1i4dn3yp7sfp7 unique (titre)
alter table utilisateur add constraint UK_3b8egvpx5k4bp5h47su1mxdjx unique (mail)
alter table utilisateur add constraint UK_cuxn7vi5kxoby13xbh75cesdc unique (pseudo)
alter table article add constraint FK7wv9jm06etge8ukyhdaap7y1p foreign key (type_contenu_id) references type_contenu (id)
alter table article add constraint FKislt1qkplvds8id9a8ke0t7w7 foreign key (utilisateur_id) references utilisateur (id)
alter table categorie_articles add constraint FKo7y3mnwqghkbgiyc3lt1r759r foreign key (articles_id) references article (id)
alter table categorie_articles add constraint FKm8itpg9qqqjbf0cbyqb67wbu8 foreign key (categories_id) references categorie (id)
alter table commentaire add constraint FKpl8x2ccino75hr790mhtghbt9 foreign key (article_id) references article (id)
alter table commentaire add constraint FKq1uh65i70kixgrre75cm2k2om foreign key (reaction_commentaire_id) references commentaire (id)
alter table commentaire add constraint FKfkx1pegfdsd6e3cp2wblsc5jf foreign key (utilisateur_id) references utilisateur (id)
alter table commentaire_reactions add constraint FKi7jj3si4uuvv5myscxv1mlfw1 foreign key (reactions_id) references commentaire (id)
alter table commentaire_reactions add constraint FKqy1c286n8wv9mufklj4tlcj00 foreign key (commentaire_id) references commentaire (id)
alter table fichier add constraint FKj1itl8jvakcxyqmrq91bmp49u foreign key (article_id) references article (id)
alter table message add constraint FKcbrdql2960xjyuvcbt3k1vi1s foreign key (destinataire_id) references utilisateur (id)
alter table message add constraint FK3wmosdkp974i59tsduv2gntbx foreign key (expediteur_id) references utilisateur (id)
alter table message add constraint FKfjqrp63xfxi82xxsh8xpxl8pv foreign key (message_id) references message (id)
alter table niveau_utilisateurs add constraint FK2nnghi9ogngy9qetmmsbrusvg foreign key (utilisateurs_id) references utilisateur (id)
alter table niveau_utilisateurs add constraint FKmbyakhptab6qt0h7p7cd05j7o foreign key (roles_id) references niveau (id)
alter table reference add constraint FKnfuxyhodu0nirp5phr85ivpfh foreign key (type_reference_id) references type_reference (id)
alter table reference_categories add constraint FK8fa44hs0lrkhlb87y7q44quoe foreign key (categories_id) references categorie (id)
alter table reference_categories add constraint FK422gyg4ps06of7sjh1a9s72nn foreign key (references_id) references reference (id)
