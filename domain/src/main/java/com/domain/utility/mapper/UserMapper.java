package com.domain.utility.mapper;

import com.domain.dto.UserDto;
import com.domain.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {


    public UserDto forVisitor(User user) {
        return UserDto.builder()
                .id(user.getId())
                .pseudo(user.getPseudo())
                .totalPublishedWebContents(user.getTotalPublishedWebContents())
                .build();
    }


}
