package com.domain.utility.mapper;

import com.domain.dto.WebContentCategoryDto;
import com.domain.dto.WebContentDto;
import com.domain.dto.WebContentTypeDto;
import com.domain.entity.User;
import com.domain.entity.WebContent;
import com.domain.entity.WebContentCategory;
import com.domain.entity.WebContentType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
public class WebContentMapper {


    private final UserMapper userMapper;
    


    public WebContentDto forVisitor(WebContent webContent, User user, WebContentType type, List<WebContentCategory> categories) {

        return com.domain.dto.WebContentDto.builder()
                .id(webContent.getId())
                .versionId(webContent.getVersionId())
                .title(webContent.getTitle())
                .htmlContent(webContent.getHtmlContent())
                .creationDate(webContent.getCreationDate())
                .editionDate(webContent.getEditionDate())
                .publishingDate(webContent.getPublishingDate())
                .acceptsComments(webContent.isAcceptsComments())
                .totalPublishedComments(webContent.getTotalPublishedComments())
                .anonymousUser(webContent.isAnonymousUser())
                .user(userMapper.forVisitor(user))
                .type(WebContentTypeDto.builder()
                        .id(type.getId())
                        .versionId(type.getVersionId())
                        .title(type.getTitle())
                        .description(type.getDescription())
                        .totalPublishedWebContents(type.getTotalPublishedWebContents())
                        .build())
                .categories(categoriesForVisitor(categories))
                .build();
    }


    public WebContentDto searchedByKeywordForVisitor(WebContent webContent, User user, WebContentType type,
                                                     List<WebContentCategory> categories, int totalKeywordInstances) {

        WebContentDto webContentDto = forVisitor(webContent, user, type, categories);
        webContentDto.setTotalKeywordInstances(totalKeywordInstances);
        return webContentDto;
    }



   private List<WebContentCategoryDto> categoriesForVisitor(List<WebContentCategory> categories) {

        List<WebContentCategoryDto> categoryDtos = new ArrayList<>();

        categories.forEach(c -> categoryDtos.add(WebContentCategoryDto.builder()
                .id(c.getId())
                .versionId(c.getVersionId())
                .title(c.getTitle())
                .description(c.getDescription())
                .creationDate(c.getCreationDate())
                .editionDate(c.getEditionDate())
                .totalPublishedWebContents(c.getTotalPublishedWebContents())
                .build()));

        return categoryDtos;

    }


}
