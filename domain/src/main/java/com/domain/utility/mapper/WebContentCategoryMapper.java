package com.domain.utility.mapper;

import com.domain.dto.WebContentCategoryDto;
import com.domain.entity.WebContentCategory;
import org.springframework.stereotype.Component;

@Component
public class WebContentCategoryMapper {

    public WebContentCategoryDto forVisitor(WebContentCategory category) {

        return WebContentCategoryDto.builder()
                .id(category.getId())
                .versionId(category.getVersionId())
                .totalPublishedWebContents(category.getTotalPublishedWebContents())
                .title(category.getTitle())
                .description(category.getDescription())
                .creationDate(category.getCreationDate())
                .editionDate(category.getEditionDate())
                .build();
    }


}
