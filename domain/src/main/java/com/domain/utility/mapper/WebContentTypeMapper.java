package com.domain.utility.mapper;

import com.domain.dto.WebContentTypeDto;
import com.domain.entity.WebContentType;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class WebContentTypeMapper {

    public WebContentTypeDto forVisitor(WebContentType webContentType) {

        return WebContentTypeDto.builder()
                .id(webContentType.getId())
                .versionId(webContentType.getVersionId())
                .title(webContentType.getTitle())
                .description(webContentType.getDescription())
                .creationDate(webContentType.getCreationDate())
                .editionDate(webContentType.getEditionDate())
                .hasLimitedModification(webContentType.isHasLimitedModification())
                .isFooterItem(webContentType.isFooterItem())
                .isNavbarItem(webContentType.isNavbarItem())
                .totalPublishedWebContents(webContentType.getTotalPublishedWebContents())
                .build();
    }

    public WebContentTypeDto forVisitor(WebContentType webContentType, List<WebContentType> webContentTypeList) {

        WebContentTypeDto webContentTypeDto = forVisitor(webContentType);

        if (webContentTypeList != null && !webContentTypeList.isEmpty()) {
            List<WebContentTypeDto>webContentTypeDtos = webContentTypeList.stream()
                    .filter(t-> !t.isHasLimitedModification() && t.isPublished() && t.isInUse() && t.getSuppressionDate() == null)
                    .map(this::forVisitor)
                    .collect(Collectors.toList());
            webContentTypeDto.setChildrenTypes(webContentTypeDtos);

        }
        return webContentTypeDto;


    }


}
