package com.domain.utility.mapper;

import com.domain.dto.CommentDto;
import com.domain.dto.UserDto;
import com.domain.entity.Comment;
import com.domain.entity.User;
import org.springframework.stereotype.Component;

@Component
public class CommentMapper {


    public CommentDto dtoForVisitor(Comment comment, User user) {
        return CommentDto.builder()
                .id(comment.getId())
                .title(comment.getTitle())
                .text(comment.getText())
                .creationDate(comment.getCreationDate())
                .publishingDate(comment.getPublishingDate())
                .user(UserDto.builder()
                        .id(user.getId())
                        .pseudo(user.getPseudo())
                        .build())
                .build();
    }



}
