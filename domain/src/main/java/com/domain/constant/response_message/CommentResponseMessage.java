package com.domain.constant.response_message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CommentResponseMessage {


    COMMENT_NOT_FOUND("comment_not_found"),
    WEB_CONTENT_NOT_FOUND("web_content_not_found"),
    WEB_CONTENT_DOES_NOT_ACCEPT_COMMENTS("web_content_does_not_accept_comments"),
    INVALID_WEB_CONTENT_ID("invalid_web_content_id"),
    INVALID_COMMENT_ID("invalid_web_content_id"),
    PARENT_COMMENT_NOT_FOUND("parent_comment_not_found");

    private final String message;

}
