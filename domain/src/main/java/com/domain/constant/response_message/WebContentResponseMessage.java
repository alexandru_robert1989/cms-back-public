package com.domain.constant.response_message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum WebContentResponseMessage {


    NOT_FOUND("web_content_not_found"),
    ALREADY_DELETED("web_content_already_deleted"),
    SUPPRESSION_FAILED("web_content_suppression_failed");

    private final String message;

}
