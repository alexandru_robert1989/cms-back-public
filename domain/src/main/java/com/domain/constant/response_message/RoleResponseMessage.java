package com.domain.constant.response_message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RoleResponseMessage {


    NOT_FOUND("role_not_found");

    private final String message;

}
