package com.domain.constant.response_message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserResponseMessage {


    NOT_FOUND("user_not_found"),
    AUTHENTICATION_EXCEPTION("authentication_exception"),
    PASSWORD_MISMATCH("password_mismatch");

    private final String message;

}
