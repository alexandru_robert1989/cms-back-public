package com.domain.constant.response_message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CategoryResponseMessage {


    NOT_FOUND("category_not_found"),
    SAVING_FAILED("saving_category_failed"),
    ALREADY_DELETED("category_already_deleted"),
    DELETING_ONE_FAILED("deleting_category_failed");

    private final String message;

}
