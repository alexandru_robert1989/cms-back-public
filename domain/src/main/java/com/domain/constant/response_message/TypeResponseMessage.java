package com.domain.constant.response_message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TypeResponseMessage {


    NOT_FOUND("type_not_found"),
    SAVING_FAILED("saving_type_failed"),
    ALREADY_DELETED("type_already_deleted"),
    DELETING_ONE_FAILED("deleting_type_failed");

    private final String message;

}
