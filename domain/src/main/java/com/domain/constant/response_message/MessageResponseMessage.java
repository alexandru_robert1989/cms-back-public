package com.domain.constant.response_message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MessageResponseMessage {


    MESSAGING_EXCEPTION("messaging_exception");

    private final String message;

}
