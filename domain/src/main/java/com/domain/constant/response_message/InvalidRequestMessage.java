package com.domain.constant.response_message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum InvalidRequestMessage {


    INVALID_REQUEST("invalid_request");

    private final String message;

}
