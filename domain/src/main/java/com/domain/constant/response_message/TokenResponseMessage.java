package com.domain.constant.response_message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TokenResponseMessage {


    MISSING_TOKEN("missing_token"),
    INVALID_OR_EXPIRED_TOKEN("invalid_or_expired_token");

    private final String message;

}
