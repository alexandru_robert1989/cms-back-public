package com.domain.constant.response_message;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RequestSuccessMessage {

    SUCCESS_MESSAGE("success");

    private final String message;
}
