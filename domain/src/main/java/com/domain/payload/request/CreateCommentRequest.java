package com.domain.payload.request;


import com.domain.entity.WebContent;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateCommentRequest {


    /**
     * A mandatory {@link String} value representing the comment's title.
     */
    @NotBlank(message = "Merci de saisir le titre du commentaire")
    @Length(min = 2, max = 50, message = "Le titre peut avoir entre 2 et 50 caractères")
    private String title;


    /**
     * A mandatory {@link String} value representing the comment's  content.
     */
    @NotBlank(message = "Vous ne pouvez pas soumettre un commentaire vide")
    @Length(min = 2, max = 1000, message = "Le commentaire doit avoir entre 2 et 1000 caractères")
    private String text;


    /**
     * A mandatory self initiated {@link LocalDateTime} value, representing the  comment's creation date.
     */
    @NotNull(message = "Merci d'ajouter la date de création du commentaire")
    private LocalDateTime creationDate = LocalDateTime.now();


    /**
     * An optional {@link LocalDateTime} value, representing the comment's publication date.
     */
    private LocalDateTime publishingDate;


    /**
     * An optional {@link LocalDateTime} value, representing the comment's  suppression date.
     */
    private LocalDateTime suppressionDate;


    /**
     * The Long id value of the {@link com.domain.entity.User} that submitted the comment.
     */
    @NotNull(message = "Il manque l'id de l'utilisateur qui a soumis le commentaire")
    private Long userId;

    /**
     * The Long id value of the {@link WebContent} associated to the comment.
     */
    @NotNull(message = "Il manque l'id du contenu web associé au commentaire")
    private Long webContentId;

    /**
     * The Long id value of the comment's parent comment.
     */
    private Long parentCommentId;







}
