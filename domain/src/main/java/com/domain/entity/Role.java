package com.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A {@link Role} is used for
 * filtering {@link User} data access authorisations.
 * <p>
 * After having created a Role
 * using the default constructor, you need to set its title
 * before persisting it in a database.
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Role {


    /**
     * A mandatory {@link Long} value representing the {@link Role}'s unique identifier.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    /**
     * An optional  {@link Long} value, representing the {@link Role}'s version id. Several {@link Role}s
     * can have the same versionId, if they represent different versions of the same {@link Role}
     */
    private Long versionId;


    /**
     * boolean value indicating if the {@link Role} is the version in use
     */
    private boolean inUse;


    /**
     * A mandatory {@link String} value representing the title of the {@link Role}
     */
    @NotBlank(message = "Merci de saisir le nom du role")
    @Length(max = 100, message = "Un role peut avoir maximum 100 caractères")
    // TODO uncomment if necessary  @Column(unique = true)
    private String title;


    /**
     * A mandatory {@link String} value representing the description of the {@link Role}
     */
    @NotBlank(message = "Merci de fournir une description du role")
    @Length(min = 2, max = 500, message = "La description peut avoir minimum 2 et maximum 500 caractères")
    @Column(columnDefinition = "TEXT")
    private String description;


    /**
     * An optional {@link List} of the {@link User}s having this {@link Role}
     */
    @ToString.Exclude
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @Fetch(FetchMode.SELECT)
    @ManyToMany(fetch = FetchType.LAZY)
    private List<User> users;


    /**
     * A mandatory self initiated {@link LocalDateTime} value, representing the creation date of the {@link Role}
     */
    @NotNull(message = "Merci d'ajouter la date de création du Role")
    private LocalDateTime creationDate = LocalDateTime.now();


    /**
     * An optional {@link LocalDateTime} value, representing the last edition date of the {@link Role}
     */
    private LocalDateTime editionDate = creationDate;


    /**
     * An optional {@link LocalDateTime} value, representing the suppression date of the {@link Role}
     */
    LocalDateTime suppressionDate;



    private Integer totalUsers;
    private Integer totalNotDeletedUsers;
    private Integer totalDeletedUsers;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Role))
            return false;

        Role other = (Role) o;

        return id != null &&
                id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }


}
