package com.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * The WebContent class provides the properties
 * describing a WebContent that can be displayed online.
 * <p>
 * Before persisting a {@link WebContent}  created using the
 * default constructor, you have to set its following properties :
 * <p>
 * title ({@link String}), htmlContent ({@link String}), rawContent ({@link String}),
 * webContentType ({@link WebContentType}), the user ({@link User}).
 * Optionally, you can set the : versionId (long), the inUse (boolean),
 * submittedForPublication (boolean), published  (boolean), the publishingDate ({@link LocalDateTime})
 * if published is true, and the webContentCategories ({@link WebContentCategory}) {@link List} (if any).
 */

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WebContent {


    /**
     * A mandatory {@link Long} value representing the id of a instance stored in
     * a database.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * A  {@link Long} value representing the version id. Several {@link WebContent}s
     * can have the same versionId, if they represent different versions of the same
     * {@link WebContent}
     */
    private Long versionId;


    /**
     * boolean value indicating if the {@link WebContent} is the version in use
     */
    private boolean inUse;


    /**
     * A mandatory {@link String} value representing the title of the {@link WebContent}.
     */
    @NotBlank(message = "Merci d'ajouter le titre principal, qui représentera le nom générique de votre publication")
    @Length(min = 2, max = 255, message = "Le titre doit avoir entre 2 et 255 caractères")
    private String title;


    /**
     * A mandatory {@link String} value representing the html content of the {@link WebContent}.
     */
    @Column(columnDefinition = "LONGTEXT")
    @NotBlank(message = "Merci d'ajouter du contenu html pour pouvoir enregistrer")
    private String htmlContent;


    /**
     * A mandatory {@link String} value representing the raw content of the {@link WebContent}.
     */
    @JsonIgnore
    @Column(columnDefinition = "LONGTEXT ")
    @NotBlank(message = "Merci d'ajouter du texte pour pouvoir enregistrer")
    private String rawContent;


    /**
     * A mandatory self initiated {@link LocalDateTime} value, representing the creation date of the {@link WebContent}
     */
    @NotNull
    private final LocalDateTime creationDate = LocalDateTime.now();


    /**
     * A mandatory self initiated {@link LocalDateTime} value, representing the last edition date of the {@link WebContent}
     */
    private LocalDateTime editionDate = creationDate;


    /**
     * An optional  {@link LocalDateTime} value, representing the publication date of the {@link WebContent}
     */
    private LocalDateTime publishingDate;


    /**
     * An optional  {@link LocalDateTime} value, representing the suppression date of the {@link WebContent}
     */
    private LocalDateTime suppressionDate;


    /**
     * A mandatory by default false boolean valus indicating if the {@link WebContent} accepts {@link Comment}s.
     */
    @NotNull(message = "Merci de préciser si le type de contenu accepte des commentaires")
    private boolean acceptsComments = false;


    /**
     * A mandatory by default false boolean valus indicating if the {@link WebContent} is submitted for publication
     */
    @NotNull
    private boolean submittedForPublication = false;


    /**
     * A mandatory by default false boolean valus indicating if the {@link WebContent} is published
     */
    @NotNull
    private boolean published = false;


    /**
     * A boolean value indicating if the {@link WebContent} is displayed as belonging to a
     * anonymous {@link User}
     */
    private boolean anonymousUser;


    /**
     * A mandatory {@link User} value, representing the user that created the {@link WebContent}
     */
    @NotNull(message = "Le contenu doit être associé à un utilisateur")
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private User user;


    /**
     * An optional {@link List} of {@link WebContentCategory}es, representing the {@link WebContent}'s categories
     */
    @ToString.Exclude
    @JsonIgnore
    @Fetch(value = FetchMode.SELECT)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToMany(mappedBy = "webContents", fetch = FetchType.LAZY)
    private List<WebContentCategory> webContentCategories;


    /**
     * A mandatory {@link WebContentType} value, representing the {@link WebContent}'s type
     */
    @JsonIgnore
    @NotNull(message = "Merci de preciser le type de contenu")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private WebContentType webContentType;


    /**
     * An optional {@link List} of {@link Comment}s, representing the {@link WebContent}'s comments
     */
    @ToString.Exclude
    @JsonIgnore
    @Fetch(value = FetchMode.SELECT)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @OneToMany(mappedBy = "webContent", fetch = FetchType.LAZY)
    private List<Comment> comments;


   private Integer totalComments;
   private Integer totalPublishedComments;
   private Integer totalWaitingPublicationComments;
   private Integer totalDeletedComments;

    private boolean defaultDeletableContent;

    @Transient
    @JsonIgnore
    private Integer totalKeywordInstances;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WebContent))
            return false;
        WebContent other = (WebContent) o;
        return id != null &&
                id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }


}
