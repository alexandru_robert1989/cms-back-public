package com.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * The {@link Message} class provides the properties
 * of emails sent using the API.
 *
 * <p>
 * Before persisting a {@link Message} created with the default constructor,
 * you need to set its :
 * <p>
 * text, its sender ({@link User}) and its recipients ( a  list of {@link User}s).
 * Optionally, you can set its subject and its parentMessage (a {@link Message}, if any),
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Message {


    /**
     * A mandatory {@link Long} value representing the id of a instance stored in
     * a database.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    /**
     * A mandatory  self initiated {@link LocalDateTime} value, representing the creation date of the {@link Message}
     */
    @NotNull(message = "Le message doit avoir une date d'envoi")
    private final LocalDateTime creationDate = LocalDateTime.now();


    /**
     * An optional  {@link LocalDateTime} value, representing the suppression date of the {@link Comment}
     */
    private LocalDateTime suppressionDate;


    /**
     * An optional {@link String} value, representing the subject of the {@link Message}
     */
    @Length(max = 100, message = "L'objet du message peut avoir maximum 100 caractères")
    private String subject;


    /**
     * A mandatory {@link String} value, representing the text  content of the {@link Message}
     */
    @Length(min = 20, max = 4000, message = "Le message doit avoir entre 20 et 4000 caractères")
    @Column(columnDefinition = "TEXT")
    private String text;


    /**
     * An optional {@link Message} value, representing a parent {@link Message}
     */
    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    private Message parentMessage;


    /**
     * A mandatory {@link User} value, representing the {@link User} that sent the {@link Message}
     */
    @NotNull(message = "Un message doit avoir un expéditeur")
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private User sender;


    /**
     * A mandatory {@link List} of {@link User}s, representing the {@link Message} recipients
     */
    @ToString.Exclude
    @Fetch(value = FetchMode.SELECT)
    @NotEmpty(message = "Un message doit avoir au moins un destinataire")
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<User> recipients;

    private boolean defaultDeletableContent;



    /**
     * An optional transient {@link File} value, representing an email attachement
     */
    @Transient
    private List<File> files;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Message))
            return false;

        Message other = (Message) o;

        return id != null &&
                id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
