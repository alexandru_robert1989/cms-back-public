package com.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;


/**
 * The {@link Comment} class provides the properties of
 * a comment submitted for a {@link WebContent} or as a reaction
 * to another comment.
 * <p>
 * After having created  a {@link Comment} using the default
 * constructor, before recording it you need to set its following properties:
 * <p>
 * title ({@link String}), text ({@link String}), user ({@link User}),webContent {@link WebContent}.
 * Optionally, you can also set  its parentComment (a {@link Comment}, if any).
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "A Web Content's comment")
public class Comment {

    /**
     * A mandatory auto generated {@link Long} value representing the comment's unique id.
     */
    @ApiModelProperty(notes = "A Long value representing the comment's unique id", example = "1L")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    /**
     * A mandatory {@link String} value representing the title of the {@link Comment}
     */
    @ApiModelProperty(notes = "The title of the comment ", example = "Nice article ...", required = true)
    @NotBlank(message = "Merci de saisir le titre du commentaire")
    @Length(min = 2, max = 50, message = "Le titre peut avoir entre 2 et 50 caractères")
    @Column(columnDefinition = "TEXT")
    private String title;


    /**
     * A mandatory {@link String} value representing the content of the {@link Comment}
     */
    @ApiModelProperty(notes = "The content of a comment ", example = "I really liked ...", required = true)
    @NotBlank(message = "Vous ne pouvez pas soumettre un commentaire vide")
    @Length(min = 2, max = 1000, message = "Le commentaire doit avoir entre 2 et 1000 caractères")
    @Column(columnDefinition = "TEXT")
    private String text;


    /**
     * A mandatory self initiated {@link LocalDateTime} value, representing the creation date of the {@link Comment}
     */
    @ApiModelProperty(notes = "The comment creation date time, created by default", example = "2021-07-23T17:28:38.015Z")
    @NotNull(message = "Merci d'ajouter la date de création du commentaire")
    private LocalDateTime creationDate = LocalDateTime.now();


    /**
     * An optional {@link LocalDateTime} value, representing the publication date of the {@link Comment}
     */
    @ApiModelProperty(notes = "The comment publishing date", example = "2021-07-23T17:28:38.015Z")
    private LocalDateTime publishingDate;


    /**
     * An optional {@link LocalDateTime} value, representing the suppression date of the {@link Comment}
     */
    @ApiModelProperty(notes = "The comment suppression date", example = "2021-07-23T17:28:38.015Z")
    private LocalDateTime suppressionDate;


    /**
     * A mandatory {@link User} value, representing the user that submitted the {@link Comment}
     */
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull(message = "Un commentaire doit avoir un utilisateur")
    @JoinColumn
    private User user;


    /**
     * An optional {@link Comment} value, representing a parent {@link Comment}
     */
    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    private Comment parentComment;

    /**
     * A mandatory {@link WebContent} value, representing the {@link WebContent} of the {@link Comment}
     */
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull(message = "Un commentaire doit être associé à une publication")
    @JoinColumn
    private WebContent webContent;

    private boolean defaultDeletableContent;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Comment))
            return false;

        Comment other = (Comment) o;

        return id != null &&
                id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
