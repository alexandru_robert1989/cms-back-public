package com.domain.entity;

import lombok.*;


/**
 * The {@link File} class provides the properties of
 * the files used as email attachments.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class File {

    /**
     * A mandatory {@link String} value representing the path of a file
     */
    private String filePath;

    /**
     * A mandatory {@link Message} value, representing the {@link Message}
     * that will contain the {@link File}
     */
    private Message message;


    @Override
    public String toString() {
        return "File{" +
                "filePath='" + filePath + '\'' +
                ", message=" + message +
                '}';
    }
}
