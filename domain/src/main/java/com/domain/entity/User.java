package com.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.List;

/**
 * The User class provides the properties of a
 * {@link User}.
 * <p>
 * Getters, setters, the default constructor, the all arguments constructor,
 * a builder, the toString and  equals and the hashCode methods were implemented implicitly using
 * {@link Lombok} annotations.
 * <p>
 * After having created a {@link User} using the default constructor,
 * before persisting it you have to set its following properties:
 * firstName ({@link String}), lastName ({@link String}),
 * pseudo ({@link String}), email ({@link String}), password ({@link String}) and getNewsletter (boolean).
 * It is indicated to also set the roles ({@link Role}s {@link List}) property.
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "A user")
public class User {


    /**
     * A mandatory {@link Long} value representing the id of a instance stored in
     * a database.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /*Regex used for firstName and lastName:
     ^(^[\s]*[A-Za-zÀ-ÿ]{1,20}([\s]*[( '-][A-Za-zÀ-ÿ]{1,20}){0,6}[\s]*)
    1 - ^[\s]* - can start with 0 or several whitespaces
    2 - [A-Za-zÀ-ÿ]{1,20} - followed by 1 to 20 letters, including special characters letters (À-ÿ)
    3 -([\s]*[( '-] - followed by 0 or more white spaces or by one - or '
    4 - [A-Za-zÀ-ÿ]{1,20}) - followed (again) by 1 to 20 letters, including special characters letters (À-ÿ)
    5 - {0,6} - the 1 to 6 input can be repeated up to 4 times
    6 - [\s]* - can finish with 0 or more white spaces

    Example of valid firstName or lastName : François, Da Silva, D'Artagnan, Marie-Pierre


    Regex used for the pseudo:
    ^(^[\s]*([a-zA-Z]){1,}([-_.]?[a-zA-Z\d]*)?[\s]*){1,2}$
    1 - ^(^[\s]* - can start with 0 or several whitespaces
    2 - ([a-zA-Z]){3,} - followed by minimum 1 letter
    3 - ([-_.]? - followed by 0 or one -, - or .
    4 - [a-zA-Z\d]* - followed by 0 or more letters
    5 - * - the 2 to 4 inputs can be repeated 0 or several times
    6 - {1,2}$ - the 1 to 5 inputs can be repeated maximum 2 times

     */

    /**
     * A mandatory {@link String} value, representing the {@link User}'s first name.
     * <p>
     * Regex used for firstName and lastName:
     * ^(^[\s]*[A-Za-zÀ-ÿ]{1,20}([\s]*[( '-][A-Za-zÀ-ÿ]{1,20}){0,6}[\s]*)
     * 1 - ^[\s]* - can start with 0 or several whitespaces
     * 2 - [A-Za-zÀ-ÿ]{1,20} - followed by 1 to 20 letters, including special characters letters (À-ÿ)
     * 3 -([\s]*[( '-] - followed by 0 or more white spaces or by one - or '
     * 4 - [A-Za-zÀ-ÿ]{1,20}) - followed (again) by 1 to 20 letters, including special characters letters (À-ÿ)
     * 5 - {0,6} - the 1 to 6 input can be repeated up to 4 times
     * 6 - [\s]* - can finish with 0 or more white spaces
     * <p>
     * Example of valid firstName or lastName : François, Da Silva, D'Artagnan, Marie-Pierre
     */
    @Pattern(regexp = "^(^[\\s]*[A-Za-zÀ-ÿ]{1,20}([\\s]*[( '-][A-Za-zÀ-ÿ]{1,20}){0,6}[\\s]*)",
            message = "\n" +
                    "Format invalide (exemple formats acceptés:  François, Da Silva, D'Artagnan, Marie-Pierre)\n" +
                    "Votre prénom peut contenir seulement des mots composés par des lettres,\n" +
                    "séparés par UN SEUL apostrophe ( ' ), par UN SEUL trait d'union ( - ) ou par des espaces")
    @Length(min = 2, max = 60, message = "Votre prénom doit avoir entre 2 et 60 caractères")
    @NotBlank(message = "Merci d'ajouter votre prénom")
    private String firstName;


    /**
     * A mandatory {@link String} value, representing the {@link User}'s last name.
     * <p>
     * Regex used for firstName and lastName:
     * ^(^[\s]*[A-Za-zÀ-ÿ]{1,20}([\s]*[( '-][A-Za-zÀ-ÿ]{1,20}){0,6}[\s]*)
     * 1 - ^[\s]* - can start with 0 or several whitespaces
     * 2 - [A-Za-zÀ-ÿ]{1,20} - followed by 1 to 20 letters, including special characters letters (À-ÿ)
     * 3 -([\s]*[( '-] - followed by 0 or more white spaces or by one - or '
     * 4 - [A-Za-zÀ-ÿ]{1,20}) - followed (again) by 1 to 20 letters, including special characters letters (À-ÿ)
     * 5 - {0,6} - the 1 to 6 input can be repeated up to 4 times
     * 6 - [\s]* - can finish with 0 or more white spaces
     * <p>
     * Example of valid firstName or lastName : François, Da Silva, D'Artagnan, Marie-Pierre
     */
    @Pattern(regexp = "^(^[\\s]*[A-Za-zÀ-ÿ]{1,20}([\\s]*[( '-][A-Za-zÀ-ÿ]{1,20}){0,6}[\\s]*)",
            message = "\n" +
                    "Format invalide (exemple formats acceptés:  François, Da Silva, D'Artagnan, Marie-Pierre)\n" +
                    "Votre nom peut contenir seulement des mots composés par des lettres,\n" +
                    "séparés par UN SEUL apostrophe ( ' ), par UN SEUL trait d'union ( - ) ou par des espaces")
    @Length(min = 2, max = 60, message = "Votre nom doit avoir entre 2 et 60 caractères")
    @NotBlank(message = "Merci d'ajouter votre nom")
    private String lastName;


    /**
     * A mandatory {@link String} value, representing the {@link User}'s pseudo.
     * <p>
     * Regex used for the pseudo:
     * ^(^[\s]*([a-zA-Z]){1,}([-_.]?[a-zA-Z\d]*)?[\s]*){1,2}$
     * 1 - ^(^[\s]* - can start with 0 or several whitespaces
     * 2 - ([a-zA-Z]){3,} - followed by minimum 1 letter
     * 3 - ([-_.]? - followed by 0 or one -, - or .
     * 4 - [a-zA-Z\d]* - followed by 0 or more letters
     * 5 - * - the 2 to 4 inputs can be repeated 0 or several times
     * 6 - {1,2}$ - the 1 to 5 inputs can be repeated maximum 2 times
     */
    @Pattern(regexp = "^(^[\\s]*([a-zA-Z]){1,}([-_.]?[a-zA-Z\\d]*)?[\\s]*){1,2}$",
            message = " Format invalide (exemple formats acceptés: jean.jean, the_pseudo, pseudo-pse22 )\n" +
                    "Le pseudo peut doit commencer par minimum 1 lettre et peut contenir des lettres sans accents" +
                    "et des chiffres, séparés par une seule instance de -, . ou _ ")
    @Length(min = 3, max = 50, message = "Votre pseudo doit avoir entre 3 et 50 caractères")
    @Column(unique = true)
    @NotBlank(message = "Merci d'ajouter un pseudo")
    private String pseudo;


    /**
     * A mandatory {@link String} value, representing the {@link User}'s email address.
     */
    @Email(message = "Le format de l'adresse mail n'est pas valide.")
    @Column(unique = true)
    @NotNull(message = "Merci d'ajouter l'adresse mail")
    private String email;


    /**
     * A mandatory {@link String} value, representing the {@link User}'s password.
     */
    @NotBlank(message = "Merci d'ajouter le mot de passe")
    private String password;


    /**
     * A mandatory {@link LocalDateTime} value, representing the {@link User}'s sign up date.
     * The date value is automatically initiated on object creation.
     */
    @NotNull(message = "Merci d'ajouter la date d'inscription")
    private LocalDateTime signUpDate = LocalDateTime.now();


    /**
     * A mandatory boolean value indicating if the {@link User}'s profile image is validated.
     * By default, this value is false.
     */
    @NotNull
    private boolean profileImageValidated = false;


    /**
     * A mandatory boolean value indicating if the {@link User}'s email address is validated.
     * By default, this value is false.
     */
    @NotNull
    private boolean emailValidated = false;


    /**
     * A mandatory boolean value indicating if the {@link User} subscribed to the newsletter.
     * By default, this value is false.
     */
    @NotNull
    private boolean getNewsletter = false;


    /**
     * An optional boolean value indicating if the {@link User} has suppressed its account.
     */
    private boolean selfDeleted;


    /**
     * An optional {@link LocalDateTime} value, indicating the {@link User}'s account suppression date.
     */
    private LocalDateTime suppressionDate;


    /**
     * An optional {@link List} of {@link WebContent}s, representing all the {@link WebContent}s
     * created by the {@link User}.
     */
    @JsonIgnore
    @ToString.Exclude
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @Fetch(FetchMode.SELECT)
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<WebContent> webContents;


    /**
     * An optional {@link List} of {@link Comment}s, representing all the {@link Comment}s
     * of the {@link User}.
     */
    @ToString.Exclude
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @Fetch(FetchMode.SELECT)
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Comment> comments;


    /**
     * An optional {@link List} of {@link Token}s, representing all the {@link Token}s
     * of the {@link User}.
     */
    @ToString.Exclude
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @Fetch(FetchMode.SELECT)
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Token> tokens;


    /**
     * A mandatory {@link List} of {@link Role}s, representing all the {@link Role}s
     * of the {@link User}.
     */
    @NotNull(message = "Un utilisateur doit avoir une liste de roles")
    @JsonIgnore
    @ToString.Exclude
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @Fetch(FetchMode.SELECT)
    @ManyToMany(mappedBy = "users", fetch = FetchType.LAZY)
    private List<Role> roles;


    /**
     * An optional {@link List} of {@link Message}s, representing all the {@link Message}s
     * received by the {@link User}.
     */
    @ToString.Exclude
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @Fetch(FetchMode.SELECT)
    @ManyToMany(mappedBy = "recipients", fetch = FetchType.LAZY)
    private List<Message> receivedMessages;


    /**
     * An optional {@link List} of {@link Message}s, representing all the {@link Message}s
     * sent by the {@link User}.
     */
    @ToString.Exclude
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @Fetch(FetchMode.SELECT)
    @OneToMany(mappedBy = "sender", fetch = FetchType.LAZY)
    private List<Message> sentMessages;



    private Integer  totalWebContents;
    private Integer totalPublishedWebContents;
    private Integer totalWaitingPublicationWebContents;
    private Integer totalNotSubmittedForPublicationWebContents;
    private Integer totalDeletedWebContents;

    private Integer totalSentComments;
    private Integer totalPublishedComments;
    private Integer totalWaitingPublicationComments;
    private Integer totalDeletedComments;

    private boolean defaultDeletableContent;



    /**
     * Transient int value representing the total found
     * {@link WebContent}s created by this {@link User}
     */
    @Transient
    int totalFoundWebContents;


    /**
     * Method formatting a String input to adapt it to the regex used for the {@link User}'s
     * first names and last names.
     *
     * @param input a {@link String} value
     * @return a {@link String} value
     */
    @JsonIgnore
    public static String formatFirstOrLastName(String input) {
        StringBuilder nomSb = new StringBuilder(input.trim().replaceAll("\\s+", " ").toLowerCase());
        nomSb.replace(0, 1, nomSb.substring(0, 1).toUpperCase());

        for (int i = 1; i < nomSb.length(); i++) {
            if (nomSb.substring(i, i + 1).equals("'") || nomSb.substring(i, i + 1).equals("-")
                    || nomSb.substring(i, i + 1).equals(" ")
            ) {
                nomSb.replace(i + 1, i + 2, nomSb.substring(i + 1, i + 2).toUpperCase());
            }
        }
        return nomSb.toString();
    }


    /**
     * Method formatting a String input to adapt it to the regex used for
     * the {@link User}'s pseudo value
     *
     * @param input a {@link String} value
     * @return a {@link String} value
     */
    @JsonIgnore
    public static String formatPseudo(String input) {
        return input.trim().replaceAll("\\s+", "");
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User))
            return false;
        User other = (User) o;
        return id != null &&
                id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }


}
