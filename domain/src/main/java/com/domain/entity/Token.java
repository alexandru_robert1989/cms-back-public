package com.domain.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Objects;


/**
 * The {@link Token} class provides a String token value used
 * for validating a {@link User}'s account or for
 * reinitialising its password.
 * <p>
 * Before trying to persist a token created with the constructor
 * demanding a {@link User}, it is recommended to check if its
 * {@link String} property token already exists in the database.
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Token {

    /**
     * Transient int value representing the {@link Token}'s validity in hours
     */
    @Transient
    private static final int VALIDITY_IN_HOURS = 24;


    /**
     * Transient {@link SecureRandom} value used for generating a random
     * chars array.
     */
    @Transient
    private static final SecureRandom SECURE_RANDOM = new SecureRandom();


    /**
     * Transient {@link Base64.Encoder} value used for generating a random
     * chars array.
     */
    @Transient
    private static final Base64.Encoder BASE64_ENCODER = Base64.getUrlEncoder();


    /**
     * Transient int value used for generating a random
     * chars array.
     */
    @Transient
    private static final int BYTES_ENCODAGE = 24;


    /**
     * A mandatory {@link Long} value representing the id of a instance stored in
     * a database.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    /**
     * A mandatory {@link String} value representing the token's text value.*
     * The value is self generated using the generateToken() method of the class.
     */
    @NotBlank(message = "Merci de fournir la valeur du token")
    @Column(nullable = false)
    @Length(max = 255, message = "Le token peut avoir maximum 255 caractères")
    private String token = generateToken();


    /**
     * A mandatory {@link LocalDateTime} value representing the {@link Token}'s
     * expiration date. This property is self initiated using the VALIDITY_IN_HOURS
     * property of the class.
     */
    @NotNull(message = "Merci de fournir la date d'expiration du token")
    @Column(nullable = false)
    private LocalDateTime expirationDate = LocalDateTime.now().plusHours(VALIDITY_IN_HOURS);


    /**
     * A mandatory {@link User} value representing the user to whom the {@link Token}
     * is associated.
     */
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull(message = "Le token doit  être associé à un utilisateur")
    private User user;

    /**
     * This method returns a 32 chars random String that wil be used for
     * setting the {@link Token}'s token value.
     *
     * @return the token String value
     */
    public static String generateToken() {
        byte[] randomBytes = new byte[BYTES_ENCODAGE];
        SECURE_RANDOM.nextBytes(randomBytes);
        return BASE64_ENCODER.encodeToString(randomBytes);
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Token))
            return false;

        Token other = (Token) o;

        return id != null &&
                id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }



}
