package com.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;


/**
 * The {@link WebContentType} provides the properties
 * describing a {@link WebContent}'s type.
 * <p>
 * Before persisting a {@link WebContentType} created with
 * the default constructor, you need to set its following
 * properties : title ({@link String}), description ({@link String}).
 * isNavbarItem (boolean) , isFooterItem (boolean).
 * Optionally, you can set the: published (boolean) property, versionId and inUse properties.
 */

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WebContentType {


    /**
     * A mandatory {@link Long} value representing the id of a instance stored in
     * a database.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    /**
     * A  {@link Long} value representing the version id. Several {@link WebContentType}s
     * can have the same versionId, if they represent different versions of the same
     * {@link WebContentType}
     */
    private Long versionId;


    /**
     * boolean value indicating if the {@link WebContentType} is the version in use
     */
    private boolean inUse;


    /**
     * A mandatory by default false boolean value indicating if the {@link WebContentType} is published
     */
    @NotNull
    private boolean published = false;


    /**
     * A mandatory {@link String} value representing the title of the {@link WebContentType}
     */
    @NotBlank(message = "Merci de saisir le titre du type de contenu")
    @Length(min = 2, max = 50, message = "Le titre peut avoir entre 2 et 50 caractères")
    @Column(columnDefinition = "TEXT")
    private String title;


    /**
     * A mandatory {@link String} value representing the description of the {@link WebContentType}
     */
    @NotBlank(message = "Merci de fournir une description du type de contenu")
    @Length(min = 2, max = 500, message = "La description doit avoir entre 2 et 500 caractères")
    @Column(columnDefinition = "TEXT")
    private String description;


    /**
     * A mandatory boolean value indicating if the {@link WebContentType} is found in the navbar
     */
    @NotNull(message = "Merci de préciser si le type de contenu est dans le navbar")
    private boolean isNavbarItem;


    /**
     * A mandatory boolean value indicating if the {@link WebContentType} is found in the footer
     */
    @NotNull(message = "Merci de préciser si le type de contenu est dans le footer")
    private boolean isFooterItem;


    /**
     * A mandatory self initiated {@link LocalDateTime} value, representing the creation date of the {@link WebContentType}
     */
    @NotNull
    LocalDateTime creationDate = LocalDateTime.now();


    /**
     * An optional  {@link LocalDateTime} value, representing the last edition date of the {{@link WebContentType}
     */
    LocalDateTime editionDate = this.creationDate;


    /**
     * An optional  {@link LocalDateTime} value, representing the suppression date of the {@link WebContentType}
     */
    LocalDateTime suppressionDate;


    /**
     * A mandatory by default false boolean value indicating if the {@link WebContentType}  has a limited modifications option
     */
    @NotNull
    private boolean hasLimitedModification = false;


    /**
     * An optional {@link List} of {@link WebContent}s representing all the web contents associated to the {@link WebContentType}
     */
    @ToString.Exclude
    @JsonIgnore
    @Fetch(value = FetchMode.SELECT)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @OneToMany(mappedBy = "webContentType", fetch = FetchType.LAZY)
    private List<WebContent> webContents;


    /**
     * An optional {@link WebContentType} value, representing a {@link WebContentType}'s parent
     */
    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    private WebContentType webContentType;


    private Integer  totalWebContents;
    private Integer totalPublishedWebContents;
    private Integer totalWaitingPublicationWebContents;
    private Integer totalNotSubmittedForPublicationWebContents;
    private Integer totalDeletedWebContents;

    private boolean defaultDeletableContent;



    /**
     * Transient int value representing the total found
     * {@link WebContent}s associated to this {@link WebContentType}
     */
    @Transient
    int totalFoundWebContents;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WebContentType))
            return false;
        WebContentType other = (WebContentType) o;
        return id != null &&
                id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }



}
