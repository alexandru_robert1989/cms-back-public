package com.domain.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * The {@link WebContentCategory} provides the properties
 * describing a {@link WebContent}'s category.
 * <p>
 * Before persisting a {@link WebContentCategory} created with
 * the default constructor, you need to set its title ({@link String}),
 * its description ({@link String}) and its inUse (boolean) properties.
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "A Web Content's category")
public class WebContentCategory {


    /**
     * A mandatory {@link Long} value representing the id of a instance stored in
     * a database.
     */
    @ApiModelProperty(notes = "A Long value representing the category's unique id", example = "1L")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    /**
     * A  {@link Long} value representing the version id. Several {@link WebContentCategory}es
     * can have the same versionId, if they represent different versions of the same
     * {@link WebContentCategory}
     */
    @ApiModelProperty(notes = "A Long value representing the category's version id," +
            " shared by all the versions of the same category", example = "1L")
    private Long versionId;


    /**
     * boolean value indicating if the {@link WebContentCategory} is the version in use
     */
    @ApiModelProperty(notes = "A boolean value indicating if the category is the version in use ", example = "true")
    @NotNull(message = "Merci de préciser si la Catégorie est utilisée comme version principale")
    private boolean inUse;


    /**
     * A mandatory by default false boolean value indicating if the {@link WebContentCategory} is published
     */
    @ApiModelProperty(notes = "A boolean value indicating if the category is published ", example = "true")
    @NotNull(message = "Merci de préciser si la Catégorie est publiée")
    private boolean published;


    /**
     * A mandatory {@link String} value representing the title of the {@link WebContentCategory}
     */
    @ApiModelProperty(notes = "The title of the category", example = "Science", required = true)
    @NotBlank(message = "Merci de saisir le titre de la catégorie de contenu")
    @Length(min = 2, max = 50, message = "Le titre peut avoir entre 2 et 50 caractères")
    @Column(columnDefinition = "TEXT")
    private String title;


    /**
     * A mandatory {@link String} value representing the description of the {@link WebContentCategory}
     */
    @ApiModelProperty(notes = "The description of the category", example = "Category including scientific articles", required = true)
    @NotBlank(message = "Merci de fournir une description de la catégorie")
    @Length(min = 2, max = 500, message = "La description doit avoir entre 2 et 500 caractères")
    @Column(columnDefinition = "TEXT")
    private String description;


    /**
     * A self initiated {@link LocalDateTime} value, representing the creation date of the {@link WebContentCategory}
     */
    @ApiModelProperty(notes = "The category's creation date time", example = "2021-07-23T17:28:38.015Z")
    @NotNull(message = "Merci d'ajouter la date de création de la catégorie")
    LocalDateTime creationDate = LocalDateTime.now();


    /**
     * An optional {@link LocalDateTime} value, representing the last edition date of the {@link WebContentCategory}
     */
    @ApiModelProperty(notes = "The category's last edition date time", example = "2021-07-23T17:28:38.015Z")
    LocalDateTime editionDate;


    /**
     * An optional {@link LocalDateTime} value, representing the suppression date of the {@link WebContentCategory}
     */
    @ApiModelProperty(notes = "The category's suppression date time", example = "2021-07-23T17:28:38.015Z")
    LocalDateTime suppressionDate;


    /**
     * An optional {@link WebContent}s {@link List}, representing the web contents associated to the {@link WebContentCategory}
     */
    @ToString.Exclude
    @JsonIgnore
    @Fetch(value = FetchMode.SELECT)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<WebContent> webContents;


    private Integer totalWebContents;
    private Integer totalPublishedWebContents;
    private Integer totalWaitingPublicationWebContents;
    private Integer totalNotSubmittedForPublicationWebContents;
    private Integer totalDeletedWebContents;

    private boolean defaultDeletableContent;



    /**
     * Transient int value representing the total found
     * {@link WebContent}s associated to this {@link WebContentType}
     */
    @Transient
    int totalFoundWebContents;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof WebContentCategory))
            return false;

        WebContentCategory other = (WebContentCategory) o;

        return id != null &&
                id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }


}
