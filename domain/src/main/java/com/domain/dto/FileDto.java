package com.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;



@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileDto {


    private String filePath;

    private MessageDto message;


}
