package com.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommentDto {

    private Long id;

    @NotBlank(message = "Merci de saisir le titre du commentaire")
    @Length(min = 2, max = 50, message = "Le titre peut avoir entre 2 et 50 caractères")
    private String title;

    @NotBlank(message = "Vous ne pouvez pas soumettre un commentaire vide")
    @Length(min = 2, max = 1000, message = "Le commentaire doit avoir entre 2 et 1000 caractères")
    private String text;

    @NotNull(message = "Merci d'ajouter la date de création du commentaire")
    private LocalDateTime creationDate;

    private LocalDateTime publishingDate;

    private LocalDateTime suppressionDate;

    @NotNull(message = "Un commentaire doit avoir un utilisateur")
    private UserDto user;

    private CommentDto parentComment;

    @NotNull(message = "Un commentaire doit être associé à une publication")
    private WebContentDto webContent;

    @ToString.Exclude
    @JsonIgnore
    List<CommentDto> childrenComments;

    private boolean defaultDeletableContent;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentDto that = (CommentDto) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
