package com.domain.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;


@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TokenDto {

    private Long id;

    @NotBlank(message = "Merci de fournir la valeur du token")
    @Length(max = 255, message = "Le token peut avoir maximum 255 caractères")
    private String token ;

    @NotNull(message = "Merci de fournir la date d'expiration du token")
    private LocalDateTime expirationDate ;

    @NotNull(message = "Le token doit  être associé à un utilisateur")
    private UserDto user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TokenDto tokenDto = (TokenDto) o;
        return Objects.equals(id, tokenDto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
