package com.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;


@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {

    private Long id;

    @Length(min = 2, max = 60, message = "Votre prénom doit avoir entre 2 et 60 caractères")
    @NotBlank(message = "Merci d'ajouter votre prénom")
    private String firstName;

    @Length(min = 2, max = 60, message = "Votre nom doit avoir entre 2 et 60 caractères")
    @NotBlank(message = "Merci d'ajouter votre nom")
    private String lastName;

    @Length(min = 3, max = 50, message = "Votre pseudo doit avoir entre 3 et 50 caractères")
    @NotBlank(message = "Merci d'ajouter un pseudo")
    private String pseudo;

    @Email(message = "Le format de l'adresse mail n'est pas valide.")
    @NotNull(message = "Merci d'ajouter l'adresse mail")
    private String email;

    @NotBlank(message = "Merci d'ajouter le mot de passe")
    private String password;

    @NotNull(message = "Merci d'ajouter la date d'inscription")
    private LocalDateTime signUpDate;

    @NotNull
    private Boolean profileImageValidated;

    @NotNull
    private Boolean emailValidated;

    @NotNull
    private Boolean getNewsletter;

    private Boolean selfDeleted;

    private LocalDateTime suppressionDate;

    @ToString.Exclude
    @JsonIgnore
    private List<WebContentDto> webContents;

    @JsonIgnore
    @ToString.Exclude
    private List<CommentDto> comments;

    @ToString.Exclude
    @JsonIgnore
    private List<TokenDto> tokens;

    @ToString.Exclude
    private List<RoleDto> roles;

    @ToString.Exclude
    @JsonIgnore
    private List<MessageDto> receivedMessages;

    @ToString.Exclude
    @JsonIgnore
    private List<MessageDto> sentMessages;

    private Integer totalWebContents;
    private Integer totalPublishedWebContents;
    private Integer totalWaitingPublicationWebContents;
    private Integer totalDeletedWebContents;

    private Integer totalSentComments;
    private Integer totalPublishedComments;
    private Integer totalWaitingPublicationComments;
    private Integer totalDeletedComments;

    private Integer totalFoundWebContents;

    private boolean defaultDeletableContent;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto userDto = (UserDto) o;
        return Objects.equals(id, userDto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
