package com.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;


@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleDto {

    private Long id;

    private Long versionId;

    private Boolean inUse;

    @NotBlank(message = "Merci de saisir le nom du role")
    @Length(max = 100, message = "Un role peut avoir maximum 100 caractères")
    private String title;

    @NotBlank(message = "Merci de fournir une description du role")
    @Length(min = 2, max = 500, message = "La description peut avoir minimum 2 et maximum 500 caractères")
    private String description;

    @ToString.Exclude
    @JsonIgnore
    private List<UserDto> users;

    @NotNull(message = "Merci d'ajouter la date de création du Role")
    private LocalDateTime creationDate ;

    private LocalDateTime editionDate;

    LocalDateTime suppressionDate;

    private Integer totalUsers;
    private Integer totalNotDeletedUsers;
    private Integer totalDeletedUsers;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleDto roleDto = (RoleDto) o;
        return Objects.equals(id, roleDto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
