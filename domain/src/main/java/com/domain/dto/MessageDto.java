package com.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;


@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageDto {

    private Long id;

    @NotNull(message = "Le message doit avoir une date d'envoi")
    private  LocalDateTime creationDate ;

    private LocalDateTime suppressionDate;

    @Length(max = 100, message = "L'objet du message peut avoir maximum 100 caractères")
    private String subject;

    @Length(min = 20, max = 4000, message = "Le message doit avoir entre 20 et 4000 caractères")
    private String text;

    private MessageDto parentMessage;

    @NotNull(message = "Un message doit avoir un expéditeur")
    private UserDto sender;

    @ToString.Exclude
    @JsonIgnore
    @NotEmpty(message = "Un message doit avoir au moins un destinataire")
    private List<UserDto> recipients;

    @ToString.Exclude
    private List<FileDto> files;

    private boolean defaultDeletableContent;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageDto that = (MessageDto) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
