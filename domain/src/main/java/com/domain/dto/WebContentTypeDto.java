package com.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;


@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WebContentTypeDto {

    private Long id;

    private Long versionId;

    private Boolean inUse;

    @NotNull
    private Boolean published ;

    @NotBlank(message = "Merci de saisir le titre du type de contenu")
    @Length(min = 2, max = 50, message = "Le titre peut avoir entre 2 et 50 caractères")
    private String title;

    @NotBlank(message = "Merci de fournir une description du type de contenu")
    @Length(min = 2, max = 500, message = "La description doit avoir entre 2 et 500 caractères")
    private String description;

    @NotNull(message = "Merci de préciser si le type de contenu est dans le navbar")
    private Boolean isNavbarItem;

    @NotNull(message = "Merci de préciser si le type de contenu est dans le footer")
    private Boolean isFooterItem;

    @NotNull
    LocalDateTime creationDate ;

    LocalDateTime editionDate ;

    LocalDateTime suppressionDate;

    @NotNull
    private Boolean hasLimitedModification ;

    @ToString.Exclude
    @JsonIgnore
    private List<WebContentDto> webContents;

    private WebContentTypeDto webContentType;

    private Integer  totalWebContents;
    private Integer totalPublishedWebContents;
    private Integer totalDeletedWebContents;


    Integer totalFoundWebContents;

    @ToString.Exclude
    List<WebContentTypeDto> childrenTypes;

    private boolean defaultDeletableContent;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WebContentTypeDto that = (WebContentTypeDto) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
