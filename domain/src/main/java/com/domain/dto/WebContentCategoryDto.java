package com.domain.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;


@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WebContentCategoryDto {

    private Long id;

    private Long versionId;

    @NotNull(message = "Merci de préciser si la Catégorie est utilisée comme version principale")
    private Boolean inUse;

    @NotNull(message = "Merci de préciser si la Catégorie est publiée")
    private Boolean published;

    @NotBlank(message = "Merci de saisir le titre de la catégorie de contenu")
    @Length(min = 2, max = 50, message = "Le titre peut avoir entre 2 et 50 caractères")
    private String title;

    @NotBlank(message = "Merci de fournir une description de la catégorie")
    @Length(min = 2, max = 500, message = "La description doit avoir entre 2 et 500 caractères")
    private String description;

    @NotNull (message = "Merci d'ajouter la date de création de la catégorie")
    LocalDateTime creationDate;

    LocalDateTime editionDate;

    LocalDateTime suppressionDate;

    @ToString.Exclude
    @JsonIgnore
    private List<WebContentDto> webContents;

    private Integer  totalWebContents;
    private Integer totalPublishedWebContents;
    private Integer totalDeletedWebContents;

    private Integer totalFoundWebContents;

    private boolean defaultDeletableContent;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WebContentCategoryDto that = (WebContentCategoryDto) o;
        return Objects.equals(id, that.id);
    }



    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
