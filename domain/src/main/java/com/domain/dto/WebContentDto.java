package com.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;


@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WebContentDto {


    private Long id;

    private Long versionId;

    private Boolean inUse;

    @NotBlank(message = "Merci d'ajouter le titre principal, qui représentera le nom générique de votre publication")
    @Length(min = 2, max = 255, message = "Le titre doit avoir entre 2 et 255 caractères")
    private String title;

    @NotBlank(message = "Merci d'ajouter du contenu html pour pouvoir enregistrer")
    private String htmlContent;

    @NotBlank(message = "Merci d'ajouter le contenu brut pour pouvoir enregistrer")
    private String rawContent;

    @NotNull
    private LocalDateTime creationDate;

    private LocalDateTime editionDate;

    private LocalDateTime publishingDate;

    private LocalDateTime suppressionDate;

    @NotNull(message = "Merci de préciser si le type de contenu accepte des commentaires")
    private Boolean acceptsComments;

    @NotNull
    private Boolean submittedForPublication;

    @NotNull
    private Boolean published;

    private Boolean anonymousUser;

    @NotNull(message = "Le contenu doit être associé à un utilisateur")
    private UserDto user;

     @ToString.Exclude
    private List<WebContentCategoryDto> categories;

    @NotNull(message = "Merci de preciser le type de contenu")
    private WebContentTypeDto type;

    @ToString.Exclude
    @JsonIgnore
    private List<CommentDto> comments;

    private Integer totalComments;
    private Integer totalPublishedComments;
    private Integer totalWaitingPublicationComments;
    private Integer totalDeletedComments;

    private Integer totalKeywordInstances;

    private boolean defaultDeletableContent;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WebContentDto that = (WebContentDto) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
