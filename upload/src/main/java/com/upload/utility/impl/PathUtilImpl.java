package com.upload.utility.impl;

import com.upload.utility.PathUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

@Component
public class PathUtilImpl implements PathUtil {


    @Override
    public Path buildFilePathFromFileName(Path root, String fileName) {
        return root.resolve(fileName);
    }

    @Override
    public Path buildFilePathFromFile(Path root, MultipartFile file) {
        return root.resolve(file.getOriginalFilename());
    }

    @Override
    public String returnFileStringPathFromFileName(Path root, String fileName) {
        return buildFilePathFromFileName(root, fileName).toString();
    }

    @Override
    public String returnFileStringPathFromFile(Path root, MultipartFile file) {
        return returnFileStringPathFromFileName(root, file.getOriginalFilename());
    }

}
