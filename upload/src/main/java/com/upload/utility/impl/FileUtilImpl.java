package com.upload.utility.impl;

import com.upload.constant.response.UploadDefaultResponse;
import com.upload.exception.DifferentFileWithSameNameException;
import com.upload.utility.FileUtil;
import org.apache.commons.io.FileUtils;
import org.apache.tika.Tika;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class FileUtilImpl implements FileUtil {

    @Override
    public boolean fileNameExists(Path path) {
        return Files.exists(path);
    }


    @Override
    public void handleSameNameExists(Path alreadyStoredFilePath, MultipartFile newFile) throws
            DifferentFileWithSameNameException,FileAlreadyExistsException, IOException {

        //get the already stored file
        File storedFile = alreadyStoredFilePath.toFile();

        //create temporary file path
        Path temporaryFilePath = createTempFilePath(alreadyStoredFilePath);

        // store temporary file throws IOException
        File temporaryFile = storeTemporaryFile(temporaryFilePath, newFile);

        if (isSameFile(storedFile, temporaryFile)) {
            // deleteOne throws IOException
            deleteTemporaryFile(temporaryFilePath);
            throw new FileAlreadyExistsException(UploadDefaultResponse.FILE_ALREADY_EXISTS.getMessage());
        } else {
            throw new DifferentFileWithSameNameException();
        }
    }

    @Override
    public boolean isImage(MultipartFile file) throws IOException {
        Tika tika = new Tika();
        String mimeType = tika.detect(file.getInputStream());
        //return mimeType.equalsIgnoreCase("image/png");
        return StringUtils.startsWithIgnoreCase(mimeType,"image");
    }

    @Override
    public boolean isVideo(MultipartFile file) throws IOException {
        Tika tika = new Tika();
        String mimeType = tika.detect(file.getInputStream());
        //return mimeType.equalsIgnoreCase("image/png");
        return StringUtils.startsWithIgnoreCase(mimeType,"video");    }


    private boolean isSameFile(File fileOne, File fileTwo) throws IOException {
        return FileUtils.contentEquals(fileOne, fileTwo);
    }


    private File storeTemporaryFile(Path path, MultipartFile file) throws IOException {
        Files.copy(file.getInputStream(), path);
        return path.toFile();
    }

    private Path createTempFilePath(Path existingFilePath) {

        int i = 1;
        String stringPath = existingFilePath.toString();

        while (true) {
            Path tempPath = Paths.get(stringPath + "(" + i + ")");
            if (!Files.exists(tempPath)) {
                return tempPath;
            }
            i++;
        }
    }


    private void deleteTemporaryFile(Path path) throws IOException {
        Files.deleteIfExists(path);
    }


}
