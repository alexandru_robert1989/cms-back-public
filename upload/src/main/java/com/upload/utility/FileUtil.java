package com.upload.utility;

import com.upload.exception.DifferentFileWithSameNameException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Path;

public interface FileUtil {


    boolean fileNameExists(Path path);

    void handleSameNameExists(Path alreadyStoredFilePath, MultipartFile newFile) throws
            DifferentFileWithSameNameException, FileAlreadyExistsException, IOException;

    boolean isImage(MultipartFile file) throws IOException;

    boolean isVideo(MultipartFile file) throws IOException;

}
