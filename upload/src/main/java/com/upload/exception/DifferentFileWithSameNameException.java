package com.upload.exception;

import com.upload.constant.response.UploadDefaultResponse;

public class DifferentFileWithSameNameException extends RuntimeException {


    public DifferentFileWithSameNameException() {
        super(UploadDefaultResponse.DIFFERENT_FILE_WITH_SAME_NAME_EXISTS.getMessage());
    }
}
