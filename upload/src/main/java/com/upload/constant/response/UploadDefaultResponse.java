package com.upload.constant.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UploadDefaultResponse {

    SAVED("file_saved_successfully"),
    FILE_ALREADY_EXISTS("file_already_exists"),
    DIFFERENT_FILE_WITH_SAME_NAME_EXISTS("different_file_with_same_name_exists"),
    FILE_IS_NOT_IMAGE("file_is_not_image"),
    FILE_IS_NOT_VIDEO("file_is_not_video"),
    SAVING_FAILED("saving_file_failed"),
    DELETED_ONE("file_deleted"),
    DELETED_ALL("files_deleted"),
    MAX_UPLOAD_SIZE_EXCEEDED("max_upload_size_exceeded"),
    DELETING_ONE_FAILED("deleting_file_failed"),
    DELETING_ALL_FAILED("deleting_all_files_failed"),
    UNEXPECTED_EXCEPTION("file_upload_unexpected_exception");

    private final String message;


}
