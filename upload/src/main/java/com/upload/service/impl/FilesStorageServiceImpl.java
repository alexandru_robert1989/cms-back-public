package com.upload.service.impl;


import com.upload.constant.response.UploadDefaultResponse;
import com.upload.exception.DifferentFileWithSameNameException;
import com.upload.service.FilesStorageService;
import com.upload.utility.FileUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.UnsupportedDataTypeException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Service providing the methods used for uploading, downloading and deleting files.
 */

@Service
public class FilesStorageServiceImpl implements FilesStorageService {


    @Value("${apache.web.content.images}")
    private String webContentImagesDirectory;

    @Value("${apache.web.content.videos}")
    private String webContentVideosDirectory;

    @Value("${apache.profile.images}")
    private String profileImagesDirectory;

    @Value("${apache.documents}")
    private String documentsDirectory;


    private final FileUtil fileUtil;


    public FilesStorageServiceImpl(FileUtil fileUtil) {
        this.fileUtil = fileUtil;
    }

    @Override
    public void saveFile(MultipartFile file, Path path) throws FileAlreadyExistsException, IOException {

        if (fileUtil.fileNameExists(path)) {
            fileUtil.handleSameNameExists(path, file);
        } else {
            //else simply save the file
            Files.copy(file.getInputStream(), path);
        }
    }

    @Override
    public void saveWebContentImage(Long webContentVersionId, MultipartFile file)
            throws DifferentFileWithSameNameException, IOException {
        if (!fileUtil.isImage(file)) {
            throw new UnsupportedDataTypeException(UploadDefaultResponse.FILE_IS_NOT_IMAGE.getMessage());
        }
        String webContentImageName = createWebContentImageName(webContentVersionId, file);
        Path webContentImagePath = createWebContentImagePath(webContentImageName);
        saveFile(file, webContentImagePath);
    }


    @Override
    public void deleteWebContentImage(String fileName) throws IOException {
        Files.deleteIfExists(Paths.get(this.webContentImagesDirectory + fileName));
    }

    @Override
    public void saveWebContentVideo(Long webContentVersionId, MultipartFile file)
            throws DifferentFileWithSameNameException, IOException {

        if (!fileUtil.isVideo(file)) {
            throw new UnsupportedDataTypeException(UploadDefaultResponse.FILE_IS_NOT_VIDEO.getMessage());
        }
        String webContentVideoName = createWebContentVideoName(webContentVersionId, file);
        Path webContentVideoPath = createWebContentImagePath(webContentVideoName);
        saveFile(file, webContentVideoPath);

    }

    @Override
    public void deleteWebContentVideo(String fileName) throws IOException {
        Files.deleteIfExists(Paths.get(this.webContentVideosDirectory + fileName));

    }


    private Path createWebContentImagePath(String webContentImageName) {
        return Paths.get(this.webContentImagesDirectory + webContentImageName);
    }

    private String createWebContentImageName(Long webContentVersionId, MultipartFile file) {
        return "wc_" + webContentVersionId + "_image_" + file.getOriginalFilename();
    }

    private String createWebContentVideoName(Long webContentVersionId, MultipartFile file) {
        return "wc_" + webContentVersionId + "_video_" + file.getOriginalFilename();
    }

    private Path createWebContentVideoPath(String webContentImageName) {
        return Paths.get(this.webContentVideosDirectory + webContentImageName);
    }

}
