package com.upload.service;

import com.upload.exception.DifferentFileWithSameNameException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Path;

/**
 * Interface providing the abstract methods that once implemented are used
 * for uploading and downloading files.
 */
public interface FilesStorageService {

    void saveFile(MultipartFile file, Path path) throws FileAlreadyExistsException,IOException;

    void saveWebContentImage(Long webContentVersionID, MultipartFile file) throws
            DifferentFileWithSameNameException, IOException;

    void deleteWebContentImage(String fileName) throws IOException;


    void saveWebContentVideo(Long webContentVersionID, MultipartFile file) throws
            DifferentFileWithSameNameException, IOException;

    void deleteWebContentVideo(String fileName) throws IOException;


}
