package com.email.exceptions;

/**
 * Exception  thrown when an email attachment exceeds
 * the approved limit.
 */
public class AttachmentSizeException extends RuntimeException {

    /**
     * Constructor initialising a {@link AttachmentSizeException} with its
     * {@link String} message value.
     *
     * @param message a {@link String} value representing a message
     */
    public AttachmentSizeException(String message) {
        super(message);
    }
}
