package com.email.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EmailTemplateName {

   SIGNUP_CONFIRMATION_EMAIL("signupConfirmationEmail"),
   EMAIL_UTILISATION_ATTEMPT("emailUtilisationAttempt"),
    PASSWORD_RESET("passwordReset"),
    RESEND_VALIDATION_EMAIL("resendValidationEmail"),
    NEW_EMAIL_CONFIRMATION("newEmailConfirmation");

    private final String name;

}
