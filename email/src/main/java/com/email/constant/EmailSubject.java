package com.email.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EmailSubject {

    SIGNUP_CONFIRMATION ("Confirmation d'inscription sur ", "Inscription confirmation on "),
    EMAIL_UTILISATION_ATTEMPT ("Tentative utilisation adresse mail compte ", "Email utilisation attempt on "),
    RESEND_VALIDATION_EMAIL("Nouveau lien validation compte ","New validation account link for "),
    PASSWORD_RESET ("Demande réinitialisation mot de passe ", "New account validation link for"),
    NEW_EMAIL_CONFIRMATION("Confirmation changement courriel compte ","Email account modification on ");

    private final String FRENCH_TITLE;
    private final String ENGLISH_TITLE;



}
