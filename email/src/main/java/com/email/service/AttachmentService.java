package com.email.service;


import com.domain.entity.Message;
import com.email.exceptions.AttachmentSizeException;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import java.io.File;

/**
 * Interface providing the abstract methods that can be used to add an attachement
 * to an email.
 */
public interface AttachmentService {


    /**
     * This methods adds file attachments to a {@link Message}.
     *
     * @param message a {@link Message} object
     * @param helper  a {@link MimeMessageHelper} object
     * @throws AttachmentSizeException an {@link AttachmentSizeException}, thrown if the
     *                                 email attachments exceed the approved limit
     * @throws MessagingException      a {@link MessagingException}
     */
    void addAttachments(Message message, MimeMessageHelper helper) throws AttachmentSizeException, MessagingException;

    /**
     * This method returns the size of a file, expressed in kb.
     *
     * @param file a {@link File} object
     * @return a double value
     */
    double getFileSizeKb(File file);

    /**
     * This method returns the size of a file, expressed in mb.
     *
     * @param file a {@link File} object
     * @return a double value
     */

    double getFileSizeMb(File file);
}
