package com.email.service.impl;

import com.domain.entity.Message;
import com.domain.entity.Token;
import com.domain.entity.User;
import com.email.constant.EmailSubject;
import com.email.constant.EmailTemplateName;
import com.email.exceptions.AttachmentSizeException;
import com.email.service.AttachmentService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import com.email.service.EmailService;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Class providing the methods that can be used to send emails,
 * with or without attachments.
 */
@Service
public class EmailServiceImpl implements EmailService {


    /**
     * Object used for sending an email.
     */
    private final JavaMailSender javaMailSender;


    /**
     * Object used for adding attachement to an email.
     */
    private final AttachmentService attachmentService;

    /**
     * Object used for handling Thymeleaf templates.
     */
    private final SpringTemplateEngine templateEngine;

    public EmailServiceImpl(JavaMailSender javaMailSender,
                            AttachmentService attachmentService,
                            SpringTemplateEngine templateEngine) {
        this.javaMailSender = javaMailSender;
        this.attachmentService = attachmentService;
        this.templateEngine = templateEngine;
    }

    // TODO check this link when using Angular
    /**
     * The base url of the site using the API
     */
    @Value("${api.site.url}")
    private String siteUrl;

    /**
     * The site name.
     */
    @Value("${api.site.name}")
    private String siteName;

    /**
     * The end of the url that the user interface uses for
     * resetting a {@link User}'s password
     */
    @Value("${api.password.reset.link}")
    private String passwordResetLink;

    /**
     * The end of url that the user interface uses for
     * validating a {@link User}'s email address
     */
    @Value("${api.email.validation.link}")
    private String emailValidationLink;


    @Override
    public boolean sendSimpleEmail(Message message) throws MessagingException {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false);

        if (message.getSubject() != null) {
            helper.setSubject(message.getSubject());
        }
        helper.setText(message.getText(), true);
        helper.setTo(getRecipientsEmails(message));
        javaMailSender.send(mimeMessage);
        return true;
    }

    @Override
    public boolean sendSimpleEmail(List<User> recipients, String subject, String text) throws MessagingException {
        sendSimpleEmail(Message.builder()
                .recipients(recipients)
                .subject(subject)
                .text(text)
                .build());
        return true;
    }


    @Override
    public boolean sendEmailWithAttachment(Message message) throws MessagingException, AttachmentSizeException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        if (message.getSubject() != null) {
            helper.setSubject(message.getSubject());
        }
        helper.setText(message.getText());
        helper.setTo(getRecipientsEmails(message));
        if (!message.getFiles().isEmpty()) {
            attachmentService.addAttachments(message, helper);
        }
        javaMailSender.send(mimeMessage);
        return true;
    }


    @Override
    public String[] getRecipientsEmails(Message message) {
        List<String> recipientsEmails = new ArrayList<>();
        for (User user : message.getRecipients()) {
            recipientsEmails.add(user.getEmail());
        }
        return recipientsEmails.toArray(new String[recipientsEmails.size()]);
    }

    @Override
    public boolean sendSignupConfirmationEmail(User user, Token token) throws MessagingException {

        String validationLink = buildEmailValidationLink(token.getToken());
        String subject = EmailSubject.SIGNUP_CONFIRMATION.getFRENCH_TITLE() + siteName;

        List<User> recipients = new ArrayList<>();
        recipients.add(user);

        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("siteName", siteName);
        context.setVariable("siteUrl", siteUrl);
        context.setVariable("validationLink", validationLink);
        String html = templateEngine.process(EmailTemplateName.SIGNUP_CONFIRMATION_EMAIL.getName(), context);

        Message message = Message.builder()
                .recipients(recipients)
                .subject(subject)
                .text(html)
                .build();
        sendSimpleEmail(message);
        return true;
    }

    @Override
    public boolean sendPasswordResetEmail(User user, Token token) throws MessagingException {
        String passwordResetLink = buildPasswordResetLink(token.getToken());
        String subject = EmailSubject.PASSWORD_RESET.getFRENCH_TITLE() + siteName;

        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("siteName", siteName);
        context.setVariable("siteUrl", siteUrl);
        context.setVariable("passwordResetLink", passwordResetLink);
        String html = templateEngine.process(EmailTemplateName.PASSWORD_RESET.getName(), context);

        List<User> recipients = new CopyOnWriteArrayList<>();
        recipients.add(user);
        Message message = Message.builder()
                .recipients(recipients)
                .subject(subject)
                .text(html)
                .build();
        sendSimpleEmail(message);
        return true;
    }

    @Override
    public boolean resendValidationEmail(User user, Token token) throws MessagingException {
        String validationLink = buildEmailValidationLink(token.getToken());

        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("siteName", siteName);
        context.setVariable("siteUrl", siteUrl);
        context.setVariable("validationLink", validationLink);
        String html = templateEngine.process(EmailTemplateName.RESEND_VALIDATION_EMAIL.getName(), context);

        String subject = EmailSubject.RESEND_VALIDATION_EMAIL.getFRENCH_TITLE() + siteName;
        List<User> recipients = new ArrayList<>();
        recipients.add(user);
        sendSimpleEmail(recipients, subject, html);
        return true;
    }

    @Override
    public boolean newEmailAddressConfirmation(User user, Token token) throws MessagingException {

        String validationLink = buildEmailValidationLink(token.getToken());
        String subject = EmailSubject.NEW_EMAIL_CONFIRMATION.getFRENCH_TITLE() + siteName;
        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("siteName", siteName);
        context.setVariable("siteUrl", siteUrl);
        context.setVariable("validationLink", validationLink);
        String html = templateEngine.process(EmailTemplateName.NEW_EMAIL_CONFIRMATION.getName(), context);

        List<User> recipients = new ArrayList<>();
        recipients.add(user);
        sendSimpleEmail(recipients, subject, html);
        return true;
    }

    @Override
    public void signalMailUseAttempt(User user) throws MessagingException {
        List<User> recipients = new ArrayList<>();
        recipients.add(user);

        String subject = EmailSubject.EMAIL_UTILISATION_ATTEMPT.getFRENCH_TITLE() + siteName;

        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("siteName", siteName);
        context.setVariable("siteUrl", siteUrl);
        String html = templateEngine.process(EmailTemplateName.EMAIL_UTILISATION_ATTEMPT.getName(), context);

        Message message = Message.builder()
                .recipients(recipients)
                .subject(subject)
                .text(html)
                .build();
        sendSimpleEmail(message);

    }


    @Override
    public String buildPasswordResetLink(String token) {
        return this.siteUrl + this.passwordResetLink + token;
    }

    @Override
    public String buildEmailValidationLink(String token) {
        return this.siteUrl + this.emailValidationLink + token;
    }
}
