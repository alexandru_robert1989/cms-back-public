package com.email.service.impl;

import com.domain.entity.Message;

import com.email.exceptions.AttachmentSizeException;
import com.email.service.AttachmentService;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.File;
import java.util.Objects;

/**
 * Class providing the methods used to ad an
 * attachment to an email.
 */
@Service
public class AttachmentServiceImpl implements AttachmentService {


    /**
     * Constant double value representing the maximum attachements
     * size in mb.
     */
    private static final double MAX_SIZE_MB = 10;

    /**
     * Constant double value representing the maximum attachements
     * size in kb.
     */
    private static final double MAX_SIZE_KB = 1024 * MAX_SIZE_MB;


    @Override
    public void addAttachments(Message message, MimeMessageHelper helper)
            throws AttachmentSizeException, MessagingException {

        double attachmentsTotalSizeKb = 0;

        for (com.domain.entity.File file1 : message.getFiles()) {

            String filePath = file1.getFilePath();

            File fileAttachment = new File(filePath);

            if (fileAttachment.exists() && getFileSizeKb(fileAttachment) < MAX_SIZE_KB) {
                FileSystemResource file = new FileSystemResource(fileAttachment);
                helper.addAttachment(Objects.requireNonNull(file.getFilename()), file);
                attachmentsTotalSizeKb += getFileSizeKb(fileAttachment);

            } else if (fileAttachment.exists() && getFileSizeKb(fileAttachment) > MAX_SIZE_KB) {
                throw new AttachmentSizeException("Le fichier \""
                        + fileAttachment.getName() + "\", d'une taille de " + getFileSizeMb(fileAttachment) +
                        " mo, depasse la limite de " + MAX_SIZE_MB + " mo");
            }
        }
        if (attachmentsTotalSizeKb > MAX_SIZE_KB) {
            double attachmentsTotalSizeKbFormatted = Double.parseDouble(String.format("%.2f", attachmentsTotalSizeKb / 1024));
            throw new AttachmentSizeException("La taille totale des attachements, de "
                    + attachmentsTotalSizeKbFormatted +
                    " mo, depasse la limite de " + MAX_SIZE_MB + " mo");
        }


    }


    @Override
    public double getFileSizeKb(File file) {
        return file.length() / 1024;
    }


    @Override
    public double getFileSizeMb(File file) {
        return file.length() / (1024 * 1024);
    }
}
