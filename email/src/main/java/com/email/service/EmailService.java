package com.email.service;

import com.domain.entity.Message;
import com.domain.entity.Token;
import com.domain.entity.User;
import com.email.exceptions.AttachmentSizeException;

import javax.mail.MessagingException;
import java.util.List;

/**
 * Interface providing the abstract methods that can be implemented
 * to send emails, with or without attachments.
 */
public interface EmailService {

    /**
     * This method sends an email without attachments.
     *
     * @param message a {@link Message} object
     * @return a boolean value indicating if the mails was successfully send (true) or not (false)
     * @throws MessagingException a {@link MessagingException}, thrown if sending the email fails
     */
    boolean sendSimpleEmail(Message message) throws MessagingException;

    /**
     * This method sends an email without attachments.
     *
     * @param recipients a {@link List} of {@link User}s representing the email recipients
     * @param subject    a {@link String} value representing the email subject
     * @param text       a {@link String} value representing the email content
     * @return a boolean value indicating if the email was sent or not
     * @throws MessagingException a {@link MessagingException}, thrown if sending the email fails
     */
    boolean sendSimpleEmail(List<User> recipients, String subject, String text) throws MessagingException;


    /**
     * This method sends  an email with attachments.
     *
     * @param message a {@link Message} object
     * @return a boolean value indicating if the mails was successfully send (true) or not (false)
     * @throws MessagingException a {@link MessagingException}, thrown if the mail sending fails
     *                            throws {@link AttachmentSizeException} exception thrown if the attachments size approve the approved limit
     */
    boolean sendEmailWithAttachment(Message message) throws MessagingException, AttachmentSizeException;


    /**
     * This method returns a String array containing a
     * {@link Message}'s list of email recipients.
     *
     * @param message a {@link Message} object
     * @return an array of String values, representing email addresses
     */
    String[] getRecipientsEmails(Message message);

    /**
     * This methods sends a signup confirmation email containing a link
     * that allows an user to validate its email address.
     *
     * @param user  a {@link User}
     * @param token a {@link Token}
     * @return a boolean indicating if the mail was successfully sent
     * @throws MessagingException a {@link MessagingException}
     */
    boolean sendSignupConfirmationEmail(User user, Token token) throws MessagingException;

    /**
     * This method send to an user an email containing a link that will
     * allow him to reset his password.
     *
     * @param user  a {@link User}
     * @param token a {@link Token}
     * @return a boolean indicating if the mail was successfully sent
     * @throws MessagingException a {@link MessagingException}
     */
    boolean sendPasswordResetEmail(User user, Token token) throws MessagingException;


    /**
     * This methods sends an email containing a link
     * that allows an user to validate its email address.
     *
     * @param user  a {@link User}
     * @param token a {@link Token}
     * @return a boolean indicating if the mail was successfully sent
     * @throws MessagingException a {@link MessagingException}
     */
    boolean resendValidationEmail(User user, Token token) throws MessagingException;

    /**
     * This methods sends an email containing a link
     * that allows an user to validate its new email address.
     *
     * @param user  a {@link User}
     * @param token a {@link Token}
     * @return a boolean indicating if the mail was successfully sent
     * @throws MessagingException a {@link MessagingException}
     */
    boolean newEmailAddressConfirmation(User user, Token token) throws MessagingException;


    /**
     * This method sends an email to an user when a failed use attempt of his
     * email is registered, for exemple when someone tries to create an account
     * using his email address.
     *
     * @return a boolean indicating if the mail was successfully sent
     * @throws MessagingException a {@link MessagingException}
     */
    void signalMailUseAttempt(User user) throws MessagingException;

    /**
     * This method builds and returns a link that an {@link User} can use
     * to reset his password.
     *
     * @param token a String value representing a token
     * @return a String value representing the password reset link.
     */
    String buildPasswordResetLink(String token);


    /**
     * This method builds and returns a link that an {@link User} can use
     * to validate his email address.
     *
     * @param token a String value representing a token
     * @return a String value representing the password reset link.
     */
    String buildEmailValidationLink(String token);

}
