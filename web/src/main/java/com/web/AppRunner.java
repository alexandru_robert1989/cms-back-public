package com.web;


import com.persistence.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;


/**
 * Main class, starting the application.
 */

@EnableJpaRepositories(basePackages = {"com.*"})
@EntityScan(basePackages = {"com.*"})
@ComponentScan(basePackages = {"com.*"})
@EnableScheduling
@SpringBootApplication()
public class AppRunner extends SpringBootServletInitializer {


    public static void main(String[] args) {
        SpringApplication.run(AppRunner.class);
    }


    /**
     * Method needed for packaging the app as a WAR file
     *
     * @param builder a {@link SpringApplicationBuilder} object
     * @return a {@link SpringApplicationBuilder} object
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {

        return builder.sources(AppRunner.class);

    }


}
