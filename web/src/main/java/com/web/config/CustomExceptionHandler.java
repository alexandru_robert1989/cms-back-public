package com.web.config;


import com.persistence.exception.NotFoundException;
import com.persistence.payload.response.ConstraintViolationExceptionResponse;
import com.persistence.payload.response.MethodArgumentNotValidResponse;
import com.persistence.payload.response.ResponseStringMessage;
import com.upload.constant.response.UploadDefaultResponse;
import org.apache.tomcat.util.http.fileupload.impl.SizeLimitExceededException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {SizeLimitExceededException.class})
    protected ResponseEntity<?> handleSizeLimitExceededException(RuntimeException ex, WebRequest request) {
        return ResponseEntity.badRequest().body(new ResponseStringMessage(UploadDefaultResponse.MAX_UPLOAD_SIZE_EXCEEDED.getMessage()));
    }

    @ExceptionHandler(value = {NotFoundException.class})
    protected ResponseEntity<?> handleNotFoundException(RuntimeException ex, WebRequest request) {
        return ResponseEntity.badRequest().body(new ResponseStringMessage(ex.getMessage()));
    }


    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<?> handleConstraintViolation(
            ConstraintViolationException ex, WebRequest request) {
        return ConstraintViolationExceptionResponse.generate(ex);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
                                                                          HttpHeaders headers, HttpStatus status, WebRequest request) {
        return ResponseEntity.badRequest().body(new ResponseStringMessage(ex.getMessage()));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {

        return ResponseEntity.badRequest().body(MethodArgumentNotValidResponse.generate(ex));
    }
}
