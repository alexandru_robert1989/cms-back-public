package com.web.config;

import com.persistence.signup_login.security.jwt.AuthEntryPointJwt;
import com.persistence.signup_login.security.jwt.AuthTokenFilter;
import com.persistence.signup_login.security.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * Class handling the Spring Security configuration.
 * <p>
 * {@link org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter}
 * provides a configuration method for {@link org.springframework.security.config.annotation.web.builders.HttpSecurity},
 * <p>
 * while {@link org.springframework.web.servlet.config.annotation.WebMvcConfigurer} provides a method used
 * for global CORS configuration.
 * <p>
 * {@link EnableWebSecurity} allows Spring to find and automatically
 * apply the class to the global Web Security.
 * {@link EnableGlobalMethodSecurity} provides AOP security on methods.
 * <p>
 * It enables @PreAuthorize, @PostAuthorize, it also supports JSR-250.
 */


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        //securedEnabled = true,
        //jsr250Enabled = true,
        prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {


    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    AuthEntryPointJwt authEntryPointJwt;

    @Bean
    public AuthTokenFilter authenticationJwtTokenFilter() {
        return new AuthTokenFilter();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Implementation of {@link WebSecurityConfigurerAdapter}'s
     * authenticationManagerBean method, exposing the AuthenticationManager built
     * using configure(AuthenticationManagerBuilder) as a Spring bean
     *
     * @return a AuthenticationManager object
     * @throws Exception a {@link Exception}
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * Implementation of {@link WebSecurityConfigurerAdapter}'s
     * configure(AuthenticationManagerBuilder authenticationManagerBuilder) method,
     * configuring {@link AuthenticationManagerBuilder}'s password encoder.
     *
     * @param authenticationManagerBuilder a {@link AuthenticationManagerBuilder} object
     * @throws Exception a {@link Exception}
     */
    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    /**
     * Implementation of {@link WebSecurityConfigurerAdapter}'s
     * configure(HttpSecurity http) method, configuring {@link HttpSecurity}
     * url filters.
     *
     * @param http a {@link HttpSecurity} object
     * @throws Exception a {@link Exception}
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(authEntryPointJwt).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests().antMatchers("/public/**").permitAll()
                .antMatchers("/auth/**").permitAll().anyRequest().authenticated();


        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);

    }


    /**
     * Method used for allowing the use of Swagger.
     *
     * @param web a {@link WebSecurity} object
     * @throws Exception an {@link Exception}
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**");
    }


    /**
     * Implementation of {@link WebMvcConfigurer}'s addCorsMappings method,
     * allowing global CORS configuration.
     *
     * @param registry a {@link CorsRegistry} object
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOrigins("http://localhost:4200",
                "https://www.site-example.fr", "*");


    }


    @Bean
    public BasicAuthenticationEntryPoint swaggerAuthenticationEntryPoint() {
        BasicAuthenticationEntryPoint entryPoint = new BasicAuthenticationEntryPoint();
        entryPoint.setRealmName("Swagger Realm");
        return entryPoint;
    }


}

