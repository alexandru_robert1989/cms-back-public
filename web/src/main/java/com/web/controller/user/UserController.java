package com.web.controller.user;


import com.domain.entity.User;
import com.persistence.exception.*;
import com.persistence.payload.response.ConstraintViolationExceptionResponse;
import com.persistence.payload.response.ResponseStringMessage;
import com.persistence.service.user.UserUserService;
import com.persistence.signup_login.payload.request.ModifyEmailAddressRequest;
import com.persistence.signup_login.payload.request.ModifyUserBaseInfoRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;

import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.ConstraintViolationException;

/**
 * Controller allowing a logged in {@link User}
 * to handle his account information.
 */
@CrossOrigin(value = "http://localhost:4200")
@RestController
@RequestMapping("auth/user")
@AllArgsConstructor
public class UserController {

    private final UserUserService userService;


    /**
     * Method returning the session {@link User}.
     *
     * @return a {@link ResponseEntity} containing a {@link User} if the session {@link User} if found, or a
     * {@link ResponseStringMessage} if the session {@link User} is not found.
     */
    @PreAuthorize("hasRole('USER')")
    @GetMapping("/get-session-user")
    public ResponseEntity<?> getSessionUser() {
        try {
            User user = userService.getSessionUser();
            return ResponseEntity.ok(user);
        } catch (AuthenticationException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("not_found"));
        }
    }

    /**
     * Method allowing a {@link User} to modify his base info: first name, last name, pseudo and newsletter inscription.
     *
     * @param request a {@link ModifyUserBaseInfoRequest}
     * @return a {@link ResponseEntity} containing a {@link SuccessResponse} if the request succeeds or a {@link ResponseStringMessage}
     * if the request is not successful
     */
    @PreAuthorize("hasRole('USER')")
    @PostMapping("/modify-base-info")
    public ResponseEntity<?> modifyBaseInfo(@RequestBody ModifyUserBaseInfoRequest request) {
        try {
            userService.modifyBaseInfo(request);
            return ResponseEntity.ok(new ResponseStringMessage("success"));
        } catch (AuthenticationException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("unauthorised"));
        } catch (PseudoAlreadyInUseException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("unavailable_pseudo"));
        } catch (ConstraintViolationException e) {
            return ConstraintViolationExceptionResponse.generate(e);
        }
    }


    /**
     * Method allowing a {@link User} to receive a email containing a link that allows him
     * to validate his email address.
     *
     * @return a {@link ResponseEntity} containing a {@link SuccessResponse} if the request succeeds or a *
     * {@link ResponseStringMessage} if the request fails.
     */
    @PreAuthorize("hasRole('USER')")
    @GetMapping("/resend-validation-mail")
    public ResponseEntity<?> resendValidationMail() {
        try {
            if (userService.resendValidationMail()) {
                return ResponseEntity.ok(new ResponseStringMessage("success"));
            } else {
                return ResponseEntity.badRequest().body(new ResponseStringMessage("unauthorised"));
            }
        } catch (AuthenticationException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("unauthorised"));
        } catch (MessagingException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("mail_exception"));
        }
    }


    @PreAuthorize("hasRole('USER')")
    @PostMapping("/modify-email-address")
    public ResponseEntity<?> modifyEmailAddress(@RequestBody ModifyEmailAddressRequest request) {
        try {
            if (userService.modifyEmailAddress(request)) {
                return ResponseEntity.ok(new ResponseStringMessage("success"));
            } else {
                return ResponseEntity.badRequest().body(new ResponseStringMessage("unauthorised"));
            }
        } catch (NotFoundException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("unauthorised"));
        } catch (EmailNotAvailableException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("email_not_available"));
        } catch (EmailAlreadyOwnedException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("email_already_owned"));
        } catch (IncorrectPasswordException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("incorrect_password"));
        } catch (MessagingException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("mail_exception"));
        }
    }


}
