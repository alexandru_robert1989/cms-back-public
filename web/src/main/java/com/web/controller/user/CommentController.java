package com.web.controller.user;


import com.domain.entity.Comment;
import com.domain.entity.User;
import com.persistence.exception.AuthenticationException;
import com.persistence.exception.NotFoundException;
import com.persistence.payload.request.AddCommentRequest;
import com.persistence.payload.response.ResponseStringMessage;
import com.persistence.service.user.UserCommentService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller allowing a logged in {@link User}
 * to leave {@link Comment}s.
 */
@CrossOrigin(value = "http://localhost:4200")
@RestController
@RequestMapping("auth/user/comments")
@AllArgsConstructor
public class CommentController {


    /**
     * Service user for allowing {@link User}s to leave {@link Comment}s
     */
    private final UserCommentService commentService;


    /**
     * Method allowing a {@link User} to leave {@link Comment}s
     *
     * @param request a {@link AddCommentRequest}
     * @return a {@link ResponseEntity} containing a message that informs the user if the {@link Comment}
     * was saved or not
     */
    @PostMapping("/add-comment")
    public ResponseEntity<?> addComment(@RequestBody AddCommentRequest request) {

        try {
            commentService.saveComment(request);
            return ResponseEntity.ok(new ResponseStringMessage("success"));
        } catch (NotFoundException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("not_found"));
        } catch (AuthenticationException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("authentication_exception"));
        } catch (NullPointerException | NumberFormatException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("invalid_request"));
        }
    }
}
