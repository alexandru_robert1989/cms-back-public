package com.web.controller.signup_login;

import com.domain.entity.User;
import com.persistence.exception.*;
import com.persistence.payload.response.ConstraintViolationExceptionResponse;
import com.persistence.payload.response.ResponseStringMessage;
import com.persistence.signup_login.payload.request.UserSignupRequest;
import com.persistence.signup_login.service.SignupAndLogin;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;

/**
 * Controller allowing users to signup by creating a new {@link User} account.
 * <p>
 * The all arguments constructor was implemented implicitly using {@link lombok.Lombok} annotations.
 */

@Log4j2
@CrossOrigin(value = "http://localhost:4200")
@RestController
@RequestMapping("public/signup")
@AllArgsConstructor
public class SignupController {

    private final SignupAndLogin signupAndLogin;

    /**
     * Method allowing a visitor to create a {@link User} account.
     *
     * @param request a {@link UserSignupRequest}
     * @return a {@link ResponseBody} containing a message describing if the account was created, and the
     * details of why the account was not created if that is the case
     */
    @PostMapping("/signup")
    public ResponseEntity<?> signup(@RequestBody UserSignupRequest request) {
        try {
            signupAndLogin.userSignup(request);
            return ResponseEntity.ok(new ResponseStringMessage("success"));
        } catch (NotFoundException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("unattended_exception"));
        } catch (EmailAlreadyOwnedException | EmailNotAvailableException e) {
            return ResponseEntity.ok(new ResponseStringMessage("success"));
        } catch (PseudoAlreadyInUseException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("unavailable_pseudo"));
        } catch (SignupEmailException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("mail_exception"));
        } catch (ConstraintViolationException e) {
            return ConstraintViolationExceptionResponse.generate(e);
        } catch (NullPointerException | IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("invalid_request"));
        }
    }


    /**
     * Method allowing a {@link User} to validate his email address
     *
     * @param token a {@link String} value
     * @return a {@link ResponseEntity} containing a message telling if the email validation succeeded or not
     */
    @PostMapping("/email-validation")
    public ResponseEntity<?> validateEmail(@RequestBody String token) {
        if (token == null) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage
                    ("invalid_token"));
        }

        if (signupAndLogin.validateUserEmail(token)) {
            return ResponseEntity.ok(new ResponseStringMessage("success"));
        }
        return ResponseEntity.badRequest().body(new ResponseStringMessage
                ("invalid_token"));
    }


}
