package com.web.controller.signup_login;


import com.domain.constant.response_message.*;
import com.domain.entity.Token;
import com.domain.entity.User;
import com.persistence.exception.NotFoundException;
import com.persistence.exception.PasswordsNotMatchingException;
import com.persistence.exception.TokenNotValidException;
import com.persistence.exception.UserDeletedException;
import com.persistence.payload.request.PasswordResetEmailRequest;
import com.persistence.payload.request.PasswordResetRequest;
import com.persistence.payload.response.ResponseStringMessage;
import com.persistence.signup_login.payload.request.LoginRequest;
import com.persistence.signup_login.service.SignupAndLogin;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;

/**
 * Controller allowing {@link User}s to login using their email and password.
 * <p>
 * The all arguments constructor was implemented implicitly using {@link lombok.Lombok} annotations.
 */


@CrossOrigin(value = "${api.site.url}")
@RestController
@RequestMapping("public/login")
@AllArgsConstructor
@Log4j2
public class LoginController {

    private final SignupAndLogin signupAndLogin;


    /**
     * Method allowing a {@link User} to login using his email
     * address and his password.
     *
     * @param request a {@link LoginRequest}
     * @return a {@link ResponseEntity} containing a {@link com.persistence.signup_login.payload.response.LoginJwtResponse} if the
     * login succeeds or a {@link ResponseStringMessage} if the login fails
     */
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginRequest request) {
        try {
            return ResponseEntity.ok(signupAndLogin.login(request));
        } catch (AuthenticationException e) {
            log.error("Sign in authentication exception from : {}", request.getEmail());
            return ResponseEntity.badRequest().body(new ResponseStringMessage(UserResponseMessage.AUTHENTICATION_EXCEPTION.getMessage()));
        }
    }


    /**
     * Method sending to a {@link User} an email containing a link that allows him
     * to reset his password
     *
     * @param request a {@link PasswordResetEmailRequest}
     * @return a {@link ResponseEntity} containing a message informing the {@link User} if the reset password
     * email was sent
     */
    @PostMapping("/send-password-reset-email")
    public ResponseEntity<?> sendPasswordResetEmail(@RequestBody PasswordResetEmailRequest request) {

        try {
            if (request.getEmail() == null) {
                return ResponseEntity.badRequest().body(new ResponseStringMessage(InvalidRequestMessage.INVALID_REQUEST.getMessage()));
            }
            signupAndLogin.sendPasswordResetEmail(request.getEmail());
            return ResponseEntity.ok(new ResponseStringMessage(RequestSuccessMessage.SUCCESS_MESSAGE.getMessage()));
        } catch (NotFoundException | UserDeletedException e) {
            return ResponseEntity.ok(new ResponseStringMessage(RequestSuccessMessage.SUCCESS_MESSAGE.getMessage()));
        } catch (MessagingException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage(MessageResponseMessage.MESSAGING_EXCEPTION.getMessage()));
        }
    }


    /**
     * Method allowing a {@link User} to reset his password using a {@link Token}.
     *
     * @param request a {@link PasswordResetRequest}
     * @return a {@link ResponseEntity} containing a message describing the result of the request
     */
    @PostMapping("/reset-password")
    public ResponseEntity<?> resetPassword(@RequestBody PasswordResetRequest request) {
        try {
            if (request == null || request.getToken() == null || request.getNewPassword() == null || request.getNewPasswordConfirmation() == null) {
                return ResponseEntity.badRequest().body(new ResponseStringMessage(InvalidRequestMessage.INVALID_REQUEST.getMessage()));
            }
            if (signupAndLogin.resetPassword(request)) {
                return ResponseEntity.ok(new ResponseStringMessage(RequestSuccessMessage.SUCCESS_MESSAGE.getMessage()));
            } else
                return ResponseEntity.badRequest().body(new ResponseStringMessage(TokenResponseMessage.MISSING_TOKEN.getMessage()));
        } catch (PasswordsNotMatchingException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage(UserResponseMessage.PASSWORD_MISMATCH.getMessage()));
        } catch (UserDeletedException | TokenNotValidException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage(TokenResponseMessage.INVALID_OR_EXPIRED_TOKEN.getMessage()));
        }

    }


}
