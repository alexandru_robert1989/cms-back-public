package com.web.controller.visitor;


import com.domain.entity.WebContent;
import com.persistence.exception.NotFoundException;
import com.persistence.payload.response.ResponseStringMessage;
import com.persistence.service.visitor.VisitorWebContentService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller returning {@link WebContent} values to visitors.
 */
@CrossOrigin(value = "http://localhost:4200")
@RestController
@RequestMapping("public/visitor/web-content")
@AllArgsConstructor
public class VisitorWebContentController {

    /**
     * Service handling {@link WebContent}s
     */
    private final VisitorWebContentService visitorWebContentService;


    @GetMapping("/home-page")
    public ResponseEntity<?> getHomePage() {
        try {
            WebContent webContent = visitorWebContentService.getHomePage();
            return ResponseEntity.ok(webContent);
        } catch (NotFoundException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("not_found"));
        }
    }


    @GetMapping("/legal-mentions")
    public ResponseEntity<?> getLegalMentionsPage() {
        try {
            WebContent webContent = visitorWebContentService.getLegalMentionsPage();
            return ResponseEntity.ok(webContent);
        } catch (NotFoundException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage("not_found"));
        }
    }




    @GetMapping("/get-all/{sort}")
    public ResponseEntity<?> getAllByTypeSorted(@PathVariable String sort) {
        return ResponseEntity.ok(visitorWebContentService.getAllPublishedSorted(sort));
    }


    @GetMapping("/get-one/{id}")
    public ResponseEntity<?> getOneById(@PathVariable Long id) {
        return ResponseEntity.ok(visitorWebContentService.getByVersionId(id));

    }



    @GetMapping("/get-all-by-type/{typeVersionId}/{sort}")
    public ResponseEntity<?> getAllByTypeSorted(@PathVariable Long typeVersionId, @PathVariable String sort) {
        return ResponseEntity.ok(visitorWebContentService.getAllByTypeSorted(typeVersionId, sort));
    }



    @GetMapping("/get-all-by-category/{categoryVersionId}/{sort}")
    public ResponseEntity<?> getAllByCategorySorted(@PathVariable Long categoryVersionId, @PathVariable String sort) {
        return ResponseEntity.ok(visitorWebContentService.getAllByCategorySorted(categoryVersionId, sort));
    }




    @GetMapping("/get-all-by-editor/{editorId}/{sort}")
    public ResponseEntity<?> getAllByEditorSorted(@PathVariable Long editorId, @PathVariable String sort) {
        return ResponseEntity.ok(visitorWebContentService.getAllByEditorSorted(editorId, sort));
    }



    @GetMapping("/get-all-by-type-and-category/{typeVersionId}/{categoryVersionId}/{sort}")
    public ResponseEntity<?> getAllByTypeAndCategorySorted
            (@PathVariable Long typeVersionId, @PathVariable Long categoryVersionId, @PathVariable String sort) {
        return ResponseEntity.ok(visitorWebContentService.getAllByTypeAndCategorySorted(typeVersionId, categoryVersionId, sort));
    }




    @GetMapping("/get-all-by-type-and-user/{typeVersionId}/{userId}/{sort}")
    public ResponseEntity<?> getAllByTypeAndUserSorted
            (@PathVariable Long typeVersionId, @PathVariable Long userId, @PathVariable String sort) {
        return ResponseEntity.ok(visitorWebContentService.getAllByTypeAndUserSorted(typeVersionId, userId, sort));
    }



    @GetMapping("/get-all-by-category-and-user/{categoryVersionId}/{userId}/{sort}")
    public ResponseEntity<?> getAllByCategoryAndUserSorted
            (@PathVariable Long categoryVersionId, @PathVariable Long userId, @PathVariable String sort) {
        return ResponseEntity.ok(visitorWebContentService.getAllByCategoryAndUserSorted(categoryVersionId, userId, sort));
    }



    @GetMapping("/get-all-by-type-and-category-and-user/{typeVersionId}/{categoryVersionId}/{userId}/{sort}")
    public ResponseEntity<?> getAllByTypeAndCategoryAndUserSorted
            (@PathVariable Long typeVersionId, @PathVariable Long categoryVersionId,
             @PathVariable Long userId, @PathVariable String sort) {
        return ResponseEntity.ok(visitorWebContentService.getAllByTypeAndCategoryAndUserSorted(typeVersionId, categoryVersionId, userId, sort));
    }






}
