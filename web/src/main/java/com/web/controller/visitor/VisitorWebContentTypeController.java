package com.web.controller.visitor;


import com.domain.entity.WebContentType;
import com.persistence.service.visitor.VisitorWebContentTypeService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller returning {@link WebContentType} values to visitors.
 * <p>
 * The all arguments constructor was implemented implicitly using {@link lombok.Lombok} annotations.
 */
@CrossOrigin(value = "http://localhost:4200")
@RestController
@RequestMapping("public/visitor/web-content-type")
@AllArgsConstructor
public class VisitorWebContentTypeController {


    private final VisitorWebContentTypeService visitorWebContentTypeService;


    @GetMapping("/get-by-version-id/{versionId}")
    public ResponseEntity<?> getByVersionId(@PathVariable Long versionId) {
        return ResponseEntity.ok(visitorWebContentTypeService.getOneInUseByVersionId(versionId));
    }

    @GetMapping("/get-navbar-types")
    public ResponseEntity<?> getNavbarTypes() {
        return ResponseEntity.ok(visitorWebContentTypeService.getAllNavbarItems());
    }

    @GetMapping("/get-footer-types")
    public ResponseEntity<?> getNFooterTypes() {
        return ResponseEntity.ok(visitorWebContentTypeService.getAllFooterItems());

    }


    @GetMapping("/get-web-content-sorting-types")
    public ResponseEntity<?> getWebContentSortingTypes() {
        return ResponseEntity.ok(visitorWebContentTypeService.getTypeSortingItems());
    }


}
