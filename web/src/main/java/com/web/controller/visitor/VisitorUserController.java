package com.web.controller.visitor;


import com.domain.entity.User;
import com.persistence.service.visitor.VisitorUserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller returning {@link User} values to visitors.
 * <p>
 * The all arguments constructor was implemented implicitly using {@link lombok.Lombok} annotations.
 */
@CrossOrigin(value = "http://localhost:4200")
@RestController
@RequestMapping("public/visitor/users")
@AllArgsConstructor
public class VisitorUserController {


    private final VisitorUserService visitorUserService;


    @GetMapping("/editors")
    public ResponseEntity<?> getEditors() {
        return ResponseEntity.ok(visitorUserService.getAllEditors());
    }


    @GetMapping("/editors/{id}")
    public ResponseEntity<?> getEditorById(@PathVariable Long id) {
        return ResponseEntity.ok(visitorUserService.getEditorById(id));
    }

    @GetMapping("/by-web-content/{id}")
    public ResponseEntity<?> getEditorByWebContentId(@PathVariable Long id) {
        return ResponseEntity.ok(visitorUserService.getEditorByWebContentVersionId(id));
    }


}
