package com.web.controller.visitor;


import com.domain.payload.request.CreateCommentRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@RestController
@RequestMapping("public/test")
@AllArgsConstructor
public class TestController {


    @PostMapping("/upload")
    public ResponseEntity<?> testFileUpload(MultipartFile file) {


        Path path = Paths.get("/home/vps-3af0d71a/public_html/azerty/" + file.getOriginalFilename());
        try {
            Files.copy(file.getInputStream(), path);
            return ResponseEntity.ok("File saved");
        } catch (IOException e) {
            return ResponseEntity.badRequest().body(e);
        }

    }


}
