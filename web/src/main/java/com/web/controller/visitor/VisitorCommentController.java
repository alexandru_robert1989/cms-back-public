package com.web.controller.visitor;


import com.domain.dto.CommentDto;
import com.domain.entity.Comment;
import com.persistence.payload.response.ResponseStringMessage;

import com.persistence.service.visitor.VisitorCommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller returning {@link Comment} values to visitors.
 * <p>
 * The all arguments constructor was implemented implicitly using {@link lombok.Lombok} annotations.
 */
@Api("Visitor Comment controller")
@CrossOrigin(value = "http://localhost:4200")
@RestController
@RequestMapping("public/visitor/comments")
@AllArgsConstructor
public class VisitorCommentController {


    private final VisitorCommentService visitorCommentService;


    @ApiOperation(value = "Get all the parent comments of a Web Content")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = CommentDto.class, responseContainer = "List", message = "Returning a List of MessageDto"),
            @ApiResponse(code = 400, response = ResponseStringMessage.class, message = "Returning a message explaining the failure")
    })
    @GetMapping("/parent-comments/{webContentVersionId}")
    public ResponseEntity<?> getWebContentParentComments(@PathVariable Long webContentVersionId) {
        return ResponseEntity.ok(visitorCommentService.getWebContentParentComments(webContentVersionId));
    }

    @ApiOperation(value = "Get all the children/reaction comments of a comment")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = CommentDto.class, responseContainer = "List", message = "Returning a List of MessageDto"),
            @ApiResponse(code = 400, response = ResponseStringMessage.class, message = "Returning a message explaining the failure")
    })
    @GetMapping("/children-comments/{commentId}")
    public ResponseEntity<?> getWebContentChildrenComments(@PathVariable Long commentId) {
        return ResponseEntity.ok(visitorCommentService.getCommentReactions(commentId));

    }


}
