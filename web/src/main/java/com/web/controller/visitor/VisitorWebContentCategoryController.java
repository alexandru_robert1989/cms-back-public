package com.web.controller.visitor;

import com.domain.entity.WebContentCategory;
import com.persistence.service.visitor.VisitorWebContentCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller returning {@link WebContentCategory} values to visitors.
 * <p>
 * The all arguments constructor was implemented implicitly using {@link lombok.Lombok} annotations.
 */
@CrossOrigin(value = "http://localhost:4200")
@RestController
@RequestMapping("public/visitor/web-content-category")
@AllArgsConstructor
public class VisitorWebContentCategoryController {

    private final VisitorWebContentCategoryService visitorWebContentCategoryService;


    @GetMapping("/get-by-version-id/{versionId}")
    public ResponseEntity<?> getByVersionId(@PathVariable Long versionId) {
        return ResponseEntity.ok(visitorWebContentCategoryService.getByVersionId(versionId));
    }


    @GetMapping("/get-web-content-sorting-categories")
    public ResponseEntity<?> getSortingItems() {
        return ResponseEntity.ok(visitorWebContentCategoryService.getWebContentSortingCategories());
    }

    @GetMapping("/get-navbar-categories")
    public ResponseEntity<?> getNavbarCategories() {
        return ResponseEntity.ok(visitorWebContentCategoryService.getNavbarCategories());
    }


}
