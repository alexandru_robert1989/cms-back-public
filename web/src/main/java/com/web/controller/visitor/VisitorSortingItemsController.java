package com.web.controller.visitor;


import com.domain.dto.WebContentOrderSorterDto;
import com.persistence.constant.sort_name.CommentSortName;
import com.persistence.constant.sort_name.DateSortName;
import com.persistence.constant.sort_name.SizeSortName;
import com.persistence.constant.sort_name.TitleSortName;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@CrossOrigin(value = "http://localhost:4200")
@RestController
@RequestMapping("public/visitor/sorting-items")
public class VisitorSortingItemsController {


    @GetMapping("/get-order-sorters")
    public ResponseEntity<?> getOrderSorters() {
        return returnOrderSorters();
    }


    public ResponseEntity<?> returnOrderSorters() {
        List<WebContentOrderSorterDto> orderSorters = new ArrayList<>();
        orderSorters.add(new WebContentOrderSorterDto(TitleSortName.ASC_TITLE, "Titres A -> Z"));
        orderSorters.add(new WebContentOrderSorterDto(TitleSortName.DESC_TITLE, "Titres Z -> A"));
        orderSorters.add(new WebContentOrderSorterDto(DateSortName.ASC_PUBLISHING_DATE, "Date publication ascendante"));
        orderSorters.add(new WebContentOrderSorterDto(DateSortName.DESC_PUBLISHING_DATE, "Date publication descendante"));
        orderSorters.add(new WebContentOrderSorterDto(DateSortName.ASC_EDITION_DATE, "Date édition ascendante"));
        orderSorters.add(new WebContentOrderSorterDto(DateSortName.DESC_EDITION_DATE, "Date édition descendante"));
        orderSorters.add(new WebContentOrderSorterDto(CommentSortName.TOTAL_COMMENTS_ASC, "Total commentaires ordre croissante"));
        orderSorters.add(new WebContentOrderSorterDto(CommentSortName.TOTAL_COMMENTS_DESC, "Total commentaires ordre décroissante"));
        orderSorters.add(new WebContentOrderSorterDto(SizeSortName.SIZE_ASC, "Taille article croissante"));
        orderSorters.add(new WebContentOrderSorterDto(SizeSortName.SIZE_DESC, "Taille article décroissante"));
        return ResponseEntity.ok(orderSorters);
    }


}
