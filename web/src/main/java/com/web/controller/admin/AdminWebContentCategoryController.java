package com.web.controller.admin;


import com.domain.dto.CommentDto;
import com.domain.entity.WebContentCategory;
import com.persistence.constant.sort_name.DateSortName;
import com.persistence.payload.response.ResponseStringMessage;
import com.persistence.service.sort_and_comparator.SortSelector;
import com.persistence.exception.SuppressionException;
import com.persistence.payload.response.ConstraintViolationExceptionResponse;
import com.persistence.service.admin.AdminWebContentCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;

/**
 * Controller used by the admin for handling {@link WebContentCategory}es.
 * <p>
 * The all arguments constructor was implemented implicitly using {@link lombok.Lombok} annotations.
 */
@Api(value = "Admin WebContentCategory Controller")
@CrossOrigin(value = "http://localhost:4200")
@RestController
@RequestMapping("auth/admin/category")
@AllArgsConstructor
public class AdminWebContentCategoryController {


    /**
     * Service handling {@link WebContentCategory}es
     */
    private final AdminWebContentCategoryService webContentCategoryService;


    @ApiOperation(value = "Create a new WebContentCategory")
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody WebContentCategory category) {
        try {
            category = webContentCategoryService.create(category);
            return ResponseEntity.ok(category);
        } catch (ConstraintViolationException e) {
            return ResponseEntity.badRequest().body(ConstraintViolationExceptionResponse.generate(e));
        }
    }

    @ApiOperation(value = "Modify a  WebContentCategory")
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/modify")
    public ResponseEntity<?> modify(@RequestBody WebContentCategory category) {
        try {
            category = webContentCategoryService.modify(category);
            return ResponseEntity.ok(category);
        } catch (ConstraintViolationException e) {
            return ResponseEntity.badRequest().body(ConstraintViolationExceptionResponse.generate(e));
        }
    }


    @ApiOperation(value = "Delete a in use WebContentCategory logically")
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/delete-one-in-use-from-display/{versionId}")
    public ResponseEntity<?> deleteOneInUseFromDisplay(@PathVariable long versionId) {
        try {
            webContentCategoryService.deleteOneInUseFromDisplay(versionId);
            return ResponseEntity.ok(new ResponseStringMessage("deleted"));
        } catch (SuppressionException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage(e.getMessage()));
        }
    }

    @ApiOperation(value = "Delete a WebContentCategory version logically")
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/delete-one-version-from-display/{id}")
    public ResponseEntity<?> deleteOneVersionFromDisplay(@PathVariable long id) {
        try {
            webContentCategoryService.deleteOneVersionFromDisplay(id);
            return ResponseEntity.ok(new ResponseStringMessage("deleted"));
        } catch (SuppressionException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage(e.getMessage()));
        }
    }


    @ApiOperation(value = "Delete a  WebContentCategory logically")
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/find-not-suppressed-versions/{wccVersionId}")
    public ResponseEntity<?> findAllNotSuppressedVersions(@PathVariable long wccVersionId) {
        Sort sort = SortSelector.selectWebContentCategorySortObject(DateSortName.DESC_EDITION_DATE);
        return ResponseEntity.ok(webContentCategoryService.findAllNotSuppressedVersions(wccVersionId, sort));
    }

    @ApiOperation(value = "Find a not logically suppressed WebContentCategory version")
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/find-not-suppressed-version/{id}")
    public ResponseEntity<?> findOneNotSuppressedVersion(@PathVariable long id) {
        WebContentCategory webContentCategory = webContentCategoryService.findOneNotSuppressedVersion(id);
        if (webContentCategory != null) {
            return ResponseEntity.ok(webContentCategory);
        }
        return ResponseEntity.badRequest().body(new ResponseStringMessage("not_found"));
    }


    @ApiOperation(value = "Delete logically all the  WebContentCategory created on startup logically")
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/delete-startup-categories-from-display")
    public ResponseEntity<?> deleteStartupCategoriesFromDisplay() {
        try {
            webContentCategoryService.deleteAllStartupCategoriesFromDisplay();
            return ResponseEntity.ok(new ResponseStringMessage("deleted"));
        } catch (SuppressionException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage(e.getMessage()));
        }
    }


}
