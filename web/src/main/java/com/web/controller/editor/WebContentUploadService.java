package com.web.controller.editor;


import com.upload.constant.response.UploadDefaultResponse;
import com.upload.exception.DifferentFileWithSameNameException;
import com.upload.service.FilesStorageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.UnsupportedDataTypeException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;

@RestController
@RequestMapping("public/web_content-upload")
@AllArgsConstructor
public class WebContentUploadService {


    private final FilesStorageService filesStorageService;

    @PostMapping("/upload-image")
    public ResponseEntity<?> uploadImage(@RequestParam Long wcVersionId,@RequestParam MultipartFile file) {

        try {
            filesStorageService.saveWebContentImage(wcVersionId, file);
            return ResponseEntity.ok("File saved");
        } catch (IOException e) {
            if (e instanceof UnsupportedDataTypeException || e instanceof FileAlreadyExistsException) {
                return ResponseEntity.badRequest().body(e.getMessage());
            } else {
                return ResponseEntity.badRequest().body(UploadDefaultResponse.UNEXPECTED_EXCEPTION);

            }
        } catch (DifferentFileWithSameNameException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }


    }


    @PostMapping("/upload-video")
    public ResponseEntity<?> uploadVideo(@RequestParam Long wcVersionId,@RequestParam MultipartFile file) {

        try {
            filesStorageService.saveWebContentVideo(wcVersionId, file);
            return ResponseEntity.ok("File saved");
        } catch (IOException e) {
            if (e instanceof UnsupportedDataTypeException || e instanceof FileAlreadyExistsException) {
                return ResponseEntity.badRequest().body(e.getMessage());
            } else {
                return ResponseEntity.badRequest().body(UploadDefaultResponse.UNEXPECTED_EXCEPTION);

            }
        } catch (DifferentFileWithSameNameException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }


    }


}
