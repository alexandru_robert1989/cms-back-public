package com.web.controller.super_admin;


import com.domain.entity.WebContentCategory;
import com.persistence.payload.response.ResponseStringMessage;
import com.persistence.service.sort_and_comparator.SortSelector;
import com.persistence.exception.RestoringException;
import com.persistence.exception.SuppressionException;
import com.persistence.service.super_admin.impl.SuperAdminWebContentCategoryServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Controller used by the super admin for handling {@link WebContentCategory}es.
 * <p>
 * The all arguments constructor was implemented implicitly using {@link lombok.Lombok} annotations.
 */
@CrossOrigin(value = "http://localhost:4200")
@RestController
@RequestMapping("auth/super-admin/category")
@AllArgsConstructor
public class SuperAdminWebContentCategoryController {


    /**
     * Service handling {@link WebContentCategory}es
     */
    private final SuperAdminWebContentCategoryServiceImpl webContentCategoryService;


    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping("/delete-startup-categories-permanently")
    public ResponseEntity<?> deleteStartupCategoriesPermanently() {
        try {
            webContentCategoryService.deleteStartupCategoriesPermanently();
            return ResponseEntity.ok(new ResponseStringMessage("deleted_permanently"));
        } catch (SuppressionException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage(e.getMessage()));
        }
    }

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping("/delete-in-use-permanently/{wccVersionId}")
    public ResponseEntity<?> deleteInUsePermanently(@PathVariable long wccVersionId) {
        try {
            webContentCategoryService.deleteInUsePermanently(wccVersionId);
            return ResponseEntity.ok(new ResponseStringMessage("deleted_permanently"));
        } catch (SuppressionException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage(e.getMessage()));
        }
    }

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping("/delete-version-permanently/{id}")
    public ResponseEntity<?> deleteVersionPermanently(@PathVariable long id) {
        try {
            webContentCategoryService.deleteVersionPermanently(id);
            return ResponseEntity.ok(new ResponseStringMessage("deleted_permanently"));
        } catch (SuppressionException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage(e.getMessage()));
        }
    }


    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping("/restore-in-use-/{wccVersionId}")
    public ResponseEntity<?> restoreInUse(@PathVariable long wccVersionId) {
        try {
            webContentCategoryService.restoreInUse(wccVersionId);
            return ResponseEntity.ok(new ResponseStringMessage("restored"));
        } catch (RestoringException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage(e.getMessage()));
        }
    }


    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping("/restore-version/{id}")
    public ResponseEntity<?> restoreVersion(@PathVariable long id) {
        try {
            webContentCategoryService.restoreVersion(id);
            return ResponseEntity.ok(new ResponseStringMessage("deleted_permanently"));
        } catch (RestoringException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage(e.getMessage()));
        }
    }

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping("/find-by-id/{id}")
    public ResponseEntity<?> findById(@PathVariable long id) {
        WebContentCategory webContentCategory = webContentCategoryService.findById(id);
        if (webContentCategory != null) {
            return ResponseEntity.ok(webContentCategory);
        }
        return ResponseEntity.badRequest().body(new ResponseStringMessage("not_found"));
    }


    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping("/find-all")
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(webContentCategoryService.findAll());
    }


    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping("/find-all-suppressed/{sortingType}")
    public ResponseEntity<?> findAllSuppressed(@PathVariable String sortingType) {
        Sort sort = SortSelector.selectWebContentCategorySortObject(sortingType);
        return ResponseEntity.ok(webContentCategoryService.findAllSuppressedFromDisplay(sort));
    }


    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping("/find-all-suppressed-in-use/{sortingType}")
    public ResponseEntity<?> findAllSuppressedInUse(@PathVariable String sortingType) {
        Sort sort = SortSelector.selectWebContentCategorySortObject(sortingType);

        return ResponseEntity.ok(webContentCategoryService.findAllSuppressedFromDisplayInUse(sort));
    }


    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping("/find-all-by-version-id/{wccVersionId}")
    public ResponseEntity<?> findAllByVersionId(@PathVariable long wccVersionId) {
        return ResponseEntity.ok(webContentCategoryService.findAllByVersionId(wccVersionId));
    }


    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping("/find-all-suppressed-by-version-id/{wccVersionId}")
    public ResponseEntity<?> findAllSuppressedByVersionId(@PathVariable long wccVersionId) {
        return ResponseEntity.ok(webContentCategoryService.findAllSuppressedByVersionId(wccVersionId));
    }
}
