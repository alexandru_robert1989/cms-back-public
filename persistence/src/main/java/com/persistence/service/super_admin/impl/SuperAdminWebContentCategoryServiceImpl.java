package com.persistence.service.super_admin.impl;

import com.domain.entity.WebContent;
import com.domain.entity.WebContentCategory;
import com.persistence.constant.sort_object.TitleSortObject;
import com.persistence.exception.RestoringException;
import com.persistence.exception.SuppressionException;
import com.persistence.repository.WebContentCategoryRepository;
import com.persistence.repository.WebContentRepository;
import com.persistence.service.admin.AdminWebContentCategoryService;
import com.persistence.service.super_admin.SuperAdminWebContentCategoryService;
import lombok.AllArgsConstructor;
import lombok.Lombok;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Service providing the methods used by super admins for interacting with
 * {@link WebContentCategory}es.
 * <p>
 * The all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */
@Service
@AllArgsConstructor
public class SuperAdminWebContentCategoryServiceImpl implements SuperAdminWebContentCategoryService {


    /**
     * Repository used for handling  {@link WebContentCategory}es.
     */
    private final WebContentCategoryRepository webContentCategoryRepository;


    /**
     * Repository used for handling  {@link WebContent}s.
     */
    private final WebContentRepository webContentRepository;


    /**
     * Service used by admin for handling {@link WebContentCategory}es
     */
    private final AdminWebContentCategoryService adminWebContentCategoryService;


    /**
     * Method deleting permanently all the default {@link WebContentCategory}es created on startup.
     *
     * @return a boolean indicating if suppression succeeded
     * @throws SuppressionException a {@link SuppressionException}, thrown if suppression fails
     */
    @Override
    public boolean deleteStartupCategoriesPermanently() throws SuppressionException {
        CopyOnWriteArrayList<WebContentCategory> webContentCategories = adminWebContentCategoryService.findStartupCategories();

        webContentCategories.forEach(c -> {
            try {
                deleteInUsePermanently(c.getVersionId());
            } catch (SuppressionException e) {
            }
        });
        if (adminWebContentCategoryService.findStartupCategories().isEmpty()) {
            return true;
        }

        throw new SuppressionException("suppression_failed");
    }


    @Override
    public boolean deleteInUsePermanently(long versionId) throws SuppressionException {
        CopyOnWriteArrayList<WebContentCategory> webContentCategories = findAllByVersionId(versionId);
        webContentCategories.forEach(wcc -> webContentCategoryRepository.delete(wcc));
        if (findAllByVersionId(versionId).isEmpty()) {
            return true;
        }
        throw new SuppressionException("suppression_failed");
    }


    @Override
    public boolean deleteVersionPermanently(long id) throws SuppressionException {
        WebContentCategory webContentCategory = findById(id);
        if (webContentCategory == null) {
            throw new SuppressionException("already_deleted");
        }
        webContentCategoryRepository.delete(webContentCategory);
        if (findById(id) == null) {
            return true;
        }
        throw new SuppressionException("suppression_failed");
    }


    @Override
    public boolean restoreInUse(long wccVersionId) throws RestoringException {
        CopyOnWriteArrayList<WebContentCategory> webContentCategories = webContentCategoryRepository
                .findAllByVersionIdAndInUseIsTrue(wccVersionId);
        if (webContentCategories.isEmpty()) {
            throw new RestoringException("to_restore_item_not_found");
        }
        WebContentCategory webContentCategory = webContentCategories.get(0);
        webContentCategory.setSuppressionDate(null);
        adminWebContentCategoryService.modify(webContentCategory);
        if (!webContentCategoryRepository.findAllByVersionIdAndInUseIsTrueAndSuppressionDateIsNull(wccVersionId).isEmpty()) {
            return true;
        }
        throw new RestoringException("restoring_failed");
    }


    @Override
    public boolean restoreVersion(long id) throws RestoringException {
        WebContentCategory webContentCategory = findById(id);
        if (webContentCategory == null) {
            throw new RestoringException("to_restore_item_not_found");
        }
        webContentCategory.setSuppressionDate(null);
        webContentCategoryRepository.save(webContentCategory);
        if (findById(id) == null) {
            return true;
        }
        throw new RestoringException("restoring_failed");
    }


    @Override
    public WebContentCategory findById(long id) {
        Optional<WebContentCategory> optional = webContentCategoryRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        }
        return null;
    }


    @Override
    public List<WebContentCategory> findAll() {
        return webContentCategoryRepository.findAll();
    }


    @Override
    public CopyOnWriteArrayList<WebContentCategory> findAllSuppressedFromDisplay(Sort sort) {
        return webContentCategoryRepository.findAllBySuppressionDateIsNotNull(sort);
    }


    @Override
    public CopyOnWriteArrayList<WebContentCategory> findAllSuppressedFromDisplayInUse(Sort sort) {
        return webContentCategoryRepository.findAllByInUseIsTrueAndSuppressionDateIsNotNull(sort);

    }


    @Override
    public CopyOnWriteArrayList<WebContentCategory> findAllByVersionId(long versionId) {
        return webContentCategoryRepository.findAllByVersionId(versionId);
    }


    @Override
    public CopyOnWriteArrayList<WebContentCategory> findAllSuppressedByVersionId(long versionId) {
        return webContentCategoryRepository
                .findAllByVersionIdAndSuppressionDateIsNotNull(versionId);
    }


    @Override
    public void setCategoryProperties(WebContentCategory webContentCategory) {
        webContentCategory.setTotalWebContents(webContentRepository
                .findAllByWebContentCategoriesContainsAndPublishedIsTrueAndInUseIsTrueAndSuppressionDateIsNull
                        (webContentCategory, TitleSortObject.ASC_TITLE).size());
        webContentCategory.setWebContents(null);
    }


}
