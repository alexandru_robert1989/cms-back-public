package com.persistence.service.super_admin;


import com.domain.entity.WebContentCategory;
import com.persistence.exception.RestoringException;
import com.persistence.exception.SuppressionException;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Interface providing the abstract methods used by super admins for handling
 * {@link WebContentCategory}es.
 */
public interface SuperAdminWebContentCategoryService {


    /**
     * Completely delete all the default {@link WebContentCategory}es created on startup.
     *
     * @return a boolean value indicating if the suppression succeeded
     * @throws SuppressionException a {@link SuppressionException} thrown if suppression fails
     */
    boolean deleteStartupCategoriesPermanently() throws SuppressionException;


    /**
     * Completely delete a in use {@link WebContentCategory} and also all of its versions.
     *
     * @param versionId a long value representing the version id of a {@link WebContentCategory}
     * @return a boolean value indication if the method succeeded or not
     * @throws SuppressionException a {@link SuppressionException}, thrown if suppression fails
     */
    boolean deleteInUsePermanently(long versionId) throws SuppressionException;


    /**
     * Completely delete a specific version of a  {@link WebContentCategory} .
     *
     * @param id a long value representing the id of a {@link WebContentCategory}
     * @return a boolean value indication if the method succeeded or not
     * @throws SuppressionException a {@link SuppressionException}, thrown if suppression fails
     */
    boolean deleteVersionPermanently(long id) throws SuppressionException;


    /**
     * Restore a in use {@link WebContentCategory}.
     *
     * @param wccVersionId a long value representing a {@link WebContentCategory} version id.
     * @return a boolean value indicating if the {@link WebContentCategory} was restores
     * @throws RestoringException a {@link RestoringException}, thrown if restoring fails
     */
    boolean restoreInUse(long wccVersionId) throws RestoringException;


    /**
     * Restore a in  {@link WebContentCategory} version .
     *
     * @param id a long value representing a {@link WebContentCategory} id
     * @return a boolean value indicating if the {@link WebContentCategory} was restores
     * @throws RestoringException a {@link RestoringException}, thrown if restoring fails
     */
    boolean restoreVersion(long id) throws RestoringException;


    /**
     * Find a {@link WebContentCategory} by its id.
     *
     * @param id a long value representing a {@link WebContentCategory} id
     * @return a {@link WebContentCategory}
     */
    WebContentCategory findById(long id);


    /**
     * Find all the {@link WebContentCategory}es
     *
     * @return a {@link List} of {@link WebContentCategory}es
     */
    List<WebContentCategory> findAll();


    /**
     * Find all the suppressed {@link WebContentCategory}es.
     *
     * @param sort a {@link Sort} object
     * @return a {@link List} of {@link WebContentCategory}es.
     */
    CopyOnWriteArrayList<WebContentCategory> findAllSuppressedFromDisplay(Sort sort);


    /**
     * Find all the suppressed in use {@link WebContentCategory}es.
     *
     * @param sort a {@link Sort} object
     * @return a {@link List} of {@link WebContentCategory}es.
     */
    CopyOnWriteArrayList<WebContentCategory> findAllSuppressedFromDisplayInUse(Sort sort);


    /**
     * Find all the versions of a {@link WebContentCategory}
     *
     * @param versionId a long value representing a {@link WebContentCategory} version id
     * @return a {@link List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByVersionId(long versionId);


    /**
     * Find all the suppressed versions of a {@link WebContentCategory}.
     *
     * @param versionId a long value representing the version id of a {@link WebContentCategory}
     * @return a {@link List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllSuppressedByVersionId(long versionId);


    /**
     * Method returning a {@link WebContentCategory} with its properties containing
     * all the properties that a super admin can use.
     *
     * @param webContentCategory a {@link WebContentCategory}
     */
    void setCategoryProperties(WebContentCategory webContentCategory);


}
