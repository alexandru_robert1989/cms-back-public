package com.persistence.service.startup;

import com.domain.entity.Comment;
import com.domain.entity.WebContent;


/**
 * Interface providing the abstract global methods used for handling
 * {@link Comment} objects.
 */
public interface StartupCommentService {

    /**
     * Method adding fake {@link Comment}s if there are no {@link Comment}s.
     *
     * @param maxReactionsByComment int representing the maximum {@link Comment} reactions to be added for each {@link Comment}
     * @param maxCommentByWc        int representing the maximum {@link Comment}s to be added for a {@link WebContent}
     * @return a boolean value indicating if the comments were added
     */
    boolean addStartupTestComments(int maxReactionsByComment, int maxCommentByWc);


}
