package com.persistence.service.startup.impl;


import com.persistence.service.startup.*;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;


/**
 * Class providing the methods used for adding
 * the data needed on application startup.
 */
@AllArgsConstructor
@Service
public class AddStartupDataImpl implements AddStartupData, CommandLineRunner {


    private final StartupRoleService startupRoleService;
    private final StartupUserService startupUserService;
    private final StartupWebContentTypeService startupWebContentTypeService;
    private final StartupWebContentCategoryService startupWebContentCategoryService;
    private final StartupWebContentService startupWebContentService;
    private final StartupCommentServiceImpl startupCommentService;


    @Override
    public void addStartupData() {


        startupRoleService.addStartupRoles();
        startupUserService.addStartupDefaultUsers();
        startupUserService.addStartupMultipleTestUsers(500);
        startupWebContentTypeService.addStartupTypes();
        startupWebContentCategoryService.addStartupCategories();
        startupWebContentService.addStartupWebContents(500);
        startupCommentService.addStartupTestComments(150, 150);

    }


    @Override
    public void run(String... args) {
        addStartupData();
    }
}
