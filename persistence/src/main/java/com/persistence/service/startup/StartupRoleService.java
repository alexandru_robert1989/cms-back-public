package com.persistence.service.startup;


import com.domain.entity.Role;

/**
 * Interface providing the abstract global methods used for handling {@link Role}s
 */
public interface StartupRoleService {


    boolean addStartupRoles();


}
