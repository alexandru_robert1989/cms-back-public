package com.persistence.service.startup;

import com.domain.entity.User;
import com.persistence.exception.NotFoundException;

/**
 * Interface providing the abstract global methods used for handling {@link User}s
 */
public interface StartupUserService {


    User addStartupDefaultUser(String firstName, String lastName, String pseudo, String email,
                               String password, String role);

    boolean addStartupDefaultUsers();


    boolean addStartupMultipleTestUsers(int totalUsers) throws NotFoundException;


}
