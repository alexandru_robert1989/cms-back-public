package com.persistence.service.startup;

import com.domain.entity.WebContentCategory;

import javax.validation.ConstraintViolationException;

/**
 * Interface providing the abstract methods used by admins for handling
 * {@link WebContentCategory}es.
 */
public interface StartupWebContentCategoryService {


    WebContentCategory create(WebContentCategory webContentCategory) throws ConstraintViolationException;

    void addStartupCategories();

}
