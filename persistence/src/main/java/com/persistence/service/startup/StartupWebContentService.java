package com.persistence.service.startup;

import com.domain.entity.*;
import com.persistence.exception.NotFoundException;

/**
 * Interface providing the global abstract methods that can be
 * implemented to allow a visitor to interact with
 * {@link WebContent} objects.
 */
public interface StartupWebContentService {





    WebContent addStartupWebContent(String title, String htmlContent, String rawContent, boolean acceptsComments,
                                    boolean submittedForPublication, boolean isPublished, WebContentType webContentType,
                                    User user);

    WebContent addStartupWebContentWithDefaultId(String title, String htmlContent, String rawContent, boolean acceptsComments,
                                                 boolean submittedForPublication, boolean isPublished, long defaultId, WebContentType webContentType,
                                                 User user);

    void addStartupHomePage();

    void addStartupLegalMentionsPage() throws NotFoundException;

    void addStartupWebContents(int totalWebContents) throws NotFoundException;


}
