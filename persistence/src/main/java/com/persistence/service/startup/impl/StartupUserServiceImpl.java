package com.persistence.service.startup.impl;

import com.domain.constant.response_message.RoleResponseMessage;
import com.domain.entity.Role;
import com.domain.entity.User;
import com.github.javafaker.Faker;
import com.persistence.constant.DefaultVersionId;
import com.persistence.constant.sort_object.DateSortObject;
import com.persistence.exception.NotFoundException;
import com.persistence.repository.RoleRepository;
import com.persistence.repository.UserRepository;
import com.persistence.service.startup.StartupUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;


@RequiredArgsConstructor
@Service
public class StartupUserServiceImpl implements StartupUserService {


    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Value("${default.user.password}")
    private String defaultUserPassword;


    @Override
    public User addStartupDefaultUser(String firstName, String lastName, String pseudo,
                                      String email, String password, String role) {

        if (userRepository.findByEmail(email) != null && userRepository.findByPseudo(pseudo) != null) {
            return null;
        }

        List<Role> roles = returnStartupRolesByHighestRoleName(role);

        User user = User.builder()
                .firstName(firstName)
                .lastName(lastName)
                .pseudo(pseudo)
                .email(email)
                .password(passwordEncoder.encode(password))
                .profileImageValidated(true)
                .emailValidated(true)
                .getNewsletter(true)
                .signUpDate(LocalDateTime.now())
                .roles(roles)
                .build();

        user = userRepository.save(user);

        CopyOnWriteArrayList<User> users = new CopyOnWriteArrayList<>();
        users.add(user);

        for (Role dbUserRole : roles) {
            updateRoleUsers(dbUserRole.getVersionId(), users);
        }
        return user;
    }

    @Override
    public boolean addStartupDefaultUsers() {

        if (!userRepository.findAll().isEmpty()) {
            return false;
        }


        addStartupDefaultUser("User", "User", "user", "user@user.com",
                defaultUserPassword, "user");

        addStartupDefaultUser("Editor", "Editor", "editor", "editor@editor.com",
                defaultUserPassword, "editor");

        addStartupDefaultUser("Publisher", "Publisher", "publisher", "publisher@publisher.com",
                defaultUserPassword, "publisher");

        addStartupDefaultUser("Moderator", "Moderator", "moderator", "moderator@moderator.com",
                defaultUserPassword, "moderator");

        addStartupDefaultUser("Admin", "Admin", "admin", "admin@admin.com",
                defaultUserPassword, "admin");

        addStartupDefaultUser("SuperAdmin", "SuperAdmin", "superadmin", "superadmin@superadmin.com",
                defaultUserPassword, "super_admin");
        return true;
    }


    @Override
    public boolean addStartupMultipleTestUsers(int totalUsers) throws NotFoundException {

        if (userRepository.findAll().size() != (roleRepository.findAllByInUseIsTrue().size())) {
            updateRoleTotalUsers();
            return false;
        }
        Role roleUser = findRoleInUseByVersionId(DefaultVersionId.ROLE_USER.getId());
        List<Role> roles = new ArrayList<>();
        roles.add(roleUser);

        Faker faker = new Faker();

        List<String> firstNamesList = new ArrayList<>();
        while (firstNamesList.size() < totalUsers) {
            firstNamesList.add(faker.name().firstName());
        }

        List<String> lastNamesList = new ArrayList<>();
        while (lastNamesList.size() < totalUsers) {
            lastNamesList.add(faker.name().lastName());
        }

        Set<String> pseudos = new LinkedHashSet<>();
        while (pseudos.size() < totalUsers) {
            pseudos.add(faker.name().username());
        }
        List<String> pseudosList = new ArrayList<>(pseudos);

        Set<String> mails = new LinkedHashSet<>();
        while (mails.size() < totalUsers) {
            mails.add(faker.internet().safeEmailAddress());
        }
        List<String> mailsList = new ArrayList<>(mails);


        CopyOnWriteArrayList<User> savedUsers = new CopyOnWriteArrayList<>();
        List<User> toSaveUsers = new ArrayList<>();

        for (String email : mailsList) {

            long start = System.currentTimeMillis();

            int index = mailsList.indexOf(email);
            boolean isTrue = index % 2 == 0;

            User user = User.builder()
                    .lastName(lastNamesList.get(index))
                    .firstName(firstNamesList.get(index))
                    .pseudo(pseudosList.get(index))
                    .email(email).password(passwordEncoder.encode(defaultUserPassword))
                    .profileImageValidated(isTrue)
                    .emailValidated(isTrue)
                    .getNewsletter(isTrue)
                    .roles(roles)
                    .defaultDeletableContent(true)
                    .signUpDate(LocalDateTime.now())
                    .build();

            toSaveUsers.add(user);


            if (toSaveUsers.size() == 50) {
                savedUsers.addAll(userRepository.saveAll(toSaveUsers));
                updateRoleUsers(roleUser.getVersionId(), savedUsers);
                toSaveUsers.clear();
                savedUsers.clear();
            }

        }
        savedUsers.addAll(userRepository.saveAll(toSaveUsers));
        updateRoleUsers(roleUser.getVersionId(), savedUsers);
        updateRoleTotalUsers();

        return true;
    }


    private void updateRoleTotalUsers() {
        List<Role> roles = roleRepository.findAll();
        roles.forEach(r -> {
            List<User> users = userRepository.findAllByRolesContains(r);
            r.setTotalUsers(users.size());
            r.setTotalDeletedUsers((int) users.stream().filter(u -> u.getSuppressionDate() != null).count());
            r.setTotalNotDeletedUsers((int) users.stream().filter(u -> u.getSuppressionDate() == null).count());
            roleRepository.save(r);
        });

    }

    private Role findRoleInUseByVersionId(Long versionId) throws NotFoundException {
        CopyOnWriteArrayList<Role> roles = roleRepository.findAllByInUseIsTrueAndVersionId(versionId);
        if (roles.isEmpty()) {
            throw new NotFoundException(RoleResponseMessage.NOT_FOUND.getMessage());
        }
        return roles.get(0);
    }

    private Role updateRoleUsers(long versionId, CopyOnWriteArrayList<User> users) throws NotFoundException {
        Role roleInUse = findRoleInUseByVersionId(versionId);
        CopyOnWriteArrayList<User> roleInUseUsers = userRepository.findAllByRolesContains(roleInUse);
        roleInUseUsers.addAllAbsent(users);
        roleInUse.setUsers(roleInUseUsers);
        roleRepository.save(roleInUse);
        return roleInUse;
    }

    private List<Role> returnStartupRolesByHighestRoleName(String roleTitle) {

        List<Role> roleList = roleRepository.findAllByInUseIsTrueAndTitle("ROLE_" + roleTitle.toUpperCase());
        if (roleList.isEmpty()) {
            return null;
        }
        Role role = roleList.get(0);
        CopyOnWriteArrayList<Role> roles = roleRepository.findAllByInUseIsTrue(DateSortObject.DESC_CREATION_DATE);
        roles.removeIf(r -> r.getVersionId() > role.getVersionId());
        return roles;

    }

}



