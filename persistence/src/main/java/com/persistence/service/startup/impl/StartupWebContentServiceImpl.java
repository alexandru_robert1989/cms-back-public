package com.persistence.service.startup.impl;

import com.domain.constant.response_message.RoleResponseMessage;
import com.domain.constant.response_message.TypeResponseMessage;
import com.domain.constant.response_message.UserResponseMessage;
import com.domain.entity.*;
import com.github.javafaker.Faker;
import com.persistence.constant.DefaultStartupWebContentTitle;
import com.persistence.constant.DefaultVersionId;
import com.persistence.constant.StartupPageTemplateName;
import com.persistence.constant.sort_object.DateSortObject;
import com.persistence.exception.NotFoundException;
import com.persistence.repository.*;
import com.persistence.service.startup.StartupWebContentService;
import lombok.AllArgsConstructor;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * Service providing the global  methods used for interacting with
 * {@link WebContent}s
 * objects.
 * <p>
 * The all arguments constructor was implemented using {@link lombok.Lombok} annotations.
 */
@Service
@AllArgsConstructor
public class StartupWebContentServiceImpl implements StartupWebContentService {


    private final WebContentRepository webContentRepository;
    private final WebContentTypeRepository webContentTypeRepository;
    private final WebContentCategoryRepository webContentCategoryRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final SpringTemplateEngine templateEngine;


    @Override
    public WebContent addStartupWebContent(String title, String htmlContent, String rawContent,
                                           boolean acceptsComments,
                                           boolean submittedForPublication,
                                           boolean isPublished,
                                           WebContentType webContentType, User user) {

        WebContent webContent = new WebContent();
        webContent.setTitle(title);
        webContent.setHtmlContent(htmlContent);
        webContent.setRawContent(rawContent);
        webContent.setAcceptsComments(acceptsComments);
        webContent.setSubmittedForPublication(submittedForPublication);
        webContent.setPublished(isPublished);
        webContent.setInUse(true);
        webContent.setDefaultDeletableContent(true);

        if (isPublished) {
            webContent.setPublishingDate(LocalDateTime.now());
            webContent.setSubmittedForPublication(false);
        }

        webContent.setWebContentType(webContentType);
        webContent.setUser(user);
        webContent = webContentRepository.save(webContent);
        webContent.setVersionId(webContent.getId());
        webContent = webContentRepository.save(webContent);
        return webContent;
    }


    @Override
    public WebContent addStartupWebContentWithDefaultId(String title, String htmlContent,
                                                        String rawContent, boolean acceptsComments,
                                                        boolean submittedForPublication, boolean isPublished,
                                                        long defaultId, WebContentType webContentType, User user) {
        WebContent webContent = WebContent.builder()
                .title(title)
                .htmlContent(htmlContent)
                .rawContent(rawContent)
                .acceptsComments(acceptsComments)
                .submittedForPublication(submittedForPublication)
                .published(isPublished)
                .inUse(true)
                .webContentType(webContentType)
                .user(user)
                .id(defaultId)
                .versionId(defaultId)
                .build();

        if (isPublished) {
            webContent.setPublishingDate(LocalDateTime.now());
            webContent.setSubmittedForPublication(false);
        }

        return webContentRepository.save(webContent);

    }

    @Override
    public void addStartupLegalMentionsPage() throws NotFoundException {

        String title = DefaultStartupWebContentTitle.LEGAL_MENTIONS_PAGE.getFrenchTitle();
        Context context = new Context();
        String html = templateEngine.process(StartupPageTemplateName.LEGAL_MENTIONS.getName(), context);
        String raw = Jsoup.parse(html).text();
        WebContentType webContentType = findTypeInUseByVersionId(DefaultVersionId.LEGAL_MENTIONS_TYPE.getId());
        User admin = findAdmin();
        addStartupWebContentWithDefaultId(title, html, raw, false, false,
                true, DefaultVersionId.LEGAL_MENTIONS_WEB_CONTENT.getId(), webContentType, admin);

    }

    @Override
    public void addStartupHomePage() {
        String title = DefaultStartupWebContentTitle.HOME_PAGE.getFrenchTitle();
        Context context = new Context();
        String html = templateEngine.process(StartupPageTemplateName.HOME_PAGE.getName(), context);
        String raw = Jsoup.parse(html).text();
        WebContentType webContentType = findTypeInUseByVersionId(DefaultVersionId.HOME_PAGE_TYPE.getId());
        User admin = findAdmin();
        addStartupWebContentWithDefaultId(title, html, raw, false, false,
                true, DefaultVersionId.HOME_PAGE_WEB_CONTENT.getId(), webContentType, admin);
    }


    @Override
    public void addStartupWebContents(int totalWebContents) throws NotFoundException {

        if (!webContentRepository.findAll().isEmpty()) {
            updateTypeTotalWebContents();
            updateCategoryTotalWebContents();
            updateUsersTotalWebContents();
            return;
        }

        addStartupHomePage();
        addStartupLegalMentionsPage();

        Faker faker = new Faker();

        List<WebContent> toSaveWebContents = new ArrayList<>();

        CopyOnWriteArrayList<String> titlesList = createTitlesList(totalWebContents, faker);
        CopyOnWriteArrayList<String> linksList = createLinksList(totalWebContents, faker);
        CopyOnWriteArrayList<String> quotes = createQuotesList(faker);

        List<WebContentType> webContentTypes = findTypesUsableWithDefaultWebContents();
        List<User> editors = findAllEditors();
        List<WebContentCategory> webContentCategories = findAllCategoriesInUse();

        Random random = new Random();
        Context context = new Context();

        titlesList.forEach(t -> {

            context.setVariable("title", t);

            User webContentEditor = editors.get(random.nextInt(editors.size()));
            WebContentType webContentType = webContentTypes.get(random.nextInt(webContentTypes.size()));

            WebContent webContent = WebContent.builder()
                    .user(webContentEditor)
                    .inUse(true)
                    .title(t)
                    .webContentType(webContentType)
                    .defaultDeletableContent(true)
                    .build();

            if (isReferenceType(webContentType)) {
                String link = linksList.get(random.nextInt(linksList.size()));
                String quote = quotes.get(random.nextInt(quotes.size()));
                context.setVariable("link", link);
                context.setVariable("quote", quote);
                String html = templateEngine.process(StartupPageTemplateName.DEFAULT_REFERENCE.getName(), context);
                String raw = Jsoup.parse(html).text();

                webContent.setHtmlContent(html);
                webContent.setRawContent(raw);
            } else {
                String html = templateEngine.process(StartupPageTemplateName.DEFAULT_ARTICLE.getName(), context);
                String raw = Jsoup.parse(html).text();
                webContent.setRawContent(raw);
                webContent.setHtmlContent(html);
            }

            LocalDateTime publishingDate = LocalDateTime.now().minusDays(random.nextInt(1825))
                    .plusMinutes(random.nextInt(60))
                    .plusSeconds(random.nextInt(60))
                    .minusDays(150)
                    .plusDays(random.nextInt(149));
            LocalDateTime editionDate = publishingDate.plusDays(random.nextInt(99));

            int index = titlesList.indexOf(t);
            if (index <= titlesList.size() * 0.7) {
                webContent.setAcceptsComments(true);
                webContent.setSubmittedForPublication(false);
                webContent.setPublished(true);
                webContent.setPublishingDate(publishingDate);
                webContent.setEditionDate(editionDate);
            } else if (index <= titlesList.size() * 0.8) {
                webContent.setAcceptsComments(false);
                webContent.setSubmittedForPublication(false);
                webContent.setPublished(true);
                webContent.setPublishingDate(publishingDate);
                webContent.setEditionDate(editionDate);
            } else if (index <= titlesList.size() * 0.9) {
                webContent.setAcceptsComments(true);
                webContent.setSubmittedForPublication(true);
                webContent.setPublished(false);
                webContent.setPublishingDate(null);
                webContent.setEditionDate(editionDate);
            } else {
                webContent.setAcceptsComments(false);
                webContent.setSubmittedForPublication(false);
                webContent.setPublished(false);
                webContent.setPublishingDate(null);
                webContent.setEditionDate(editionDate);
            }

            toSaveWebContents.add(webContent);
        });


        List<WebContent> savedWebContents = new ArrayList<>(webContentRepository.saveAll(toSaveWebContents));

        //adding the version id
        savedWebContents.forEach(wc -> wc.setVersionId(wc.getId()));
        webContentRepository.saveAll(savedWebContents);

        for (WebContentCategory contentCategory : webContentCategories) {
            CopyOnWriteArrayList<WebContent> categoryWebContents = webContentRepository
                    .findAllByWebContentCategoriesContains(contentCategory);

            if (webContentCategories.indexOf(contentCategory) == 0) {
                categoryWebContents.addAllAbsent(savedWebContents);
            } else {
                List<WebContent> webContents = savedWebContents;
                webContents.removeIf(wc -> webContents.indexOf(wc) > (savedWebContents.size() / (webContentCategories.indexOf(contentCategory))));
                categoryWebContents.addAllAbsent(webContents);
            }

            addWebContentsToCategory(contentCategory, categoryWebContents);
        }

        updateTypeTotalWebContents();
        updateCategoryTotalWebContents();
        updateUsersTotalWebContents();


    }


    private void updateTypeTotalWebContents() {
        List<WebContentType> webContentTypes = webContentTypeRepository.findAll();
        webContentTypes.forEach(wct -> {
            List<WebContent> webContents = webContentRepository.findAllByWebContentTypeIdOrWebContentTypeWebContentTypeId(wct.getId(), wct.getId());
            wct.setTotalWebContents(webContents.size());
            wct.setTotalPublishedWebContents((int) webContents.stream().filter(wc -> wc.isPublished() && wc.getSuppressionDate() == null).count());
            wct.setTotalNotSubmittedForPublicationWebContents((int) webContents
                    .stream().filter(wc -> !wc.isPublished() && !wc.isSubmittedForPublication() && wc.getSuppressionDate() == null).count());
            wct.setTotalWaitingPublicationWebContents((int) webContents
                    .stream().filter(wc -> !wc.isPublished() && wc.isSubmittedForPublication() && wc.getSuppressionDate() == null).count());
            wct.setTotalDeletedWebContents((int) webContents.stream().filter(wc -> wc.getSuppressionDate() != null).count());
            webContentTypeRepository.save(wct);
        });
    }


    private void updateCategoryTotalWebContents() {
        List<WebContentCategory> webContentCategories = webContentCategoryRepository.findAll();
        webContentCategories.forEach(c -> {
            List<WebContent> webContents = webContentRepository.findAllByWebContentCategoriesContains(c);
            c.setTotalWebContents(webContents.size());
            c.setTotalPublishedWebContents((int) webContents.stream()
                    .filter(wc -> wc.isPublished() && wc.getSuppressionDate() == null).count());
            c.setTotalNotSubmittedForPublicationWebContents((int) webContents
                    .stream().filter(wc -> !wc.isPublished() && !wc.isSubmittedForPublication() && wc.getSuppressionDate() == null).count());
            c.setTotalWaitingPublicationWebContents((int) webContents
                    .stream().filter(wc -> !wc.isPublished() && wc.isSubmittedForPublication() && wc.getSuppressionDate() == null).count());
            c.setTotalDeletedWebContents((int) webContents.stream().filter(wc -> wc.getSuppressionDate() != null).count());
            webContentCategoryRepository.save(c);
        });
    }


    private void updateUsersTotalWebContents() {

        List<User> users = userRepository.findAll();
        users.forEach(u -> {
            List<WebContent> webContents = webContentRepository.findAllByUser(u);
            u.setTotalWebContents(webContents.size());
            u.setTotalPublishedWebContents((int) webContents.stream().filter(wc -> wc.isPublished() && wc.getSuppressionDate() == null).count());
            u.setTotalNotSubmittedForPublicationWebContents((int) webContents
                    .stream().filter(wc -> !wc.isPublished() && !wc.isSubmittedForPublication() && wc.getSuppressionDate() == null).count());
            u.setTotalWaitingPublicationWebContents((int) webContents
                    .stream().filter(wc -> !wc.isPublished() && wc.isSubmittedForPublication() && wc.getSuppressionDate() == null).count());
            u.setTotalDeletedWebContents((int) webContents.stream().filter(wc -> wc.getSuppressionDate() != null).count());
            userRepository.save(u);
        });
    }


    private CopyOnWriteArrayList<User> findAllEditors() throws NotFoundException {
        List<Role> editors = roleRepository.findAllByInUseIsTrueAndVersionId(DefaultVersionId.ROLE_EDITOR.getId());
        if (editors.isEmpty()) {
            throw new NotFoundException(RoleResponseMessage.NOT_FOUND.getMessage());
        }
        return userRepository
                .findAllByRolesContains(editors.get(0));
    }

    private User findAdmin() {
        List<Role> adminRoles = roleRepository.findAllByInUseIsTrueAndVersionId(DefaultVersionId.ROLE_ADMIN.getId());
        if (adminRoles.isEmpty()) {
            throw new NotFoundException(RoleResponseMessage.NOT_FOUND.getMessage());
        }
        List<User> admins = userRepository.findAllByRolesContains(adminRoles.get(0));
        if (admins.isEmpty()) {
            throw new NotFoundException(UserResponseMessage.NOT_FOUND.getMessage());
        }
        return admins.get(0);
    }

    public WebContentType findTypeInUseByVersionId(long versionId) {
        CopyOnWriteArrayList<WebContentType> webContentTypes = webContentTypeRepository
                .findAllByVersionIdAndInUseIsTrueAndSuppressionDateIsNull(versionId);
        if (webContentTypes.isEmpty()) {
            throw new NotFoundException(TypeResponseMessage.NOT_FOUND.getMessage());
        }
        return webContentTypes.get(0);
    }

    public List<WebContentType> findTypesUsableWithDefaultWebContents() {
        CopyOnWriteArrayList<WebContentType> webContentTypes = webContentTypeRepository
                .findAllByInUseIsTrue();

        return webContentTypes.stream()
                .filter(wct -> !wct.isHasLimitedModification())
                .filter(wct -> !wct.getVersionId().equals(DefaultVersionId.ARTICLE_TYPE.getId()))
                .filter(wct -> !wct.getVersionId().equals(DefaultVersionId.REFERENCE_TYPE.getId()))
                .collect(Collectors.toList());
    }

    public CopyOnWriteArrayList<WebContentCategory> findAllCategoriesInUse() {
        return webContentCategoryRepository.findAllByInUseIsTrue();
    }


    public void addWebContentsToCategory(WebContentCategory category, List<WebContent> webContents) {
        CopyOnWriteArrayList<WebContent> initialWebContents = webContentRepository.findAllByWebContentCategoriesContains(category);
        initialWebContents.addAllAbsent(webContents);
        category.setWebContents(initialWebContents);
        webContentCategoryRepository.save(category);
    }


    private CopyOnWriteArrayList<String> createTitlesList(int totalWebContents, Faker faker) {
        CopyOnWriteArrayList<String> titlesList = new CopyOnWriteArrayList<>();

        while (titlesList.size() < totalWebContents) {
            titlesList.add(faker.funnyName().toString());
            titlesList.add(faker.starTrek().location());
            titlesList.add(faker.lordOfTheRings().location());
            titlesList.add(faker.lordOfTheRings().character());
            titlesList.add(faker.dune().title());
            titlesList.add(faker.dragonBall().character());
            titlesList.add(faker.shakespeare().hamletQuote());
            titlesList.add(faker.shakespeare().kingRichardIIIQuote());
            titlesList.add(faker.shakespeare().asYouLikeItQuote());
            titlesList.add(faker.shakespeare().romeoAndJulietQuote());
            titlesList.add(faker.space().meteorite());
            titlesList.add(faker.space().agency());
            titlesList.add(faker.space().company());
            titlesList.add(faker.space().constellation());
            titlesList.add(faker.space().galaxy());
            titlesList.add(faker.book().author());
            titlesList.add(faker.book().publisher());
            titlesList.add(faker.book().title());
            titlesList.add(faker.name().bloodGroup());
        }
        return titlesList;
    }

    private CopyOnWriteArrayList<String> createLinksList(int totalWebContents, Faker faker) {
        CopyOnWriteArrayList<String> linksList = new CopyOnWriteArrayList<>();

        while (linksList.size() < totalWebContents) {
            linksList.add(faker.internet().url());
        }
        return linksList;
    }


    private CopyOnWriteArrayList<String> createQuotesList(Faker faker) {
        CopyOnWriteArrayList<String> quotes = new CopyOnWriteArrayList<>();

        while (quotes.size() < 1000) {
            quotes.add(faker.shakespeare().kingRichardIIIQuote());
            quotes.add(faker.shakespeare().hamletQuote());
            quotes.add(faker.shakespeare().asYouLikeItQuote());
            quotes.add(faker.shakespeare().romeoAndJulietQuote());
            quotes.add(faker.backToTheFuture().quote());
            quotes.add(faker.elderScrolls().quote());
            quotes.add(faker.elderScrolls().quote());
            quotes.add(faker.elderScrolls().quote());
            quotes.add(faker.lebowski().quote());
        }
        return quotes;
    }

    private boolean isReferenceType(WebContentType type) {
        return type.getVersionId().equals(DefaultVersionId.INTERNET_SITE_TYPE.getId())
                || type.getVersionId().equals(DefaultVersionId.BOOK_TYPE.getId());
    }

    private boolean isArticleType(WebContentType type) {
        return type.getVersionId().equals(DefaultVersionId.SCIENTIFIC_ARTICLES_TYPE.getId())
                || type.getVersionId().equals(DefaultVersionId.CULTURE_ARTICLES_TYPE.getId())
                || type.getVersionId().equals(DefaultVersionId.MEDICINE_ARTICLES_TYPE.getId())
                || type.getVersionId().equals(DefaultVersionId.ASTROPHYSICS_ARTICLES_TYPE.getId());
    }

    private void timeTest() {
        long start = System.currentTimeMillis();
        CopyOnWriteArrayList<WebContent> webContentsTest1 = webContentRepository
                .findAllByWebContentTypeIdOrWebContentTypeWebContentTypeIdAndPublishedIsTrueAndInUseIsTrueAndSuppressionDateIsNull
                        (4L, 4L, DateSortObject.ASC_CREATION_DATE);

        long end = System.currentTimeMillis();
        System.out.println("\tAzerty test 1 get filtered and sorted took " + (end - start) + "ms");
        System.out.println("\tTest 1 retrieved " + webContentsTest1.size() + " items");
        System.out.println("Ids : " + webContentsTest1.get(0).getId() + " " + webContentsTest1.get(45).getId());

        start = System.currentTimeMillis();
        CopyOnWriteArrayList<WebContent> webContentsTest2 = webContentRepository
                .findAllByWebContentTypeIdOrWebContentTypeWebContentTypeIdAndPublishedIsTrueAndInUseIsTrueAndSuppressionDateIsNull
                        (4L, 4L);
        webContentsTest2.sort(Comparator.comparing(WebContent::getCreationDate));
        end = System.currentTimeMillis();
        System.out.println("\tAzerty test 2 get filtered and sort with java took " + (end - start) + " ms");
        System.out.println("\tTest 2 retrieved " + webContentsTest2.size() + " items");
        System.out.println("Ids : " + webContentsTest2.get(0).getId() + " " + webContentsTest2.get(45).getId());

        start = System.currentTimeMillis();
        CopyOnWriteArrayList<WebContent> webContentsTest3 = webContentRepository
                .findAllByWebContentTypeIdOrWebContentTypeWebContentTypeId
                        (4L, 4L, DateSortObject.ASC_CREATION_DATE);
        webContentsTest3.removeIf(wc -> !wc.isPublished() || !wc.isInUse() || wc.getSuppressionDate() != null);
        end = System.currentTimeMillis();
        System.out.println("\tAzerty test 3 get by type sorted, then filter  with java took " + (end - start) + " ms");
        System.out.println("\tTest 3 retrieved " + webContentsTest3.size() + " items");
        System.out.println("Ids : " + webContentsTest3.get(0).getId() + " " + webContentsTest3.get(45).getId());


        start = System.currentTimeMillis();
        CopyOnWriteArrayList<WebContent> webContentsTest4 = webContentRepository
                .findAllByWebContentTypeIdOrWebContentTypeWebContentTypeId
                        (4L, 4L);
        webContentsTest4.removeIf(wc -> !wc.isPublished() || !wc.isInUse() || wc.getSuppressionDate() != null);
        webContentsTest4.sort(Comparator.comparing(WebContent::getCreationDate));
        end = System.currentTimeMillis();
        System.out.println("\tAzerty test 4 get by type, then filter and sort with java took " + (end - start) + " ms");
        System.out.println("\tTest 4 retrieved " + webContentsTest4.size() + " items");
        System.out.println("Ids : " + webContentsTest4.get(0).getId() + " " + webContentsTest4.get(45).getId());


    }

}
