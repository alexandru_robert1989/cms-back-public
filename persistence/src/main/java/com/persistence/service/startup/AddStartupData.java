package com.persistence.service.startup;


/**
 * Interface providing the abstract methods that can be implemented
 * to add the data needed on application startup.
 */
public interface AddStartupData {

    /**
     * Method adding all the needed data on application startup.
     */
    void addStartupData();


}
