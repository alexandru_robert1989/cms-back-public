package com.persistence.service.startup.impl;


import com.domain.entity.Comment;
import com.domain.entity.User;
import com.domain.entity.WebContent;
import com.github.javafaker.Faker;
import com.persistence.repository.CommentRepository;
import com.persistence.repository.UserRepository;
import com.persistence.repository.WebContentRepository;
import com.persistence.service.startup.StartupCommentService;
import lombok.AllArgsConstructor;
import lombok.Lombok;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Service providing the global  methods used for interacting with
 * {@link Comment}s.
 * <p>
 * The all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */
@AllArgsConstructor
@Service
public class StartupCommentServiceImpl implements StartupCommentService {

    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final WebContentRepository webContentRepository;


    @Override
    public boolean addStartupTestComments(int maxReactionsByComment, int maxCommentByWc) {

        if (!commentRepository.findAll().isEmpty()) {
            updateWebContentsTotalComments();
            updateUsersTotalComments();
            return false;
        }

        List<User> users = userRepository.findAll();
        List<WebContent> webContents = webContentRepository.findAll();
        webContents.removeIf(wc -> !wc.isInUse());
        webContents.removeIf(wc -> !wc.isAcceptsComments());

        if (users.isEmpty() || webContents.isEmpty()) {
            return false;
        }

        Faker faker = new Faker();
        Random random = new Random();


        List<String> titlesList = new ArrayList<>();
        while (titlesList.size() < 1000) {
            titlesList.add(faker.book().genre());
            titlesList.add(faker.space().galaxy());
            titlesList.add(faker.space().constellation());
            titlesList.add(faker.elderScrolls().creature());
            titlesList.add(faker.elderScrolls().city());
        }

        List<String> textsList = new ArrayList<>();
        while (textsList.size() < 1000) {
            textsList.add(faker.elderScrolls().quote());
            textsList.add(faker.shakespeare().romeoAndJulietQuote());
            textsList.add(faker.shakespeare().asYouLikeItQuote());
            textsList.add(faker.shakespeare().hamletQuote());
            textsList.add(faker.shakespeare().kingRichardIIIQuote());
            textsList.add(faker.backToTheFuture().quote());
            textsList.add(faker.book().genre());
        }

        CopyOnWriteArrayList<Comment> comments = new CopyOnWriteArrayList<>();

        webContents.forEach(wc -> {

            List<Comment> savedParentComments = new CopyOnWriteArrayList<>();
            List<Comment> savedChildrenComments = new CopyOnWriteArrayList<>();

            // adding parent comments
            for (int i = 0; i < random.nextInt(maxCommentByWc); i++) {

                boolean isPublished = i % 2 != 0;
                Comment parentComment = new Comment();
                parentComment.setDefaultDeletableContent(true);
                String title = titlesList.get(random.nextInt(titlesList.size()));
                String text = textsList.get(random.nextInt(textsList.size()));
                User user = users.get(random.nextInt(users.size()));
                parentComment.setTitle(title);
                parentComment.setText(text);
                parentComment.setUser(user);
                parentComment.setWebContent(wc);

                if (isPublished) {
                    parentComment.setPublishingDate(LocalDateTime.now());
                }
                savedParentComments.add(commentRepository.save(parentComment));
            }


            comments.addAllAbsent(savedParentComments);

            for (Comment comment : savedParentComments) {

                for (int i = 0; i < random.nextInt(maxReactionsByComment); i++) {

                    boolean isPublished = i % 2 != 0;
                    Comment childrenComment = new Comment();
                    childrenComment.setDefaultDeletableContent(true);
                    String title = titlesList.get(random.nextInt(titlesList.size()));
                    String text = textsList.get(random.nextInt(textsList.size()));
                    User user = users.get(random.nextInt(users.size()));
                    childrenComment.setTitle(title);
                    childrenComment.setText(text);
                    childrenComment.setUser(user);
                    childrenComment.setWebContent(wc);
                    childrenComment.setParentComment(comment);
                    if (isPublished && comment.getPublishingDate() != null) {
                        childrenComment.setPublishingDate(LocalDateTime.now());
                    }
                    savedChildrenComments.add(commentRepository.save(childrenComment));
                }
                comments.addAllAbsent(savedChildrenComments);
            }
        });

        List<WebContent> webContentList = webContentRepository.findAll();


        updateWebContentsTotalComments();
        updateUsersTotalComments();
        return true;
    }


    private void updateWebContentsTotalComments() {

        List<WebContent> webContents = webContentRepository.findAll();

        webContents.forEach(wc -> {
            List<Comment> comments = commentRepository.findAllByWebContent(wc);
            wc.setTotalComments(comments.size());
            wc.setTotalPublishedComments((int) comments.stream().filter(c -> c.getPublishingDate() != null && c.getSuppressionDate() == null).count());
            wc.setTotalDeletedComments((int) comments.stream().filter(c -> c.getSuppressionDate() != null).count());
            wc.setTotalWaitingPublicationComments((int) comments.stream().filter(c -> c.getSuppressionDate() == null && c.getPublishingDate() == null).count());
            webContentRepository.save(wc);

        });
    }


    private void updateUsersTotalComments() {
        List<User> users = userRepository.findAll();

        users.forEach(u -> {
            List<Comment> comments = commentRepository.findAllByUser(u);
            u.setTotalSentComments(comments.size());
            u.setTotalPublishedComments((int) comments.stream().filter(c -> c.getPublishingDate() != null && c.getSuppressionDate() == null).count());
            u.setTotalDeletedComments((int) comments.stream().filter(c -> c.getSuppressionDate() != null).count());
            u.setTotalWaitingPublicationComments((int) comments.stream().filter(c -> c.getSuppressionDate() == null && c.getPublishingDate() == null).count());
            userRepository.save(u);


        });


    }


}
