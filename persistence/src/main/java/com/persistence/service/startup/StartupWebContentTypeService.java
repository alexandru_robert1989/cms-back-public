package com.persistence.service.startup;

import com.domain.entity.WebContentType;
import com.persistence.exception.NotFoundException;

import javax.validation.ConstraintViolationException;

/**
 * Interface providing the global abstract methods that can be
 * implemented to allow a visitor to interact with
 * {@link WebContentType} objects.
 */
public interface StartupWebContentTypeService {


    WebContentType saveNew(WebContentType webContentType) throws ConstraintViolationException;

    WebContentType findOneInUseByVersionId(long versionId);

    WebContentType findOneInUseByTitle(String title) throws NotFoundException;

    WebContentType addStartupTypeWithDefaultId(String title, String description,
                                               boolean isNavbarItem, boolean isFooterItem, boolean isPublished, boolean hasLimitedModification,
                                               long defaultId, long parentTypeId) throws NotFoundException;

    boolean addStartupTypes();


}
