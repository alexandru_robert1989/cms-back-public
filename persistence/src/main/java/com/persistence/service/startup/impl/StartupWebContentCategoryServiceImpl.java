package com.persistence.service.startup.impl;


import com.domain.entity.WebContentCategory;
import com.persistence.constant.DefaultDate;
import com.persistence.constant.DefaultStartupWebContentCategoryTitle;
import com.persistence.repository.WebContentCategoryRepository;
import com.persistence.service.startup.StartupWebContentCategoryService;
import lombok.AllArgsConstructor;
import lombok.Lombok;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Service providing the methods used by admins for interacting with
 * {@link WebContentCategory}es.
 * <p>
 * The all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */
@Service
@AllArgsConstructor
public class StartupWebContentCategoryServiceImpl implements StartupWebContentCategoryService {

    private final WebContentCategoryRepository webContentCategoryRepository;

    @Override
    public WebContentCategory create(WebContentCategory webContentCategory) throws ConstraintViolationException {
        webContentCategory.setInUse(true);
        webContentCategory = webContentCategoryRepository.save(webContentCategory);
        webContentCategory.setVersionId(webContentCategory.getId());
        return webContentCategoryRepository.save(webContentCategory);
    }


    @Override
    public void addStartupCategories() {
        if (webContentCategoryRepository.findAll().isEmpty()) {


            WebContentCategory history = WebContentCategory.builder()
                    .defaultDeletableContent(true)
                    .title(DefaultStartupWebContentCategoryTitle.HISTORY.getFrenchTitle())
                    .description("Présentation du passé de l'humanité et des sociétés humaines")
                    .published(true)
                    .inUse(true)
                    .editionDate(DefaultDate.DEFAULT_STARTUP_CREATION_DATE)
                    .creationDate(DefaultDate.DEFAULT_STARTUP_CREATION_DATE)
                    .build();

            create(history);

            WebContentCategory science = WebContentCategory.builder()
                    .defaultDeletableContent(true)
                    .title(DefaultStartupWebContentCategoryTitle.SCIENCE.getFrenchTitle())
                    .description("Présentation des connaissance approfondies d'un domaine quelconque, acquises par la réflexion ou l'expérience")
                    .published(true)
                    .inUse(true)
                    .editionDate(DefaultDate.DEFAULT_STARTUP_CREATION_DATE)
                    .creationDate(DefaultDate.DEFAULT_STARTUP_CREATION_DATE)
                    .build();
            create(science);

            WebContentCategory medicine = WebContentCategory.builder()
                    .defaultDeletableContent(true)
                    .title(DefaultStartupWebContentCategoryTitle.MEDICINE.getFrenchTitle())
                    .description("La médecine est la science qui a pour objet la conservation et le rétablissement de la santé")
                    .published(true)
                    .inUse(true)
                    .editionDate(DefaultDate.DEFAULT_STARTUP_CREATION_DATE)
                    .creationDate(DefaultDate.DEFAULT_STARTUP_CREATION_DATE)
                    .build();
            create(medicine);

            WebContentCategory culture = WebContentCategory.builder()
                    .defaultDeletableContent(true)
                    .title(DefaultStartupWebContentCategoryTitle.CULTURE.getFrenchTitle())
                    .description("Présentation des articles dont le rôle est d'enrichir l'esprit par des exercices intellectuels")
                    .published(true)
                    .inUse(true)
                    .editionDate(DefaultDate.DEFAULT_STARTUP_CREATION_DATE)
                    .creationDate(DefaultDate.DEFAULT_STARTUP_CREATION_DATE)
                    .build();
            create(culture);
        }
    }


}
