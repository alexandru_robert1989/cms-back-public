package com.persistence.service.startup.impl;

import com.domain.entity.WebContentType;
import com.persistence.constant.DefaultStartupWebContentTypeTitle;
import com.persistence.constant.DefaultVersionId;
import com.persistence.exception.NotFoundException;
import com.persistence.repository.WebContentTypeRepository;
import com.persistence.service.startup.StartupWebContentTypeService;
import lombok.AllArgsConstructor;
import lombok.Lombok;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Service providing the global  methods used for interacting with
 * {@link WebContentType}s
 *
 * <p>
 * The all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */
@AllArgsConstructor
@Service
public class StartupWebContentTypeServiceImpl implements StartupWebContentTypeService {


    private final WebContentTypeRepository webContentTypeRepository;

    @Override
    public WebContentType saveNew(WebContentType webContentType) throws ConstraintViolationException {
        WebContentType savedWct = webContentTypeRepository.save(webContentType);
        savedWct.setVersionId(savedWct.getId());
        savedWct.setInUse(true);
        return webContentTypeRepository.save(savedWct);
    }


    @Override
    public WebContentType findOneInUseByVersionId(long versionId) {
        CopyOnWriteArrayList<WebContentType> webContentTypes = webContentTypeRepository
                .findAllByVersionIdAndInUseIsTrueAndSuppressionDateIsNull(versionId);
        if (webContentTypes.isEmpty()) {
            return null;
        }
        return webContentTypes.get(0);
    }


    @Override
    public WebContentType findOneInUseByTitle(String title) throws NotFoundException {
        CopyOnWriteArrayList<WebContentType> webContentTypes = webContentTypeRepository
                .findAllByTitleAndInUseIsTrue(title);
        if (webContentTypes.isEmpty()) {
            throw new NotFoundException();
        } else {
            long versionId = webContentTypes.get(0).getVersionId();
            CopyOnWriteArrayList<WebContentType> webContentTypes1 = webContentTypeRepository
                    .findAllByVersionIdAndInUseIsTrueOrderByEditionDateDesc(versionId);
            if (webContentTypes1.isEmpty()) {
                throw new NotFoundException();
            }
            return webContentTypes1.get(0);
        }
    }

    @Override
    public WebContentType addStartupTypeWithDefaultId(String title, String description,
                                                      boolean isNavbarItem, boolean isFooterItem,
                                                      boolean isPublished, boolean hasLimitedModification,
                                                      long defaultId, long parentTypeId) throws NotFoundException {

        WebContentType webContentType = WebContentType.builder()
                .title(title)
                .description(description)
                .isNavbarItem(isNavbarItem)
                .isFooterItem(isFooterItem)
                .published(isPublished)
                .id(defaultId)
                .versionId(defaultId)
                .hasLimitedModification(hasLimitedModification)
                .creationDate(LocalDateTime.now())
                .editionDate(LocalDateTime.now()).build();

        if (!isFooterItem && !isNavbarItem && parentTypeId != -99) {
            WebContentType parentType = findOneInUseByVersionId(parentTypeId);
            webContentType.setWebContentType(parentType);
        }
        return saveNew(webContentType);

    }



    @Override
    public boolean addStartupTypes() {

        if (!webContentTypeRepository.findAll().isEmpty()) {
            return false;
        }


        /* unique items, such as the Home Page and Legal Mentions Page*/

        addStartupTypeWithDefaultId(DefaultStartupWebContentTypeTitle.HOME.getFrenchTitle(),
                "Type de contenu affiché sur la page d'accueil",
                false, false, false,
                true, DefaultVersionId.HOME_PAGE_TYPE.getId(), -99);

        addStartupTypeWithDefaultId(DefaultStartupWebContentTypeTitle.LEGAL_MENTIONS.getFrenchTitle(),
                "Type de contenu présentant les mentions légales de l'utilisation du site ",
                false, false, false, true,
                DefaultVersionId.LEGAL_MENTIONS_WEB_CONTENT.getId(), -99);


        addStartupTypeWithDefaultId(DefaultStartupWebContentTypeTitle.INDICATIONS.getFrenchTitle(),
                "Type de contenu présentant les indications pour l'utilisation du site ",
                false, false, false, true,
                DefaultVersionId.INDICATIONS_TYPE.getId(), -99);


        /*navbar main items */

        addStartupTypeWithDefaultId(DefaultStartupWebContentTypeTitle.ARTICLES.getFrenchTitle(),
                "Les différentes types d'articles disponibles sur le site",
                true, false, true,
                false, DefaultVersionId.ARTICLE_TYPE.getId(), -99);

        addStartupTypeWithDefaultId(DefaultStartupWebContentTypeTitle.REFERENCES.getFrenchTitle(),
                "Les différentes types de références disponibles sur le site",
                true, false, true,
                false, DefaultVersionId.REFERENCE_TYPE.getId(), -99);

        /*navbar Référence subtypes */

        addStartupTypeWithDefaultId(DefaultStartupWebContentTypeTitle.INTERNET_SITES.getFrenchTitle(),
                "Des liens vers d'autres sites Internet",
                false, false, true, false,
                DefaultVersionId.INTERNET_SITE_TYPE.getId(), DefaultVersionId.REFERENCE_TYPE.getId());

        this.addStartupTypeWithDefaultId(DefaultStartupWebContentTypeTitle.BOOKS.getFrenchTitle(),
                "Des liens vers la présentation de livres",
                false, false, true, false,
                DefaultVersionId.BOOK_TYPE.getId(), DefaultVersionId.REFERENCE_TYPE.getId());



        /*navbar Article subtypes */

        addStartupTypeWithDefaultId(DefaultStartupWebContentTypeTitle.SCIENTIFIC_ARTICLES.getFrenchTitle(),
                "Des articles scientifiques",
                false, false, true, false,
                DefaultVersionId.SCIENTIFIC_ARTICLES_TYPE.getId(), DefaultVersionId.ARTICLE_TYPE.getId());

        addStartupTypeWithDefaultId(DefaultStartupWebContentTypeTitle.CULTURE_ARTICLES.getFrenchTitle(),
                "Des articles de culture générale",
                false, false, true, false,
                DefaultVersionId.CULTURE_ARTICLES_TYPE.getId(),  DefaultVersionId.ARTICLE_TYPE.getId());

        addStartupTypeWithDefaultId(DefaultStartupWebContentTypeTitle.MEDICINE_ARTICLES.getFrenchTitle(),
                "Des articles du domaine de la médecine",
                false, false, true, false,
                DefaultVersionId.MEDICINE_ARTICLES_TYPE.getId(),  DefaultVersionId.ARTICLE_TYPE.getId());

        addStartupTypeWithDefaultId(DefaultStartupWebContentTypeTitle.ASTROPHYSICS_ARTICLES.getFrenchTitle(),
                "Des articles du domaine de l'astrophysique",
                false, false, true, false,
                DefaultVersionId.ASTROPHYSICS_ARTICLES_TYPE.getId(),  DefaultVersionId.ARTICLE_TYPE.getId());


        return true;
    }


}
