package com.persistence.service.startup.impl;

import com.domain.entity.Role;
import com.persistence.constant.DefaultVersionId;
import com.persistence.repository.RoleRepository;
import com.persistence.service.startup.StartupRoleService;
import lombok.AllArgsConstructor;
import lombok.Lombok;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * Service providing the global methods used for handling
 * {@link Role}s .
 * <p>
 * The all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */
@Service
@AllArgsConstructor
public class StartupRoleServiceImpl implements StartupRoleService {

    private final RoleRepository roleRepository;

    @Override
    public boolean addStartupRoles() {

        if (!roleRepository.findAll().isEmpty()) {
            return false;
        }

        LocalDateTime now = LocalDateTime.now();

        Role user = Role.builder()
                .title("ROLE_USER")
                .description("Un user (utilisateur) est un utilisateur qui peut laisser des commentaires" +
                        "et qui peut utiliser la rubrique Contactez-nous")
                .inUse(true)
                .creationDate(now)
                .editionDate(now)
                .versionId(DefaultVersionId.ROLE_USER.getId())
                .build();
        roleRepository.save(user);

        Role editor = Role.builder()
                .title("ROLE_EDITOR")
                .description("Un editor (rédacteur) est un utilisateur qui peut laisser des commentaires," +
                        " qui peut utiliser la rubrique Contactez-nous, et qui peut écrire des articles " +
                        "(WebContent), pour ensuite les soumettre pour publication")
                .inUse(true)
                .creationDate(now)
                .editionDate(now)
                .versionId(DefaultVersionId.ROLE_EDITOR.getId())
                .build();
        roleRepository.save(editor);

        Role publisher = Role.builder()
                .title("ROLE_PUBLISHER")
                .description("Un publisher (éditeur) est un utilisateur qui peut laisser des commentaires," +
                        " qui peut utiliser la rubrique Contactez-nous, et qui peut publier des articles (WebContent)")
                .inUse(true)
                .creationDate(now)
                .editionDate(now)
                .versionId(DefaultVersionId.ROLE_PUBLISHER.getId())
                .build();
        roleRepository.save(publisher);

        Role moderator = Role.builder()
                .title("ROLE_MODERATOR")
                .description("Un moderator (modérateur) est un utilisateur qui peut laisser des commentaires," +
                        " qui peut utiliser la rubrique Contactez-nous, et qui peut modérer les commentaires et les images de profil")
                .inUse(true)
                .creationDate(now)
                .editionDate(now)
                .versionId(DefaultVersionId.ROLE_MODERATOR.getId())
                .build();
        roleRepository.save(moderator);

        Role admin = Role.builder()
                .title("ROLE_ADMIN")
                .description("Un admin (administrateur) est un utilisateur qui peut laisser des commentaires," +
                        " qui peut utiliser la rubrique Contactez-nous, qui peut modérer les commentaires et les images de profil, qui peut gérer " +
                        "le contenu affiché sur le site (sans avoir accès au contenu supprimé), et qui peut également supprimer et créer les comptes" +
                        "des utilisateurs avec un rôle plus bas que le sien")
                .inUse(true)
                .creationDate(now)
                .editionDate(now)
                .versionId(DefaultVersionId.ROLE_ADMIN.getId())
                .build();
        roleRepository.save(admin);

        Role superAdmin = Role.builder()
                .title("ROLE_SUPER_ADMIN")
                .description("Un superAdmin (super administrateur) est un utilisateur qui peut laisser " +
                        "des commentaires, qui peut utiliser la rubrique Contactez-nous, qui peut modérer les commentaires et les images de profil, " +
                        "qui peut gérer tout le contenu affiché sur le site, qui peut restaurer le contenu supprimé du site" +
                        " et qui peut également supprimer et créer les comptes" +
                        "des utilisateurs avec un rôle plus bas que le sien")
                .inUse(true)
                .creationDate(now)
                .editionDate(now)
                .versionId(DefaultVersionId.ROLE_SUPER_ADMIN.getId())
                .build();
        roleRepository.save(superAdmin);

        return true;
    }



}
