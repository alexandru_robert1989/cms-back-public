package com.persistence.service.admin;

import com.domain.entity.WebContent;
import com.domain.entity.WebContentCategory;
import com.persistence.exception.SuppressionException;
import org.springframework.data.domain.Sort;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Interface providing the abstract methods used by admins for handling
 * {@link WebContentCategory}es.
 */
public interface AdminWebContentCategoryService {


    /**
     * Save a new {@link WebContentCategory}.
     *
     * @param webContentCategory a {@link WebContentCategory}
     * @return a {@link WebContentCategory}
     * @throws ConstraintViolationException exception thrown if one or more  persistence constraints are not respected
     */
    WebContentCategory create(WebContentCategory webContentCategory) throws ConstraintViolationException;


    /**
     * Save a new version of a {@link WebContentCategory}.
     *
     * @param webContentCategory a {@link WebContentCategory}
     * @return a {@link WebContentCategory}
     * @throws ConstraintViolationException exception thrown if one or more  persistence constraints are not respected
     */
    WebContentCategory saveLastVersion(WebContentCategory webContentCategory) throws ConstraintViolationException;


    /**
     * Modify a {@link WebContentCategory}, after having saved the version of the {@link WebContentCategory} before modification.
     *
     * @param webContentCategory a {@link WebContentCategory}
     * @return a {@link WebContentCategory}
     * @throws ConstraintViolationException exception thrown if one or more  persistence constraints are not respected
     */
    WebContentCategory modify(WebContentCategory webContentCategory) throws ConstraintViolationException;


    /**
     * Add a suppression date to a in use {@link WebContentCategory}
     *
     * @param versionId a long value representing the version id of a {@link WebContentCategory}
     * @return a boolean value indication if the method succeeded or not
     * @throws SuppressionException a {@link SuppressionException}, thrown if suppression fails
     */
    boolean deleteOneInUseFromDisplay(long versionId) throws SuppressionException;


    /**
     * Add a suppression date to a specific version of a  {@link WebContentCategory}
     *
     * @param id a long value representing the id of a {@link WebContentCategory}
     * @return a boolean value indication if the method succeeded or not
     * @throws SuppressionException a {@link SuppressionException}, thrown if suppression fails
     */
    boolean deleteOneVersionFromDisplay(long id) throws SuppressionException;


    /**
     * Find a {@link WebContentCategory} by its id.
     *
     * @param id a long value representing a {@link WebContentCategory} id
     * @return a {@link WebContentCategory}
     */
    WebContentCategory findById(long id);


    /**
     * Find all the not suppressed versions of a {@link WebContentCategory}.
     *
     * @param versionId a long value representing a {@link WebContentCategory} version id
     * @return a {@link List}of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllNotSuppressedVersions(long versionId, Sort sort);

    /**
     * Find a not suppressed {@link WebContentCategory} version by its id.
     *
     * @param id a long value representing a {@link WebContentCategory} id
     * @return a {@link WebContentCategory}
     */
    WebContentCategory findOneNotSuppressedVersion(long id);


    /**
     * Method returning a in use (published or not) {@link WebContentCategory} version found by its id,
     *
     * @param versionId a {@link Long} value representing the version id of a {@link WebContentCategory}
     * @return a {@link WebContentCategory}
     */
    WebContentCategory findOneNotSuppressedInUseByVersionId(long versionId);



    /**
     * Find all the {@link WebContentCategory}es created by default on startup.
     *
     * @return a {@link List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findStartupCategories();


    /**
     * Delete from display all the {@link WebContentCategory}es created by default on startup.
     *
     * @return a boolean value indicating if all the startup {@link WebContentCategory}es were deleted from display
     * @throws SuppressionException a {@link SuppressionException}, thrown if suppression fails
     */
    boolean deleteAllStartupCategoriesFromDisplay() throws SuppressionException;


    /**
     * Method adding a {@link WebContent} to a {@link WebContentCategory}
     *
     * @param category   a {@link WebContentCategory}
     * @param webContent a {@link WebContent}
     * @return a boolean value indicating if updating the {@link WebContent}s succeeded
     */
    boolean addWebContent(WebContentCategory category, WebContent webContent);


    /**
     * Method adding {@link WebContent}s to a {@link WebContentCategory}
     *
     * @param category    a {@link WebContentCategory}
     * @param webContents a {@link List} of {@link WebContent}s
     * @return a boolean value indicating if updating the {@link WebContent}s succeeded
     */
    boolean addWebContents(WebContentCategory category, List<WebContent> webContents);


    /**
     * Method removing a {@link WebContent} from a {@link WebContentCategory}
     *
     * @param category   a {@link WebContentCategory}
     * @param webContent a {@link WebContent}
     * @return a boolean value indicating if updating the {@link WebContent}s succeeded
     */
    boolean removeWebContent(WebContentCategory category, WebContent webContent);


    /**
     * Method removing {@link WebContent}s from a {@link WebContentCategory}
     *
     * @param category    a {@link WebContentCategory}
     * @param webContents a {@link List} of {@link WebContent}s
     * @return a boolean value indicating if updating the {@link WebContent}s succeeded
     */
    boolean removeWebContents(WebContentCategory category, List<WebContent> webContents);


    /**
     * Method returning a {@link WebContentCategory} with its properties containing
     * all the properties that a admin can use.
     *
     * @param webContentCategory a {@link WebContentCategory}
     */
    void setCategoryProperties(WebContentCategory webContentCategory);





}
