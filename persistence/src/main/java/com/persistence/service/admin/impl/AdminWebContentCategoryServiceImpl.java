package com.persistence.service.admin.impl;


import com.domain.constant.response_message.CategoryResponseMessage;
import com.domain.entity.WebContent;
import com.domain.entity.WebContentCategory;
import com.persistence.constant.*;
import com.persistence.constant.sort_object.TitleSortObject;
import com.persistence.exception.SuppressionException;

import com.persistence.repository.WebContentCategoryRepository;
import com.persistence.repository.WebContentRepository;
import com.persistence.service.admin.AdminWebContentCategoryService;
import lombok.AllArgsConstructor;
import lombok.Lombok;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Service providing the methods used by admins for interacting with
 * {@link WebContentCategory}es.
 * <p>
 * The all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */
@Service
@AllArgsConstructor
public class AdminWebContentCategoryServiceImpl implements AdminWebContentCategoryService {

    /**
     * Repository used for handling  {@link WebContentCategory}es.
     */
    private final WebContentCategoryRepository webContentCategoryRepository;


    /**
     * Repository used for handling  {@link WebContent}s.
     */
    private final WebContentRepository webContentRepository;


    @Override
    public WebContentCategory create(WebContentCategory webContentCategory) throws ConstraintViolationException {
        webContentCategory.setInUse(true);
        webContentCategory = webContentCategoryRepository.save(webContentCategory);
        webContentCategory.setVersionId(webContentCategory.getId());
        return webContentCategoryRepository.save(webContentCategory);
    }


    @Override
    public WebContentCategory saveLastVersion(WebContentCategory webContentCategory) throws ConstraintViolationException {
        webContentCategory.setInUse(false);
        webContentCategory.setId(null);
        return webContentCategoryRepository.save(webContentCategory);
    }


    @Override
    public WebContentCategory modify(WebContentCategory webContentCategory) throws ConstraintViolationException {
        WebContentCategory oldCategory = findOneNotSuppressedInUseByVersionId(webContentCategory.getVersionId());
        if (oldCategory == null) {
            return null;
        }
        saveLastVersion(oldCategory);
        oldCategory.setEditionDate(LocalDateTime.now());
        oldCategory.setTitle(webContentCategory.getTitle());
        oldCategory.setDescription(webContentCategory.getDescription());
        oldCategory.setPublished(webContentCategory.isPublished());
        return webContentCategoryRepository.save(oldCategory);
    }


    @Override
    public boolean deleteOneInUseFromDisplay(long versionId) throws SuppressionException {
        WebContentCategory category = findOneNotSuppressedInUseByVersionId(versionId);
        if (category == null) {
            throw new SuppressionException(CategoryResponseMessage.ALREADY_DELETED.getMessage());
        }
        saveLastVersion(category);
        LocalDateTime now = LocalDateTime.now();
        category.setSuppressionDate(now);
        category.setEditionDate(now);
        category = webContentCategoryRepository.save(category);
        if (category.getSuppressionDate() != null) {
            return true;
        }
        throw new SuppressionException(CategoryResponseMessage.DELETING_ONE_FAILED.getMessage());
    }


    @Override
    public boolean deleteOneVersionFromDisplay(long id) throws SuppressionException {
        WebContentCategory category = webContentCategoryRepository.findByIdAndSuppressionDateIsNull(id);
        if (category == null) {
            throw new SuppressionException(CategoryResponseMessage.ALREADY_DELETED.getMessage());
        }
        LocalDateTime now = LocalDateTime.now();
        category.setSuppressionDate(now);
        category.setEditionDate(now);
        category = webContentCategoryRepository.save(category);
        if (category.getSuppressionDate() != null) {
            return true;
        }
        throw new SuppressionException(CategoryResponseMessage.DELETING_ONE_FAILED.getMessage());
    }


    @Override
    public WebContentCategory findById(long id) {
        Optional<WebContentCategory> optional = webContentCategoryRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        }
        return null;
    }


    @Override
    public CopyOnWriteArrayList<WebContentCategory> findAllNotSuppressedVersions(long versionId, Sort sort) {
        return webContentCategoryRepository
                .findAllByVersionIdAndSuppressionDateIsNull(versionId, sort);
    }


    @Override
    public WebContentCategory findOneNotSuppressedVersion(long id) {
        WebContentCategory webContentCategory = webContentCategoryRepository.findByIdAndSuppressionDateIsNull(id);
        if (webContentCategory == null || webContentCategory.isInUse()) {
            return null;
        }
        return webContentCategory;
    }


    @Override
    public WebContentCategory findOneNotSuppressedInUseByVersionId(long versionId) {
        CopyOnWriteArrayList<WebContentCategory> webContentCategories = webContentCategoryRepository
                .findAllByVersionIdAndInUseIsTrueAndSuppressionDateIsNull(versionId);
        if (webContentCategories.isEmpty()) {
            return null;
        }
        return webContentCategories.get(0);
    }




    @Override
    public CopyOnWriteArrayList<WebContentCategory> findStartupCategories() {
        return webContentCategoryRepository.findAllByEditionDate(DefaultDate.DEFAULT_STARTUP_CREATION_DATE);
    }


    @Override
    public boolean deleteAllStartupCategoriesFromDisplay() throws SuppressionException {
        CopyOnWriteArrayList<WebContentCategory> webContentCategories = findStartupCategories();
        if (webContentCategories.isEmpty()) {
            return true;
        }
        LocalDateTime now = LocalDateTime.now();
        webContentCategories.forEach(c -> {
            c.setSuppressionDate(now);
            c.setEditionDate(now);
            c = webContentCategoryRepository.save(c);
        });

        webContentCategories.removeIf(c -> c.getSuppressionDate() != null);
        if (!webContentCategories.isEmpty()) {
            throw new SuppressionException("suppression_failed");
        }

        return true;
    }


    @Override
    public boolean addWebContent(WebContentCategory category, WebContent webContent) {
        CopyOnWriteArrayList<WebContent> initialWebContents = webContentRepository.findAllByWebContentCategoriesContains(category);
        initialWebContents.addIfAbsent(webContent);
        category.setWebContents(initialWebContents);
        webContentCategoryRepository.save(category);
        return true;
    }


    @Override
    public boolean addWebContents(WebContentCategory category, List<WebContent> webContents) {
        CopyOnWriteArrayList<WebContent> initialWebContents = webContentRepository.findAllByWebContentCategoriesContains(category);
        initialWebContents.addAllAbsent(webContents);
        category.setWebContents(initialWebContents);
        webContentCategoryRepository.save(category);
        return true;
    }


    @Override
    public boolean removeWebContent(WebContentCategory category, WebContent webContent) {
        CopyOnWriteArrayList<WebContent> initialWebContents = webContentRepository.findAllByWebContentCategoriesContains(category);
        initialWebContents.remove(webContent);
        category.setWebContents(initialWebContents);
        webContentCategoryRepository.save(category);
        return true;
    }


    @Override
    public boolean removeWebContents(WebContentCategory category, List<WebContent> webContents) {
        CopyOnWriteArrayList<WebContent> initialWebContents = webContentRepository.findAllByWebContentCategoriesContains(category);
        initialWebContents.removeAll(webContents);
        category.setWebContents(initialWebContents);
        webContentCategoryRepository.save(category);
        return true;
    }


    @Override
    public void setCategoryProperties(WebContentCategory webContentCategory) {
        webContentCategory.setTotalWebContents(webContentRepository
                .findAllByWebContentCategoriesContainsAndPublishedIsTrueAndInUseIsTrueAndSuppressionDateIsNull
                        (webContentCategory, TitleSortObject.ASC_TITLE).size());
        webContentCategory.setWebContents(null);
    }


}
