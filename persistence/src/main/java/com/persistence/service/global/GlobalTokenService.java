package com.persistence.service.global;

import com.domain.entity.Token;
import com.domain.entity.User;

/**
 * Interface providing the abstract methods that can be
 * implemented to allow a visitor to validate his account using
 * a {@link Token}.
 */
public interface GlobalTokenService {

    /**
     * Method checking if a {@link Token} is valid.
     *
     * @param token a {@link Token}
     * @return a boolean value indicating if the {@link Token} is valid (true) or not (false)
     */
    boolean checkToken(Token token);


    /**
     * Method adding a {@link Token} associated to a {@link User}.
     *
     * @param user the {@link User}  that will be associated to the {@link Token}
     * @return the newly created  {@link Token}
     */
    Token addToken(User user);


    /**
     * Method validating a {@link User}' email address and thus its account,
     * using a {@link Token}.
     *
     * @param token a {@link Token} object
     * @return a {@link User} object
     */
    User validateUserEmail(Token token);

}
