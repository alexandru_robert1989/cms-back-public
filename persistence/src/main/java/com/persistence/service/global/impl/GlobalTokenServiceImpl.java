package com.persistence.service.global.impl;

import com.domain.entity.Token;
import com.domain.entity.User;
import com.persistence.repository.TokenRepository;
import com.persistence.repository.UserRepository;
import com.persistence.service.global.GlobalTokenService;
import lombok.AllArgsConstructor;
import lombok.Lombok;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * Service providing the methods used for handling
 * {@link Token}s persistence for visitors.
 * <p>
 * The all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */
@AllArgsConstructor
@Service
public class GlobalTokenServiceImpl implements GlobalTokenService {


    /**
     * Repository used for handling  {@link Token}s
     */
    private final TokenRepository tokenRepository;

    /**
     * Repository used for handling  {@link User}s
     */
    private final UserRepository userRepository;


    @Override
    public boolean checkToken(Token token) {
        return tokenRepository.findByTokenAndExpirationDateIsAfter(token.getToken(), LocalDateTime.now()) != null;
    }


    @Override
    public Token addToken(User user) {
        Token token = Token.builder().user(user).build();
        while (tokenRepository.findByToken(token.getToken()) != null) {
            token.setToken(Token.generateToken());
        }
        return tokenRepository.save(token);
    }


    @Override
    public User validateUserEmail(Token token) {
        User user = userRepository.findByTokensContains(token);
        if (user.getSuppressionDate() == null && checkToken(token)) {
            user.setEmailValidated(true);
            userRepository.save(user);
            return user;
        } else {
            return null;
        }

    }


}
