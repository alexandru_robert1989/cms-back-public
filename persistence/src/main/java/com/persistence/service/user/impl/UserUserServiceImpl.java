package com.persistence.service.user.impl;

import com.domain.entity.Token;
import com.domain.entity.User;
import com.email.service.EmailService;
import com.persistence.exception.*;
import com.persistence.repository.UserRepository;
import com.persistence.service.user.UserUserService;
import com.persistence.service.global.GlobalTokenService;
import com.persistence.signup_login.payload.request.ModifyEmailAddressRequest;
import com.persistence.signup_login.payload.request.ModifyUserBaseInfoRequest;

import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import javax.mail.MessagingException;
import javax.validation.ConstraintViolationException;


/**
 * Service providing the methods that a
 * {@link User} can use to modify its account information.
 */
@Service
@AllArgsConstructor
public class UserUserServiceImpl implements UserUserService {

    private final UserRepository userRepository;
    private final EmailService emailService;
    private final GlobalTokenService globalTokenService;
    private final PasswordEncoder passwordEncoder;



    @Override
    public User getSessionUser() throws AuthenticationException {
        User user = findSessionUser();
        if (user == null) {
            return null;
        }
        setUserReturnProperties(user);
        return user;
    }


    @Override
    public void setUserReturnProperties(User user) {
        user.setPassword(null);
        user.setComments(null);
        user.setReceivedMessages(null);
        user.setSentMessages(null);
        user.setTokens(null);
        user.setWebContents(null);
    }


    @Override
    public boolean modifyBaseInfo(ModifyUserBaseInfoRequest request)
            throws AuthenticationException, PseudoAlreadyInUseException, ConstraintViolationException {
        User sessionUser = findSessionUser();
        if (!sessionUser.getEmail().equals(request.getEmail())) {
            throw new AuthenticationException();
        }

        if (userRepository.findByPseudo(request.getPseudo()) != null) {
            if (!sessionUser.getPseudo().equals(request.getPseudo())) {
                throw new PseudoAlreadyInUseException();
            }
        }

        sessionUser.setFirstName(User.formatFirstOrLastName(request.getFirstName()));
        sessionUser.setLastName(User.formatFirstOrLastName(request.getLastName()));
        sessionUser.setPseudo(User.formatPseudo(request.getPseudo()));
        sessionUser.setGetNewsletter(request.getGetNewsletter());
        userRepository.save(sessionUser);
        return true;
    }


    @Override
    public boolean resendValidationMail() throws MessagingException, AuthenticationException {
        User user = findSessionUser();
        Token token = globalTokenService.addToken(user);
        return emailService.resendValidationEmail(user, token);
    }


    @Override
    public boolean modifyEmailAddress(ModifyEmailAddressRequest request) throws MessagingException, NotFoundException,
            EmailNotAvailableException, EmailAlreadyOwnedException, AuthenticationException {

        User user = findSessionUser();
        if (!user.getEmail().equals(request.getOldEmail())) {
            throw new AuthenticationException();
        }

        if (!passwordEncoder.matches(request.getPassword(), user.getPassword())) {
            throw new IncorrectPasswordException();
        }

        if (userRepository.findByEmail(request.getNewEmail()) != null) {
            User oldUser = userRepository.findByEmail(request.getNewEmail());
            if (oldUser.getSuppressionDate() == null) {
                throw new EmailAlreadyOwnedException();
            } else if (oldUser.getSuppressionDate() != null && !oldUser.isSelfDeleted()) {
                throw new EmailNotAvailableException();
            }
        }

        Token token = globalTokenService.addToken(user);
        user.setEmail(request.getNewEmail());
        user.setEmailValidated(false);
        user = userRepository.save(user);

        return emailService.newEmailAddressConfirmation(user, token);
    }


    @Override
    public User findSessionUser() throws AuthenticationException {

        UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == null) {
            throw new AuthenticationException();
        }
        User user = userRepository.findByEmail(principal.getUsername());
        if (user == null) {
            throw new AuthenticationException();
        }
        if (user.getSuppressionDate() != null) {
            throw new AuthenticationException();
        }

        return user;
    }


}
