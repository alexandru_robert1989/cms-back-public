package com.persistence.service.user;

import com.domain.entity.*;
import com.persistence.exception.NotFoundException;

import java.util.List;

/**
 * Interface providing the global abstract methods that can be
 * implemented to allow a visitor to interact with
 * {@link WebContent} objects.
 */
public interface UserWebContentService {



    WebContent findPublishedInUseByVersionId(long versionId) throws NotFoundException;

    List<WebContent> returnRestrictedModificationWebContents() throws NotFoundException;





}
