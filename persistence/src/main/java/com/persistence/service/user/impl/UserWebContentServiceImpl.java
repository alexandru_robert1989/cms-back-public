package com.persistence.service.user.impl;

import com.domain.entity.WebContent;
import com.persistence.exception.NotFoundException;
import com.persistence.repository.WebContentRepository;
import com.persistence.service.user.UserWebContentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Service providing the global  methods used for interacting with
 * {@link WebContent}s
 * objects.
 * <p>
 * The all arguments constructor was implemented using {@link lombok.Lombok} annotations.
 */
@Service
@AllArgsConstructor
public class UserWebContentServiceImpl implements UserWebContentService {


    private final WebContentRepository webContentRepository;


    @Override
    public WebContent findPublishedInUseByVersionId(long versionId) throws NotFoundException {
        CopyOnWriteArrayList<WebContent> webContents = webContentRepository
                .findAllByVersionIdAndInUseIsTrue(versionId);
        if (webContents.isEmpty()) {
            throw new NotFoundException();
        }
        if (returnRestrictedModificationWebContents().contains(webContents.get(0))) {
            throw new NotFoundException();
        }
        return webContents.get(0);
    }




    @Override
    public List<WebContent> returnRestrictedModificationWebContents() throws NotFoundException {
        return webContentRepository.findAllByWebContentTypeHasLimitedModificationIsTrue();
    }



}
