package com.persistence.service.user;


import com.domain.entity.User;
import com.persistence.exception.*;
import com.persistence.signup_login.payload.request.ModifyEmailAddressRequest;
import com.persistence.signup_login.payload.request.ModifyUserBaseInfoRequest;

import javax.mail.MessagingException;
import javax.validation.ConstraintViolationException;

/**
 * Interface providing the abstract methods that can be
 * implemented to allow a {@link User} to use its account.
 */
public interface UserUserService {

    /**
     * Method returning the session {@link User} after having set its properties using the setUserReturnProperties()
     * method.
     *
     * @return a {@link User}
     * @throws AuthenticationException a {@link AuthenticationException}, thrown if the session {@link User} is not found
     */
    User getSessionUser() throws AuthenticationException;

    /**
     * Method setting {@link User} informations before returning them to the front end.
     *
     * @param user a {@link User}
     */
    void setUserReturnProperties(User user);

    /**
     * Method modifyinh a {@link User}'s base info : first name, last name, pseudo and newsletter inscription.
     *
     * @param request a {@link ModifyUserBaseInfoRequest}
     * @return a boolean value indicating if the request succeeded or not
     * @throws AuthenticationException      a {@link AuthenticationException}, thrown if there is a problem retrieving the info of the {@link User}
     *                                      that sent the request
     * @throws PseudoAlreadyInUseException  a {@link PseudoAlreadyInUseException}, thrown if the new pseudo is not available
     * @throws ConstraintViolationException a {@link ConstraintViolationException}, thrown if saving the new {@link User} base information fails due to violating constraints
     */
    boolean modifyBaseInfo(ModifyUserBaseInfoRequest request) throws AuthenticationException, PseudoAlreadyInUseException,
            ConstraintViolationException;

    /**
     * Method sending to a {@link User} a new email that will allow him to validate his email address and thus
     * his account.
     *
     * @return a boolean value indicating if the request succeeded or not
     * @throws MessagingException      a {@link MessagingException}, thrown if sending the email containing the validation link fails
     * @throws AuthenticationException a {@link AuthenticationException}, thrown if there is a problem retrieving the {@link User} that send the request
     */
    boolean resendValidationMail() throws MessagingException, AuthenticationException;

    /**
     * Method modifying a {@link User}'s email address.
     *
     * @param request a {@link ModifyEmailAddressRequest}
     * @return a boolean value indicating if the mail was modified or not
     * @throws MessagingException         a {@link MessagingException}, thrown if sending a email modification confirmation fails
     * @throws NotFoundException          a {@link NotFoundException}, thrown if there is a problem retrieving the {@link User} that sent the request
     * @throws EmailNotAvailableException a {@link EmailNotAvailableException}, thrown if the email used as the new email address belongs to a suppressed {@link User}
     * @throws EmailAlreadyOwnedException a {@link EmailAlreadyOwnedException}, thrown if the email used as the new email address belongs to another {@link User}
     * @throws AuthenticationException    a {@link AuthenticationException}, thrown if the session {@link User} is not found
     */
    boolean modifyEmailAddress(ModifyEmailAddressRequest request) throws MessagingException, NotFoundException,
            EmailNotAvailableException, EmailAlreadyOwnedException, AuthenticationException;

    ;

    /**
     * Method returning the session {@link User}, if any.
     *
     * @return a {@link User}
     * @throws AuthenticationException a {@link AuthenticationException}, thrown if the session {@link User} is not found
     */
    User findSessionUser() throws AuthenticationException;






}
