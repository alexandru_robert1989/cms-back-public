package com.persistence.service.user.impl;

import com.domain.constant.response_message.RoleResponseMessage;
import com.domain.entity.Message;
import com.domain.entity.Role;
import com.domain.entity.User;
import com.email.service.EmailService;
import com.persistence.constant.DefaultVersionId;
import com.persistence.constant.sort_object.DateSortObject;
import com.persistence.exception.AuthenticationException;
import com.persistence.exception.InvalidRequestException;
import com.persistence.exception.NotFoundException;
import com.persistence.payload.request.MessageRequest;
import com.persistence.repository.MessageRepository;
import com.persistence.repository.RoleRepository;
import com.persistence.repository.UserRepository;
import com.persistence.service.user.UserMessageService;
import com.persistence.service.user.UserUserService;
import lombok.AllArgsConstructor;
import lombok.Lombok;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Service providing the  methods that a
 * {@link User} can use to send or consult his {@link Message}s.
 * <p>
 * The all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */

@Service
@AllArgsConstructor
public class UserMessageServiceImpl implements UserMessageService {

    private final MessageRepository messageRepository;

    private final UserUserService userUserService;

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final EmailService emailService;


    @Override
    public boolean treatMessageRequest(MessageRequest request) throws AuthenticationException, InvalidRequestException, MessagingException {
        return false;
    }

    @Override
    public boolean sendMessageUsingContact(MessageRequest request) throws AuthenticationException,
            NotFoundException, InvalidRequestException, MessagingException {

        User sender = userUserService.findSessionUser(); // throws AuthenticationException

        if (!sender.isEmailValidated()) {
            throw new InvalidRequestException("email_not_validated");
        }

        if (request.getText() == null) {
            throw new InvalidRequestException("missing_text");
        }

        CopyOnWriteArrayList<User> recipients = findAdmins(); //throws NotFoundException
        recipients.removeIf(u -> u.getSuppressionDate() != null);

        Message message = new Message();
        message.setSender(sender);
        if (request.getSubject() != null) {
            message.setSubject(request.getSubject());
        }

        Message finalMessage = messageRepository.save(message);
        recipients.forEach(r -> updateUserReceivedMessages(r, finalMessage));
        return true;
    }

    @Override
    public CopyOnWriteArrayList<Message> findUserReceivedMessages(User user) {
        return messageRepository.findDistinctByRecipientsContainsAndSuppressionDateIsNull(user, DateSortObject.DESC_CREATION_DATE);
    }

    @Override
    public boolean updateUserReceivedMessages(User user, Message message) {
        CopyOnWriteArrayList<Message> userMessages = messageRepository
                .findDistinctByRecipientsContains(user, DateSortObject.DESC_CREATION_DATE);
        userMessages.addIfAbsent(message);
        user.setReceivedMessages(userMessages);
        userRepository.save(user);
        return true;
    }

    @Override
    public boolean updateUserReceivedMessages(User user, CopyOnWriteArrayList<Message> messages) {
        CopyOnWriteArrayList<Message> userMessages = messageRepository
                .findDistinctByRecipientsContains(user, DateSortObject.DESC_CREATION_DATE);
        userMessages.addAllAbsent(messages);
        user.setReceivedMessages(userMessages);
        userRepository.save(user);
        return true;
    }


    public CopyOnWriteArrayList<User> findAdmins() throws NotFoundException {
        List<Role> roleList = roleRepository.findAllByInUseIsTrueAndVersionId(DefaultVersionId.ROLE_ADMIN.getId());
        if(roleList.isEmpty() || roleList.get(0).getSuppressionDate() != null){
            throw new NotFoundException(RoleResponseMessage.NOT_FOUND.getMessage());
        }
        return userRepository.findAllByRolesContains(roleList.get(0));
    }
}
