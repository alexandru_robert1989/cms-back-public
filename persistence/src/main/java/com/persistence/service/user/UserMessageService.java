package com.persistence.service.user;

import com.domain.entity.Message;
import com.domain.entity.User;
import com.persistence.exception.AuthenticationException;
import com.persistence.exception.InvalidRequestException;
import com.persistence.exception.NotFoundException;
import com.persistence.payload.request.MessageRequest;

import javax.mail.MessagingException;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Interface providing the abstract methods that can be
 * implemented to allow a {@link User} to send or consult his {@link Message}s.
 */
public interface UserMessageService {


    boolean treatMessageRequest(MessageRequest request) throws AuthenticationException, InvalidRequestException, MessagingException;

    boolean sendMessageUsingContact(MessageRequest request) throws AuthenticationException, NotFoundException,
            InvalidRequestException, MessagingException;


    CopyOnWriteArrayList<Message> findUserReceivedMessages(User user);


    boolean updateUserReceivedMessages(User user, Message message);


    boolean updateUserReceivedMessages(User user, CopyOnWriteArrayList<Message> messages);


}
