package com.persistence.service.user;


import com.domain.entity.Comment;
import com.domain.entity.User;
import com.domain.entity.WebContent;
import com.persistence.exception.AuthenticationException;
import com.persistence.exception.NotFoundException;
import com.persistence.payload.request.AddCommentRequest;

/**
 * Interface providing the abstract methods that can be
 * implemented to allow a {@link User} to leave {@link Comment}s.
 */
public interface UserCommentService {


    Comment findById(long id) throws NotFoundException;

    Comment findPublishedById(Long id) throws NotFoundException;



    /**
     * Method allowing a {@link User} to leave a {@link Comment}
     *
     * @param request a {@link AddCommentRequest}
     * @return a boolean value indicating if saving the {@link Comment} succeeded
     * @throws NumberFormatException   a {@link NumberFormatException}, thrown if parsing the request id {@link String}
     *                                 values to long fails
     * @throws NotFoundException       a {@link NotFoundException}, thrown if the {@link WebContent} used for
     *                                 creating the {@link Comment} is not found
     * @throws NullPointerException    a {@link NullPointerException}, thrown if the request is missing the title and/or the text properties
     * @throws AuthenticationException a {@link AuthenticationException}, thrown if the session {@link User} is not found
     */
    public boolean saveComment(AddCommentRequest request)
            throws NumberFormatException, NotFoundException, NullPointerException, AuthenticationException;


    /**
     * Method deleting a {@link Comment} by adding to it a suppression date.
     *
     * @param commentId a long value representing the id of a {@link Comment}
     * @return a boolean value indicating if the {@link Comment} was deleted
     * @throws NotFoundException a {@link NotFoundException}, thrown if the {@link Comment} is not found
     */
    public boolean deleteComment(long commentId) throws NotFoundException;


}
