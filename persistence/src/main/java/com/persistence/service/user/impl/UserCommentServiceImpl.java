package com.persistence.service.user.impl;

import com.domain.constant.response_message.CommentResponseMessage;
import com.domain.entity.Comment;
import com.domain.entity.User;
import com.domain.entity.WebContent;
import com.persistence.exception.AuthenticationException;
import com.persistence.exception.NotFoundException;

import com.persistence.payload.request.AddCommentRequest;
import com.persistence.repository.CommentRepository;
import com.persistence.service.user.UserCommentService;
import com.persistence.service.user.UserUserService;
import com.persistence.service.user.UserWebContentService;
import lombok.AllArgsConstructor;
import lombok.Lombok;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;


/**
 * Service providing the  methods that a
 * {@link User} can use to leave {@link Comment}s.
 * <p>
 * The all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */
@Service
@AllArgsConstructor
public class UserCommentServiceImpl implements UserCommentService {


    /**
     * Repository used for handling {@link Comment}s
     */
    private final CommentRepository commentRepository;
    private final UserWebContentService userWebContentService;
    private final UserUserService userService;



    @Override
    public Comment findById(long id) throws NotFoundException {
        Optional<Comment> commentOptional = commentRepository.findById(id);
        if (commentOptional.isPresent()) {
            return commentOptional.get();
        }
        throw new NotFoundException(CommentResponseMessage.COMMENT_NOT_FOUND.getMessage());
    }


    @Override
    public Comment findPublishedById(Long id) throws NotFoundException {
        Optional<Comment> comment = commentRepository.findById(id);
        if (comment.isPresent()) {
            if (comment.get().getPublishingDate() != null) {
                return comment.get();
            }
        }
        throw new NotFoundException(CommentResponseMessage.COMMENT_NOT_FOUND.getMessage());
    }


    public boolean saveComment(AddCommentRequest request)
            throws NumberFormatException, NotFoundException, NullPointerException, AuthenticationException {

        User user = userService.findSessionUser();// throws AuthenticationException
        if (request.getWebContentId() == null) {
            throw new NotFoundException();
        }
        WebContent webContent = userWebContentService.findPublishedInUseByVersionId(Long.parseLong(request.getWebContentId()));
        //throws NumberFormatException & throws NotFoundException
        Comment comment = new Comment();
        comment.setUser(user);
        comment.setWebContent(webContent);
        comment.setTitle(request.getTitle()); // throws NullPointerException
        comment.setText(request.getText()); // throws NullPointerException

        if (request.getParentCommentId() != null) {
            Comment parentComment = findPublishedById(Long.parseLong(request.getParentCommentId()));
            //throws NumberFormatException & throws NotFoundException
            comment.setParentComment(parentComment);
        }
        commentRepository.save(comment);
        return true;
    }


    @Override
    public boolean deleteComment(long commentId) throws NotFoundException {
        Comment comment = findById(commentId);
        if (comment.getSuppressionDate() == null) {
            comment.setSuppressionDate(LocalDateTime.now());
            commentRepository.save(comment);
        }
        return true;
    }


}
