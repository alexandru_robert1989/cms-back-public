package com.persistence.service.sort_and_comparator;

import com.domain.entity.User;
import com.domain.entity.WebContent;
import com.domain.entity.WebContentCategory;
import com.domain.entity.WebContentType;
import com.persistence.constant.sort_name.CommentSortName;
import com.persistence.constant.sort_name.DateSortName;
import com.persistence.constant.sort_name.SizeSortName;
import com.persistence.constant.sort_name.TotalWebContentsSortName;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;


/**
 * Class providing the default methods that sort objects
 * using a {@link Comparator}.
 */

@AllArgsConstructor
@Service
public class ComparatorService {


    /**
     * Method sorting a {@link List} of {@link WebContent}s using a {@link Comparator}.
     *
     * @param sortName    a {@link String} value used to choose a {@link Comparator}
     * @param webContents a List of {@link WebContent}s
     */
    public static void sortWebContents(String sortName, List<WebContent> webContents) {

        if (sortNameIsNullOrEmpty(sortName)) {
            return;
        }

        switch (sortName.toLowerCase()) {

            case CommentSortName.TOTAL_COMMENTS_ASC:
                 webContents.sort(Comparator.comparingInt(WebContent::getTotalPublishedComments));
                break;

            case CommentSortName.TOTAL_COMMENTS_DESC:
                webContents.sort((o1, o2) -> Integer.compare(o2.getTotalPublishedComments(), o1.getTotalPublishedComments()));
                break;

            case SizeSortName.SIZE_ASC:
                webContents.sort(Comparator.comparingInt(o -> o.getHtmlContent().length()));
                break;

            case SizeSortName.SIZE_DESC:
                webContents.sort((o1, o2) -> Integer.compare(o2.getHtmlContent().length(), o1.getHtmlContent().length()));
                break;

            default:
        }
    }


    /**
     * Method sorting a {@link List} of {@link WebContentCategory}es using a {@link Comparator}.
     *
     * @param sortName             a {@link String} value used to choose a {@link Comparator}
     * @param webContentCategories a {@link List} of {@link WebContentCategory}es
     */
    public static void sortWebContentCategories(String sortName, List<WebContentCategory> webContentCategories) {

        if (sortNameIsNullOrEmpty(sortName)) {
            return;
        }

        switch (sortName.toLowerCase()) {

            case TotalWebContentsSortName.TOTAL_WEB_CONTENTS_ASC:
                webContentCategories.sort((o1, o2) -> Long.compare(o2.getTotalWebContents(), o1.getTotalWebContents()));
                break;

            case TotalWebContentsSortName.TOTAL_WEB_CONTENTS_DESC:
                webContentCategories.sort((o1, o2) -> Integer.compare(o2.getTotalWebContents(), o1.getTotalWebContents()));
                break;

            case DateSortName.ASC_EDITION_DATE:
                webContentCategories.sort(Comparator.comparing(WebContentCategory::getEditionDate));
                break;

            case DateSortName.DESC_EDITION_DATE:
                webContentCategories.sort((o1, o2) -> o2.getEditionDate().compareTo(o1.getEditionDate()));
                break;

            case DateSortName.ASC_CREATION_DATE:
                webContentCategories.sort(Comparator.comparing(WebContentCategory::getCreationDate));
                break;

            case DateSortName.DESC_CREATION_DATE:
                webContentCategories.sort((o1, o2) -> o2.getCreationDate().compareTo(o1.getCreationDate()));
                break;

            case DateSortName.ASC_SUPPRESSION_DATE:
                webContentCategories.sort(Comparator.comparing(WebContentCategory::getSuppressionDate));
                break;

            case DateSortName.DESC_SUPPRESSION_DATE:
                webContentCategories.sort((o1, o2) -> o2.getSuppressionDate().compareTo(o1.getSuppressionDate()));
                break;

            default:
        }


    }

    /**
     * Method sorting a {@link List} of {@link WebContentType}s  using a {@link Comparator}.
     *
     * @param sortName        a {@link String} value used to choose a {@link Comparator}
     * @param webContentTypes a {@link List} of {@link WebContentType}s
     */
    public static void sortWebContentTypes(String sortName, List<WebContentType> webContentTypes) {

        if (sortNameIsNullOrEmpty(sortName)) {
            return;
        }

        switch (sortName.toLowerCase()) {

            case TotalWebContentsSortName.TOTAL_WEB_CONTENTS_ASC:
                webContentTypes.sort(Comparator.comparingInt(WebContentType::getTotalWebContents));
                break;

            case TotalWebContentsSortName.TOTAL_WEB_CONTENTS_DESC:
                webContentTypes.sort((o1, o2) -> Integer.compare(o2.getTotalWebContents(), o1.getTotalWebContents()));
                break;

            case DateSortName.ASC_EDITION_DATE:
                webContentTypes.sort(Comparator.comparing(WebContentType::getEditionDate));
                break;

            case DateSortName.DESC_EDITION_DATE:
                webContentTypes.sort((o1, o2) -> o2.getEditionDate().compareTo(o1.getEditionDate()));
                break;

            case DateSortName.ASC_CREATION_DATE:
                webContentTypes.sort(Comparator.comparing(WebContentType::getCreationDate));
                break;

            case DateSortName.DESC_CREATION_DATE:
                webContentTypes.sort((o1, o2) -> o2.getCreationDate().compareTo(o1.getCreationDate()));
                break;

            case DateSortName.ASC_SUPPRESSION_DATE:
                webContentTypes.sort(Comparator.comparing(WebContentType::getSuppressionDate));
                break;

            case DateSortName.DESC_SUPPRESSION_DATE:
                webContentTypes.sort((o1, o2) -> o2.getSuppressionDate().compareTo(o1.getSuppressionDate()));
                break;

            default:
        }

    }


    /**
     * Method sorting a {@link List} of {@link User}s  using a {@link Comparator}.
     *
     * @param sortName a {@link String} value used to choose a {@link Comparator}
     * @param users    a {@link List} of {@link User}s
     */
    public static void sortUsers(String sortName, List<User> users) {

        if (sortNameIsNullOrEmpty(sortName)) {
            return;
        }

        String lowerCaseSortTitle = sortName.toLowerCase();

        if (lowerCaseSortTitle.equals(TotalWebContentsSortName.TOTAL_WEB_CONTENTS_ASC)) {
            users.sort(Comparator.comparingInt(User::getTotalWebContents));
        } else if (lowerCaseSortTitle.equals(TotalWebContentsSortName.TOTAL_WEB_CONTENTS_DESC)) {
            users.sort((o1, o2) -> Integer.compare(o2.getTotalWebContents(), o1.getTotalWebContents()));
        }
    }


    private static boolean sortNameIsNullOrEmpty(String sortName) {
        return sortName == null || sortName.isEmpty();
    }


}
