package com.persistence.service.sort_and_comparator;

import com.persistence.constant.sort_name.DateSortName;
import com.persistence.constant.sort_name.TitleSortName;
import com.persistence.constant.sort_object.DateSortObject;
import com.persistence.constant.sort_object.TitleSortObject;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


/**
 * Class providing the methods used for selecting the  {@link Sort} objects
 * used with {@link org.springframework.data.jpa.repository.JpaRepository} GET methods.
 */

@Service
public class SortSelector {

    /**
     * Method returning a {@link Sort} object chosen by analysing the value of the
     * sortName.
     *
     * @param sortName a {@link String} value used to choose a {@link Sort} object
     * @return a {@link Sort} object
     */
    public static Sort selectWebContentSortObject(String sortName) {


        if (sortNameIsNullOrEmpty(sortName)) {
            return DateSortObject.DESC_PUBLISHING_DATE;
        }

        switch (sortName.toLowerCase()) {
            case TitleSortName.ASC_TITLE:
                return TitleSortObject.ASC_TITLE;
            case TitleSortName.DESC_TITLE:
                return TitleSortObject.DESC_TITLE;
            case DateSortName.ASC_CREATION_DATE:
                return DateSortObject.ASC_CREATION_DATE;
            case DateSortName.DESC_CREATION_DATE:
                return DateSortObject.DESC_CREATION_DATE;
            case DateSortName.ASC_PUBLISHING_DATE:
                return DateSortObject.ASC_PUBLISHING_DATE;
            case DateSortName.ASC_EDITION_DATE:
                return DateSortObject.ASC_EDITION_DATE;
            case DateSortName.DESC_EDITION_DATE:
                return DateSortObject.DESC_EDITION_DATE;
            case DateSortName.ASC_SUPPRESSION_DATE:
                return DateSortObject.ASC_SUPPRESSION_DATE;
            case DateSortName.DESC_SUPPRESSION_DATE:
                return DateSortObject.DESC_SUPPRESSION_DATE;
            default:
                return DateSortObject.DESC_PUBLISHING_DATE;
        }
    }


    /**
     * Method returning a {@link Sort} object chosen by analysing the value of the
     * sortName.
     *
     * @param sortName a {@link String} value used to choose a {@link Sort} object
     * @return a {@link Sort} object
     */
    public static Sort selectWebContentCategorySortObject(String sortName) {
        if (sortNameIsNullOrEmpty(sortName)) {
            return TitleSortObject.ASC_TITLE;
        }

        switch (sortName.toLowerCase()) {
            case TitleSortName.DESC_TITLE:
                return TitleSortObject.DESC_TITLE;
            case DateSortName.ASC_CREATION_DATE:
                return DateSortObject.ASC_CREATION_DATE;
            case DateSortName.DESC_CREATION_DATE:
                return DateSortObject.DESC_CREATION_DATE;
            case DateSortName.ASC_EDITION_DATE:
                return DateSortObject.ASC_EDITION_DATE;
            case DateSortName.DESC_EDITION_DATE:
                return DateSortObject.DESC_EDITION_DATE;
            case DateSortName.ASC_SUPPRESSION_DATE:
                return DateSortObject.ASC_SUPPRESSION_DATE;
            case DateSortName.DESC_SUPPRESSION_DATE:
                return DateSortObject.DESC_SUPPRESSION_DATE;
            default:
                return TitleSortObject.ASC_TITLE;
        }
    }

    /**
     * Method returning a {@link Sort} object chosen by analysing the value of the
     * sortName.
     *
     * @param sortName a {@link String} value used to choose a {@link Sort} object
     * @return a {@link Sort} object
     */
    public static Sort selectWebContentTypeSort(String sortName) {

        if (sortNameIsNullOrEmpty(sortName)) {
            return TitleSortObject.ASC_TITLE;
        }


        switch (sortName.toLowerCase()) {
            case TitleSortName.DESC_TITLE:
                return TitleSortObject.DESC_TITLE;
            case DateSortName.ASC_CREATION_DATE:
                return DateSortObject.ASC_CREATION_DATE;
            case DateSortName.DESC_CREATION_DATE:
                return DateSortObject.DESC_CREATION_DATE;
            case DateSortName.ASC_EDITION_DATE:
                return DateSortObject.ASC_EDITION_DATE;
            case DateSortName.DESC_EDITION_DATE:
                return DateSortObject.DESC_EDITION_DATE;
            case DateSortName.ASC_SUPPRESSION_DATE:
                return DateSortObject.ASC_SUPPRESSION_DATE;
            case DateSortName.DESC_SUPPRESSION_DATE:
                return DateSortObject.DESC_SUPPRESSION_DATE;
            default:
                return TitleSortObject.ASC_TITLE;
        }
    }

    private static boolean sortNameIsNullOrEmpty(String sortName) {
        return sortName == null || sortName.isEmpty();
    }


}
