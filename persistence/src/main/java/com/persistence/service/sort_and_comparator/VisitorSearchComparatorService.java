package com.persistence.service.sort_and_comparator;

import com.domain.entity.WebContent;
import com.persistence.constant.sort_name.CommentSortName;
import com.persistence.constant.sort_name.DateSortName;
import com.persistence.constant.sort_name.SizeSortName;
import com.persistence.constant.sort_name.TitleSortName;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;


/**
 * Class providing the default methods that sort objects
 * using a {@link Comparator}.
 */

@AllArgsConstructor
@Service
public class VisitorSearchComparatorService {



    public static void sortFoundWebContents(String sortName, List<WebContent> webContents) {

        if (sortNameIsNullOrEmpty(sortName)) {
            webContents.sort((wc1, wc2) -> wc2.getTotalKeywordInstances().compareTo(wc1.getTotalKeywordInstances()));
            return;
        }

        switch (sortName.toLowerCase()) {

            case TitleSortName.ASC_TITLE:
                webContents.sort(Comparator.comparing(WebContent::getTitle));
                break;

            case TitleSortName.DESC_TITLE:
                webContents.sort((o1, o2) -> o2.getTitle().compareTo(o1.getTitle()));
                break;

            case DateSortName.ASC_PUBLISHING_DATE:
                webContents.sort(Comparator.comparing(WebContent::getPublishingDate));
                break;


            case DateSortName.DESC_PUBLISHING_DATE:
                webContents.sort((o1, o2) -> o2.getPublishingDate().compareTo(o1.getPublishingDate()));
                break;

            case DateSortName.ASC_EDITION_DATE:
                webContents.sort(Comparator.comparing(WebContent::getEditionDate));
                break;


            case DateSortName.DESC_EDITION_DATE:
                webContents.sort((o1, o2) -> o2.getEditionDate().compareTo(o1.getEditionDate()));
                break;


            case CommentSortName.TOTAL_COMMENTS_ASC:
                webContents.sort(Comparator.comparingInt(WebContent::getTotalPublishedComments));
                break;


            case CommentSortName.TOTAL_COMMENTS_DESC:
                webContents.sort((o1, o2) -> Integer.compare(o2.getTotalPublishedComments(), o1.getTotalPublishedComments()));
                break;

            case SizeSortName.SIZE_ASC:
                webContents.sort(Comparator.comparingInt(o -> o.getHtmlContent().length()));
                break;
            case SizeSortName.SIZE_DESC:
                webContents.sort((o1, o2) -> Integer.compare(o2.getHtmlContent().length(), o1.getHtmlContent().length()));
                break;

            default:
                webContents.sort((wc1, wc2) -> wc2.getTotalKeywordInstances().compareTo(wc1.getTotalKeywordInstances()));

        }
    }


    private static boolean sortNameIsNullOrEmpty(String sortName) {
        return sortName == null || sortName.isEmpty();
    }


}
