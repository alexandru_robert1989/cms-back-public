package com.persistence.service.scheduled;

import com.domain.entity.Token;
import com.persistence.repository.TokenRepository;
import lombok.AllArgsConstructor;
import lombok.Lombok;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * This class provides methods that are triggered automatically at fixed time delays.
 *
 * The all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */
@Service
@AllArgsConstructor
public class ScheduledTasks {


    /**
     * Service used for handling {@link Token}s.
     */
    private final TokenRepository tokenRepository;


    /**
     * This method deletes expired tokens automatically after a fixed time delay.
     */
    @Scheduled(fixedDelayString = "${api.fixedDelay.in.milliseconds}")
    private void deleteExpiredTokens() {
        CopyOnWriteArrayList<Token> expiredTokens = tokenRepository.findAllByExpirationDateBefore(LocalDateTime.now());
        tokenRepository.deleteAll(expiredTokens);

    }


}
