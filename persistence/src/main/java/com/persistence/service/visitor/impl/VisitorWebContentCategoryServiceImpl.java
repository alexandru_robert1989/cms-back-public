package com.persistence.service.visitor.impl;

import com.domain.constant.response_message.CategoryResponseMessage;
import com.domain.dto.WebContentCategoryDto;
import com.domain.entity.WebContent;
import com.domain.entity.WebContentCategory;
import com.domain.utility.mapper.WebContentCategoryMapper;
import com.persistence.exception.NotFoundException;
import com.persistence.service.sort_and_comparator.ComparatorService;
import com.persistence.constant.sort_name.TotalWebContentsSortName;
import com.persistence.constant.sort_object.TitleSortObject;
import com.persistence.repository.WebContentCategoryRepository;
import com.persistence.repository.WebContentRepository;
import com.persistence.service.visitor.VisitorWebContentCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;


@AllArgsConstructor
@Service
public class VisitorWebContentCategoryServiceImpl implements VisitorWebContentCategoryService {


    private final WebContentCategoryRepository webContentCategoryRepository;
    private final WebContentRepository webContentRepository;
    private final WebContentCategoryMapper webContentCategoryMapper;


    @Override
    public WebContentCategory findOneInUseByVersionId(Long versionId) throws NotFoundException {
        List<WebContentCategory> categories = webContentCategoryRepository.findAllByVersionIdAndInUseIsTrue(versionId);
        if(categories.isEmpty() || !categories.get(0).isPublished() || categories.get(0).getSuppressionDate()!= null){
            throw new NotFoundException(CategoryResponseMessage.NOT_FOUND.getMessage());
        }
        return categories.get(0);
    }

    @Override
    public WebContentCategoryDto getByVersionId(long versionId) throws NotFoundException {
        CopyOnWriteArrayList<WebContentCategory> categories = webContentCategoryRepository.
                findAllByVersionIdAndInUseIsTrue(versionId);
        if (categories.isEmpty() || !categories.get(0).isPublished()
                || categories.get(0).getSuppressionDate() != null) {
            throw new NotFoundException(CategoryResponseMessage.NOT_FOUND.getMessage());
        }
        return webContentCategoryMapper.forVisitor(categories.get(0));

    }


    @Override
    public CopyOnWriteArrayList<WebContentCategory> findAllNotSuppressedInUseByWebContentAndSort(long wcVersionId, Sort sort) {

        CopyOnWriteArrayList<WebContent> webContents = webContentRepository
                .findAllByVersionIdAndInUseIsTrueAndPublishedIsTrueAndSuppressionDateIsNull(wcVersionId);
        if (webContents.isEmpty()) {
            return null;
        }
        WebContent webContent = webContents.get(0);

        CopyOnWriteArrayList<WebContentCategory> webContentCategories = webContentCategoryRepository
                .findAllByWebContentsContainsAndInUseIsTrueAndSuppressionDateIsNull(webContent, sort);
        webContentCategories.forEach(this::setVisitorCategoryProperties);
        return webContentCategories;
    }




    @Override
    public List<WebContentCategoryDto> getNavbarCategories() {
        List<WebContentCategory> categories = webContentCategoryRepository.findAllByInUseIsTrue();

        return categories.stream()
                .filter(c -> c.isPublished() && c.getSuppressionDate() == null)
                .sorted((c1, c2) -> c2.getTotalPublishedWebContents().compareTo(c1.getTotalPublishedWebContents()))
                .map(webContentCategoryMapper::forVisitor)
                .collect(Collectors.toList());
    }

    @Override
    public List<WebContentCategoryDto> getWebContentSortingCategories() {
        List<WebContentCategory> categories = webContentCategoryRepository.findAllByInUseIsTrue();
        return categories.stream()
                .filter(c -> c.isPublished() && c.getSuppressionDate() == null && c.getTotalPublishedWebContents() > 0)
                .sorted((c1, c2) -> c2.getTotalPublishedWebContents().compareTo(c1.getTotalPublishedWebContents()))
                .map(webContentCategoryMapper::forVisitor)
                .collect(Collectors.toList());
    }


    @Override
    public CopyOnWriteArrayList<WebContentCategory> findAllNotSuppressedInUseSortingItems(Sort sort) {

        CopyOnWriteArrayList<WebContentCategory> webContentCategories = webContentCategoryRepository
                .findAllByInUseIsTrueAndSuppressionDateIsNull(sort);
        webContentCategories.forEach(this::setVisitorCategoryProperties);
        webContentCategories.removeIf(c -> c.getTotalWebContents() == 0);
        ComparatorService.sortWebContentCategories(TotalWebContentsSortName.TOTAL_WEB_CONTENTS_DESC, webContentCategories);
        return webContentCategories;
    }



    @Override
    public void setVisitorCategoryProperties(WebContentCategory webContentCategory) {
        webContentCategory.setTotalWebContents(webContentRepository
                .findAllByWebContentCategoriesContainsAndPublishedIsTrueAndInUseIsTrueAndSuppressionDateIsNull
                        (webContentCategory, TitleSortObject.ASC_TITLE).size());
        webContentCategory.setWebContents(null);
    }


}
