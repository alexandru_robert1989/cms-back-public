package com.persistence.service.visitor;

import com.domain.dto.WebContentTypeDto;
import com.domain.entity.WebContentType;
import com.persistence.exception.NotFoundException;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Interface providing the global abstract methods that can be
 * implemented to allow a visitor to interact with
 * {@link WebContentType} objects.
 */
public interface VisitorWebContentTypeService {

    WebContentType findOneInUseByVersionId(long versionId);

    WebContentTypeDto getOneInUseByVersionId(long versionId);

    CopyOnWriteArrayList<WebContentTypeDto> getAllNavbarItems();

    CopyOnWriteArrayList<WebContentTypeDto> getAllFooterItems();

    List<WebContentTypeDto> getTypeSortingItems();


}
