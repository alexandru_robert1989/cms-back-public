package com.persistence.service.visitor.impl;

import com.domain.constant.response_message.CommentResponseMessage;
import com.domain.dto.CommentDto;
import com.domain.entity.Comment;
import com.domain.entity.WebContent;
import com.domain.utility.mapper.CommentMapper;
import com.persistence.exception.NotFoundException;
import com.persistence.repository.CommentRepository;
import com.persistence.repository.WebContentRepository;
import com.persistence.service.visitor.VisitorCommentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class VisitorCommentServiceImpl implements VisitorCommentService {


    private final WebContentRepository webContentRepository;
    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;


    @Override
    public List<CommentDto> getWebContentParentComments(Long wcVersionId) throws NotFoundException {
        WebContent webContent = findWebContentInUseByVersionId(wcVersionId);
        List<Comment> comments = commentRepository.findAllByWebContent(webContent);


        return comments.stream()
                .filter(c -> c.getPublishingDate() != null
                        && c.getSuppressionDate() == null
                        && c.getUser().getSuppressionDate() == null
                        && c.getParentComment() == null)
                .sorted((c1, c2) -> c2.getPublishingDate().compareTo(c1.getPublishingDate()))
                .map(c -> commentMapper.dtoForVisitor(c, c.getUser()))
                .collect(Collectors.toList());

    }

    @Override
    public List<CommentDto> getCommentReactions(Long commentId) throws NotFoundException {
        Optional<Comment> comment = commentRepository.findById(commentId);
        if (!comment.isPresent()) {
            throw new NotFoundException(CommentResponseMessage.COMMENT_NOT_FOUND.getMessage());
        } else {
            if (comment.get().getPublishingDate() == null || comment.get().getSuppressionDate() != null) {
                throw new NotFoundException(CommentResponseMessage.COMMENT_NOT_FOUND.getMessage());
            }
        }

        List<Comment> comments = commentRepository.findAllByParentCommentId(commentId);
        return comments.stream()
                .filter(c -> c.getPublishingDate() != null && c.getSuppressionDate() == null && c.getUser().getSuppressionDate() == null)
                .sorted((c1, c2) -> c2.getPublishingDate().compareTo(c1.getPublishingDate()))
                .map(c -> commentMapper.dtoForVisitor(c, c.getUser()))
                .collect(Collectors.toList());
    }


    private WebContent findWebContentInUseByVersionId(long versionId) throws NotFoundException {
        CopyOnWriteArrayList<WebContent> webContents = webContentRepository.findAllByVersionIdAndInUseIsTrue(versionId);
        if (webContents.isEmpty()) {
            throw new NotFoundException(CommentResponseMessage.WEB_CONTENT_NOT_FOUND.getMessage());
        } else {
            if (webContents.get(0).getPublishingDate() == null || webContents.get(0).getSuppressionDate() != null) {
                throw new NotFoundException(CommentResponseMessage.WEB_CONTENT_NOT_FOUND.getMessage());
            }
        }
        return webContents.get(0);
    }


}
