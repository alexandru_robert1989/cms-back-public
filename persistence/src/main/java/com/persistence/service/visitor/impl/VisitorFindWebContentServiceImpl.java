package com.persistence.service.visitor.impl;

import com.domain.constant.response_message.WebContentResponseMessage;
import com.domain.dto.WebContentDto;
import com.domain.entity.User;
import com.domain.entity.WebContent;
import com.domain.entity.WebContentCategory;
import com.domain.entity.WebContentType;
import com.persistence.exception.NotFoundException;
import com.persistence.repository.WebContentRepository;
import com.persistence.service.visitor.VisitorFindWebContentService;
import com.persistence.service.visitor.VisitorUserService;
import com.persistence.service.visitor.VisitorWebContentCategoryService;
import com.persistence.service.visitor.VisitorWebContentTypeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Service providing the global  methods used for finding
 * {@link WebContent}s
 * objects.
 * <p>
 * The all arguments constructor was implemented using {@link lombok.Lombok} annotations.
 */
@Service
@AllArgsConstructor
public class VisitorFindWebContentServiceImpl implements VisitorFindWebContentService {


    private final WebContentRepository webContentRepository;
    private final VisitorWebContentTypeService visitorWebContentTypeService;
    private final VisitorWebContentCategoryService visitorWebContentCategoryService;
    private final VisitorUserService visitorUserService;


    @Override
    public WebContent getByVersionId(long versionId) throws NotFoundException {
        CopyOnWriteArrayList<WebContent> webContents = webContentRepository
                .findAllByVersionIdAndInUseIsTrue(versionId);
        if (webContents.isEmpty() || !webContents.get(0).isPublished() || webContents.get(0).getSuppressionDate() != null
        || webContents.get(0).getPublishingDate() == null) {
            throw new NotFoundException(WebContentResponseMessage.NOT_FOUND.getMessage());
        }
        return webContents.get(0);
    }



    @Override
    public CopyOnWriteArrayList<WebContent> getAll() {
        return new CopyOnWriteArrayList<>(webContentRepository.findAll());
    }



    @Override
    public CopyOnWriteArrayList<WebContent> getAllByType(long typeVersionId) throws NotFoundException {
        WebContentType webContentType = visitorWebContentTypeService.findOneInUseByVersionId(typeVersionId);
        CopyOnWriteArrayList<WebContent> webContents = webContentRepository
                .findAllByWebContentTypeIdOrWebContentTypeWebContentTypeId(webContentType.getId(), webContentType.getId());
        return webContents;
    }


    @Override
    public CopyOnWriteArrayList<WebContent> getAllByCategory(Long categoryVersionId) throws NotFoundException {
        WebContentCategory webContentCategory = visitorWebContentCategoryService.findOneInUseByVersionId(categoryVersionId);
        CopyOnWriteArrayList<WebContent> webContents = webContentRepository.findAllByWebContentCategoriesContains(webContentCategory);
        return webContents;
    }


    @Override
    public CopyOnWriteArrayList<WebContent> getAllByTypeAndCategory(long typeVersionId, long categoryVersionId) throws NotFoundException {
        WebContentType type = visitorWebContentTypeService.findOneInUseByVersionId(typeVersionId);
        WebContentCategory category = visitorWebContentCategoryService.findOneInUseByVersionId(categoryVersionId);
        CopyOnWriteArrayList<WebContent> webContents = webContentRepository
                .findAllByWebContentTypeIdOrWebContentTypeWebContentTypeIdAndWebContentCategoriesContains
                        (type.getId(), type.getId(), category);
        return webContents;
    }


    @Override
    public CopyOnWriteArrayList<WebContent> getAllByEditor(long editorId) throws NotFoundException {
        User user = visitorUserService.findUserById(editorId);
        CopyOnWriteArrayList<WebContent> webContents = webContentRepository.findAllByUser(user);
        return webContents;
    }


    @Override
    public CopyOnWriteArrayList<WebContent> getAllByTypeAndUser(long typeVersionId, long userId) throws NotFoundException {
        WebContentType type = visitorWebContentTypeService.findOneInUseByVersionId(typeVersionId);
        User user = visitorUserService.findUserById(userId);
        CopyOnWriteArrayList<WebContent> webContents = webContentRepository
                .findAllByWebContentTypeIdOrWebContentTypeWebContentTypeIdAndUser(type.getId(), type.getId(), user);
        return webContents;
    }


    @Override
    public CopyOnWriteArrayList<WebContent> getAllByCategoryAndUser(long categoryVersionId, long userId) throws NotFoundException {
        WebContentCategory category = visitorWebContentCategoryService.findOneInUseByVersionId(categoryVersionId);
        User user = visitorUserService.findUserById(userId);
        CopyOnWriteArrayList<WebContent> webContents = webContentRepository.findAllByWebContentCategoriesContainsAndUser(category, user);
        return webContents;
    }


    @Override
    public CopyOnWriteArrayList<WebContent> getAllByTypeAndCategoryAndUser
            (long typeVersionId, long categoryVersionId, long userId) throws NotFoundException {
        WebContentType type = visitorWebContentTypeService.findOneInUseByVersionId(typeVersionId);
        User user = visitorUserService.findUserById(userId);
        WebContentCategory category = visitorWebContentCategoryService.findOneInUseByVersionId(categoryVersionId);

        CopyOnWriteArrayList<WebContent> webContents = webContentRepository
                .findAllByPublishedIsTrueAndWebContentTypeIdOrWebContentTypeWebContentTypeIdAndWebContentCategoriesContainsAndUser
                        (type.getId(), type.getId(), category, user);
        return webContents;
    }



}
