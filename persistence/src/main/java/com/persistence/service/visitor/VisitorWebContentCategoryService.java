package com.persistence.service.visitor;


import com.domain.dto.WebContentCategoryDto;
import com.domain.entity.WebContentCategory;
import com.persistence.exception.NotFoundException;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Interface providing the abstract methods provided to visitors for handling
 * {@link WebContentCategory}es.
 */
public interface VisitorWebContentCategoryService {


    WebContentCategory findOneInUseByVersionId(Long versionId) throws NotFoundException;

    WebContentCategoryDto getByVersionId(long versionId) throws NotFoundException;

    CopyOnWriteArrayList<WebContentCategory> findAllNotSuppressedInUseByWebContentAndSort(long versionWcId, Sort sort);


    List<WebContentCategoryDto> getNavbarCategories();

    List<WebContentCategoryDto> getWebContentSortingCategories();

    CopyOnWriteArrayList<WebContentCategory> findAllNotSuppressedInUseSortingItems(Sort sort);

    void setVisitorCategoryProperties(WebContentCategory webContentCategory);


}
