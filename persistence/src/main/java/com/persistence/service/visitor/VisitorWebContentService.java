package com.persistence.service.visitor;

import com.domain.dto.WebContentDto;
import com.domain.entity.*;
import com.persistence.exception.NotFoundException;

import java.util.List;

/**
 * Interface providing the global abstract methods that can be
 * implemented to allow a visitor to interact with
 * {@link WebContent} objects.
 */
public interface VisitorWebContentService {


    WebContentDto getByVersionId(long versionId) throws NotFoundException;


    WebContent getHomePage() throws NotFoundException;


    WebContent getLegalMentionsPage() throws NotFoundException;


    List<WebContentDto> getAllPublishedSorted(String sort);





    List<WebContentDto> getAllByTypeSorted
            (long typeVersionId, String sort) throws NotFoundException;




    List<WebContentDto> getAllByCategorySorted(Long categoryVersionId, String sort) throws NotFoundException;



    List<WebContentDto> getAllByTypeAndCategorySorted(long typeVersionId, long categoryVersionId, String sort) throws NotFoundException;



    List<WebContentDto> getAllByEditorSorted(long editorId, String sort) throws NotFoundException;



    List<WebContentDto> getAllByTypeAndUserSorted(long typeVersionId, long userId, String sort) throws NotFoundException;



    List<WebContentDto> getAllByCategoryAndUserSorted(long categoryVersionId, long userId, String sort) throws NotFoundException;

    List<WebContentDto> getAllByTypeAndCategoryAndUserSorted
            (long typeVersionId, long categoryVersionId, long userId, String sort) throws NotFoundException;


}
