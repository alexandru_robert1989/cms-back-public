package com.persistence.service.visitor;

import com.domain.dto.UserDto;
import com.domain.entity.User;

import java.util.List;

public interface VisitorUserService {


    User findUserById(long id);


    List<UserDto> getAllEditors();

    UserDto getEditorById(Long id);

    UserDto getEditorByWebContentVersionId(Long webContentVersionId);

}
