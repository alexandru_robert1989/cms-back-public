package com.persistence.service.visitor.impl;

import com.domain.constant.response_message.RoleResponseMessage;
import com.domain.constant.response_message.UserResponseMessage;
import com.domain.constant.response_message.WebContentResponseMessage;
import com.domain.dto.UserDto;
import com.domain.entity.Role;
import com.domain.entity.User;
import com.domain.entity.WebContent;
import com.domain.utility.mapper.UserMapper;
import com.persistence.constant.DefaultVersionId;
import com.persistence.exception.NotFoundException;
import com.persistence.repository.RoleRepository;
import com.persistence.repository.UserRepository;
import com.persistence.repository.WebContentRepository;
import com.persistence.service.visitor.VisitorUserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class VisitorUserServiceImpl implements VisitorUserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserMapper userMapper;
    private final WebContentRepository webContentRepository;


    @Override
    public User findUserById(long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent() && user.get().getSuppressionDate() == null) {
            return user.get();
        }
        throw new NotFoundException(UserResponseMessage.NOT_FOUND.getMessage());
    }



    @Override
    public List<UserDto> getAllEditors() throws NotFoundException {
        List<User> editorEntitiesList = userRepository.findAllByRolesContainsAndSuppressionDateIsNull(findEditorRole());
        return editorEntitiesList.stream()
                .filter(e -> e.getTotalPublishedWebContents() > 0)
                .sorted((wct1, wct2) -> wct2.getTotalPublishedWebContents().compareTo(wct1.getTotalPublishedWebContents()))
                .map(userMapper::forVisitor)
                .collect(Collectors.toList());
    }

    @Override
    public UserDto getEditorById(Long id) throws NotFoundException {
        User user = userRepository.findByIdAndRolesContainsAndSuppressionDateIsNull(id, findEditorRole());
        if (user == null) {
            throw new NotFoundException(UserResponseMessage.NOT_FOUND.getMessage());
        }
        return userMapper.forVisitor(user);
    }


    @Override
    public UserDto getEditorByWebContentVersionId(Long webContentVersionId) throws NotFoundException {

        WebContent webContent = findEditorByWebContentVersionId(webContentVersionId);
        User user = userRepository.findByWebContentsContainsAndSuppressionDateIsNull(webContent);
        if (user == null) {
            throw new NotFoundException(UserResponseMessage.NOT_FOUND.getMessage());
        }
        return userMapper.forVisitor(user);
    }


    private Role findEditorRole() throws NotFoundException {
        List<Role> editorRoles = roleRepository
                .findAllByInUseIsTrueAndVersionId(DefaultVersionId.ROLE_EDITOR.getId());
        if (editorRoles.isEmpty()) {
            throw new NotFoundException(RoleResponseMessage.NOT_FOUND.getMessage());
        }

        return editorRoles.get(0);
    }


    private WebContent findEditorByWebContentVersionId(Long webContentVersionId) throws NotFoundException {
        List<WebContent> webContents = webContentRepository
                .findAllByVersionIdAndInUseIsTrueAndPublishedIsTrueAndSuppressionDateIsNull(webContentVersionId);
        if (webContents.isEmpty()) {
            throw new NotFoundException(WebContentResponseMessage.NOT_FOUND.getMessage());
        }
        return webContents.get(0);
    }

}
