package com.persistence.service.visitor;


import com.domain.dto.CommentDto;
import com.persistence.exception.NotFoundException;

import java.util.List;

public interface VisitorCommentService {

    List<CommentDto> getWebContentParentComments(Long wcVersionId) throws NotFoundException;

    List<CommentDto> getCommentReactions(Long commentId) throws NotFoundException;




}
