package com.persistence.service.visitor;

import com.domain.entity.WebContent;
import com.persistence.exception.NotFoundException;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Interface providing the global abstract methods that can be
 * implemented to allow a visitor to interact with
 * {@link WebContent} objects.
 */
public interface VisitorFindWebContentService {


    WebContent getByVersionId(long versionId) throws NotFoundException;


    CopyOnWriteArrayList<WebContent> getAll();


    CopyOnWriteArrayList<WebContent> getAllByType
            (long typeVersionId) throws NotFoundException;


    CopyOnWriteArrayList<WebContent> getAllByCategory(Long categoryVersionId) throws NotFoundException;


    CopyOnWriteArrayList<WebContent> getAllByTypeAndCategory(long typeVersionId, long categoryVersionId) throws NotFoundException;


    CopyOnWriteArrayList<WebContent> getAllByEditor(long editorId) throws NotFoundException;


    CopyOnWriteArrayList<WebContent> getAllByTypeAndUser(long typeVersionId, long userId) throws NotFoundException;


    CopyOnWriteArrayList<WebContent> getAllByCategoryAndUser(long categoryVersionId, long userId) throws NotFoundException;

    CopyOnWriteArrayList<WebContent> getAllByTypeAndCategoryAndUser
            (long typeVersionId, long categoryVersionId, long userId) throws NotFoundException;


}
