package com.persistence.service.visitor.impl;

import com.domain.constant.response_message.TypeResponseMessage;
import com.domain.dto.WebContentTypeDto;
import com.domain.entity.WebContentType;
import com.domain.utility.mapper.WebContentTypeMapper;
import com.persistence.exception.NotFoundException;
import com.persistence.repository.WebContentTypeRepository;
import com.persistence.service.visitor.VisitorWebContentTypeService;
import lombok.AllArgsConstructor;
import lombok.Lombok;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * Service providing the global  methods used for interacting with
 * {@link WebContentType}s
 *
 * <p>
 * The all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */
@AllArgsConstructor
@Service
public class VisitorWebContentTypeServiceImpl implements VisitorWebContentTypeService {


    private final WebContentTypeRepository webContentTypeRepository;
    private final WebContentTypeMapper webContentTypeMapper;


    @Override
    public WebContentType findOneInUseByVersionId(long versionId) throws NotFoundException {
        CopyOnWriteArrayList<WebContentType> webContentTypes = webContentTypeRepository
                .findAllByVersionIdAndInUseIsTrueAndSuppressionDateIsNull(versionId);
        if (webContentTypes.isEmpty() || !webContentTypes.get(0).isPublished()
                || webContentTypes.get(0).getSuppressionDate() != null) {
            throw new NotFoundException(TypeResponseMessage.NOT_FOUND.getMessage());
        }
        return webContentTypes.get(0);
    }


    @Override
    public WebContentTypeDto getOneInUseByVersionId(long versionId) {
        CopyOnWriteArrayList<WebContentType> webContentTypes = webContentTypeRepository
                .findAllByVersionIdAndInUseIsTrueAndSuppressionDateIsNull(versionId);
        if (webContentTypes.isEmpty() || !webContentTypes.get(0).isPublished()) {
            throw new NotFoundException(TypeResponseMessage.NOT_FOUND.getMessage());
        }
        return webContentTypeMapper.forVisitor(webContentTypes.get(0), findChildrenTypes(webContentTypes.get(0)));
    }


    @Override
    public CopyOnWriteArrayList<WebContentTypeDto> getAllNavbarItems() {
        CopyOnWriteArrayList<WebContentType> webContentTypes = webContentTypeRepository.findAllByInUseIsTrueAndIsNavbarItemIsTrue();

        return webContentTypes.stream()
                .filter(t -> t.isPublished() && t.getSuppressionDate() == null && !t.isHasLimitedModification())
                .sorted(Comparator.comparing(WebContentType::getTitle))
                .map(t -> webContentTypeMapper.forVisitor(t, findChildrenTypes(t)))
                .collect(Collectors.toCollection(CopyOnWriteArrayList::new));
    }


    @Override
    public CopyOnWriteArrayList<WebContentTypeDto> getAllFooterItems() {
        CopyOnWriteArrayList<WebContentType> webContentTypes = webContentTypeRepository.findAllByInUseIsTrueAndIsFooterItemIsTrue();

        return webContentTypes.stream()
                .filter(t -> t.isPublished() && t.getSuppressionDate() == null && !t.isHasLimitedModification())
                .sorted(Comparator.comparing(WebContentType::getTitle))
                .map(t -> webContentTypeMapper.forVisitor(t, findChildrenTypes(t)))
                .collect(Collectors.toCollection(CopyOnWriteArrayList::new));
    }


    @Override
    public List<WebContentTypeDto> getTypeSortingItems() {
        List<WebContentType> webContentTypes = webContentTypeRepository.findAllByInUseIsTrueAndPublishedIsTrue();
        return webContentTypes.stream()
                .filter(wct -> !wct.isHasLimitedModification() && wct.getSuppressionDate() == null)
                .sorted((t1, t2) -> t2.getTotalPublishedWebContents().compareTo(t1.getTotalPublishedWebContents()))
                .map(webContentTypeMapper::forVisitor).collect(Collectors.toCollection(CopyOnWriteArrayList::new));
    }


    private List<WebContentType> findChildrenTypes(WebContentType webContentType) {
        List<WebContentType> webContentTypes = webContentTypeRepository.findAllByInUseIsTrueAndWebContentType(webContentType);
        return webContentTypes.stream().filter(t -> t.isPublished() && !t.isHasLimitedModification() & t.getSuppressionDate() == null)
                .collect(Collectors.toList());
    }


}
