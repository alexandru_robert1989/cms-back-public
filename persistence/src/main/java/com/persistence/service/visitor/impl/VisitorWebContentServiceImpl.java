package com.persistence.service.visitor.impl;

import com.domain.constant.response_message.WebContentResponseMessage;
import com.domain.dto.WebContentDto;
import com.domain.entity.WebContent;
import com.domain.utility.mapper.WebContentMapper;
import com.persistence.constant.DefaultVersionId;
import com.persistence.exception.NotFoundException;
import com.persistence.repository.WebContentRepository;
import com.persistence.service.sort_and_comparator.VisitorComparatorService;
import com.persistence.service.visitor.VisitorFindWebContentService;
import com.persistence.service.visitor.VisitorWebContentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * Service providing the global  methods used for interacting with
 * {@link WebContent}s
 * objects.
 * <p>
 * The all arguments constructor was implemented using {@link lombok.Lombok} annotations.
 */
@Service
@AllArgsConstructor
public class VisitorWebContentServiceImpl implements VisitorWebContentService {


    private final WebContentMapper webContentMapper;
    private final VisitorFindWebContentService visitorFindWebContentService;


    @Override
    public WebContentDto getByVersionId(long versionId) throws NotFoundException {
        WebContent webContent = visitorFindWebContentService.getByVersionId(versionId);
        return webContentMapper
                .forVisitor(webContent, webContent.getUser(), webContent.getWebContentType(), webContent.getWebContentCategories());
    }


    @Override
    public WebContent getHomePage() throws NotFoundException {
        return visitorFindWebContentService.getByVersionId(DefaultVersionId.HOME_PAGE_WEB_CONTENT.getId());
    }


    @Override
    public WebContent getLegalMentionsPage() throws NotFoundException {
        return visitorFindWebContentService.getByVersionId(DefaultVersionId.LEGAL_MENTIONS_WEB_CONTENT.getId());

    }


    @Override
    public List<WebContentDto> getAllPublishedSorted(String sort) {
        return filterSortAndMapWebContents(sort, visitorFindWebContentService.getAll());

    }


    @Override
    public List<WebContentDto> getAllByTypeSorted(long typeVersionId, String sort) throws NotFoundException {
        return filterSortAndMapWebContents(sort, visitorFindWebContentService.getAllByType(typeVersionId));
    }


    @Override
    public List<WebContentDto> getAllByCategorySorted(Long categoryVersionId, String sort) throws NotFoundException {
        return filterSortAndMapWebContents(sort, visitorFindWebContentService.getAllByCategory(categoryVersionId));
    }


    @Override
    public List<WebContentDto> getAllByTypeAndCategorySorted(long typeVersionId, long categoryVersionId, String sort) throws NotFoundException {

        return filterSortAndMapWebContents(sort, visitorFindWebContentService.getAllByTypeAndCategory(typeVersionId, categoryVersionId));
    }


    @Override
    public List<WebContentDto> getAllByEditorSorted(long editorId, String sort) throws NotFoundException {
        return filterSortAndMapWebContents(sort, visitorFindWebContentService.getAllByEditor(editorId));
    }


    @Override
    public List<WebContentDto> getAllByTypeAndUserSorted(long typeVersionId, long userId, String sort) throws NotFoundException {
        return filterSortAndMapWebContents(sort, visitorFindWebContentService.getAllByTypeAndUser(typeVersionId, userId));
    }


    @Override
    public List<WebContentDto> getAllByCategoryAndUserSorted(long categoryVersionId, long userId, String sort) throws NotFoundException {
        return filterSortAndMapWebContents(sort, visitorFindWebContentService.getAllByCategoryAndUser(categoryVersionId, userId));

    }


    @Override
    public List<WebContentDto> getAllByTypeAndCategoryAndUserSorted
            (long typeVersionId, long categoryVersionId, long userId, String sort) throws NotFoundException {
        return filterSortAndMapWebContents(sort, visitorFindWebContentService.getAllByTypeAndCategoryAndUser(typeVersionId, categoryVersionId, userId));
    }


    private List<WebContentDto> filterSortAndMapWebContents(String sort, List<WebContent> webContents) {
        webContents.removeIf(wc -> !wc.isInUse() || !wc.isPublished() || wc.getPublishingDate() == null || wc.getSuppressionDate() != null
                || wc.getWebContentType().isHasLimitedModification());
        VisitorComparatorService.sortWebContents(sort, webContents);
        return webContents.stream()
                .map(wc -> webContentMapper.forVisitor(wc, wc.getUser(), wc.getWebContentType(), wc.getWebContentCategories()))
                .collect(Collectors.toList());
    }


}
