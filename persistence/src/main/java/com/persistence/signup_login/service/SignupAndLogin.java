package com.persistence.signup_login.service;


import com.domain.entity.Role;
import com.domain.entity.Token;
import com.domain.entity.User;
import com.persistence.exception.*;
import com.persistence.payload.request.PasswordResetRequest;
import com.persistence.signup_login.payload.request.LoginRequest;
import com.persistence.signup_login.payload.request.UserSignupRequest;
import com.persistence.signup_login.payload.response.LoginJwtResponse;
import org.springframework.security.core.AuthenticationException;

import javax.mail.MessagingException;
import javax.validation.ConstraintViolationException;

public interface SignupAndLogin {


    /**
     * This method returns a {@link User}
     * found using his email address.
     *
     * @param email a {@link String} value representing a email address
     * @return a {@link User}
     */
    User findByEmail(String email);


    /**
     * This method returns a {@link User}
     * found using his pseudo.
     *
     * @param pseudo a {@link String} value representing a pseudo
     * @return a {@link User}
     */
    User findByPseudo(String pseudo);


    /**
     * This method creates a new {@link Token} using a {@link User}.
     *
     * @param user a {@link User}
     * @return a {@link Token}
     */
    Token createTokenUsingUser(User user);


    /**
     * This method verifies if a {@link Token} is valid and if that is
     * the case it validates the email address of the {@link User}
     * owning the {@link Token};
     *
     * @param token a {@link String} value
     * @return a boolean value indicating if the email address was validated (true) or not (false)
     */
    boolean validateUserEmail(String token);


    /**
     * This method verifies if a {@link User} is not deleted.
     *
     * @param user a {@link User}
     * @return a boolean value indicating if the user is not deleted (true), or if it is deleted (false)
     */
    boolean userIsNotDeleted(User user);


    /**
     * This method verifies if a {@link Token} is valid.
     *
     * @param token a {@link String} value
     * @return a boolean value indicating if the {@link Token} is valid (true) or not (false
     */
    boolean tokenIsValid(String token);


    /**
     * This method sends to a {@link User} an email containing a link
     * that will allow him to validate his email address.
     *
     * @param user    a {@link User}
     * @param baseUrl a String value representing the base url of the website
     * @return a boolean value indicating if the email was sent (true) or not (false)
     * @throws MessagingException exception thrown if the  sending the confirmation email fails
     */
    boolean sendSignupConfirmationEmail(User user, String baseUrl) throws MessagingException;


    /**
     * Method sending an email to a {@link User} when someone attempts
     * to create an account using its email address.
     *
     * @param user a {@link User}
     * @throws MessagingException a {@link MessagingException}, thrown when sending an email fails
     */
    void signalMailUseAttempt(User user) throws MessagingException;

    /**
     * This method send to a {@link User} an email with a link that allows him to reset
     * his password.
     *
     * @param email a String value representing an email address
     * @return a boolean value indicating if the email was sent (true) or not (false)
     * @throws UserDeletedException a {@link UserDeletedException}, thrown if the email address
     *                              belong to a deleted {@link User}
     * @throws NotFoundException    a {@link NotFoundException}, thrown if there is no {@link User} is found using the email address
     * @throws MessagingException   exception thrown if the  sending the email fails
     */
    boolean sendPasswordResetEmail(String email) throws UserDeletedException, NotFoundException, MessagingException;


    /**
     * This method resets a {@link User}'s password.
     *
     * @param passwordResetRequest a {@link PasswordResetRequest} object
     * @return a boolean value indicating if the password was reseted (true) or not (false)
     * @throws PasswordsNotMatchingException a {@link PasswordsNotMatchingException}, thrown if the password and
     *                                       the password confirmation are not identical
     * @throws UserDeletedException          a {@link UserDeletedException}, thrown if the {@link User} trying yo reset its password is deleted
     * @throws TokenNotValidException        a {@link TokenNotValidException}, thrown if the {@link Token} used for validating the password
     *                                       request is not valid
     */
    boolean resetPassword(PasswordResetRequest passwordResetRequest) throws PasswordsNotMatchingException,
            UserDeletedException, TokenNotValidException;


    /**
     * This method creates a new {@link User}.
     *
     * @param request a {@link UserSignupRequest}
     * @return a {@link User} object
     * @throws NotFoundException            a {@link NotFoundException}, thrown if the
     *                                      'ROLE_USER' {@link Role} is not found
     * @throws PseudoAlreadyInUseException  a {@link PseudoAlreadyInUseException},
     *                                      thrown if the pseudo is already used by another {@link User}
     * @throws EmailNotAvailableException   a {@link EmailNotAvailableException}, thrown if
     *                                      the email used for signing up is not available
     * @throws EmailAlreadyOwnedException   a {@link EmailAlreadyOwnedException}, thrown if the email address is used by another {@link User}
     * @throws SignupEmailException         a {@link SignupEmailException}, thrown if the confirmation mail sending fails
     * @throws ConstraintViolationException exception thrown when the {@link User} validation constraints
     *                                      are not respected
     */
    User userSignup(UserSignupRequest request) throws NotFoundException, PseudoAlreadyInUseException, SignupEmailException,
            EmailNotAvailableException, EmailAlreadyOwnedException, ConstraintViolationException;


    /**
     * Method allowing a {@link User} to login in using his email address and password.
     *
     * @param loginRequest a {@link LoginRequest}
     * @return a {@link LoginJwtResponse}
     * @throws AuthenticationException a {@link AuthenticationException}, thrown if the login credentials are not valid
     */
    LoginJwtResponse login(LoginRequest loginRequest) throws AuthenticationException;



}
