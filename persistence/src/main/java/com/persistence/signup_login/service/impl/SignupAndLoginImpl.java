package com.persistence.signup_login.service.impl;

import com.domain.entity.Role;
import com.domain.entity.Token;
import com.domain.entity.User;
import com.persistence.constant.sort_object.DateSortObject;
import com.persistence.exception.*;
import com.persistence.repository.RoleRepository;
import com.persistence.repository.TokenRepository;
import com.persistence.repository.UserRepository;
import com.persistence.payload.request.PasswordResetRequest;
import com.persistence.signup_login.payload.request.LoginRequest;
import com.persistence.signup_login.payload.request.UserSignupRequest;
import com.persistence.signup_login.payload.response.LoginJwtResponse;
import com.persistence.signup_login.security.jwt.JwtUtils;
import com.persistence.signup_login.security.service.UserDetailsImpl;
import com.persistence.signup_login.service.SignupAndLogin;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.email.service.EmailService;

import javax.mail.MessagingException;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * Class providing the methods used for signup and for
 * login.
 * <p>
 * The all arguments constructor was implemented implicitly using {@link lombok.Lombok} annotations.
 */
@Log4j2
@Service
public class SignupAndLoginImpl implements SignupAndLogin {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final TokenRepository tokenRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;
    private final EmailService emailService;


    /**
     * Constructor instantiating a {@link SignupAndLoginImpl}
     * with all the class private final properties.
     *
     * @param userRepository        a {@link UserRepository}, used for handling {@link User} instances
     * @param passwordEncoder       a {@link PasswordEncoder}, used for hashing a {@link User}'s password before persisting his account
     * @param roleRepository        a {@link RoleRepository}, used for handling {@link Role}s
     * @param tokenRepository       a {@link TokenRepository}, used for handling {@link Token}s
     * @param authenticationManager a {@link AuthenticationManager}, used for handling {@link User} authentication with SpringSecurity
     * @param jwtUtils              a {@link JwtUtils}, used for generating JWT
     * @param emailService          a {@link EmailService}, used for sending emails
     */
    public SignupAndLoginImpl(UserRepository userRepository,
                              PasswordEncoder passwordEncoder,
                              RoleRepository roleRepository,
                              TokenRepository tokenRepository,
                              AuthenticationManager authenticationManager,
                              JwtUtils jwtUtils,
                              EmailService emailService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.tokenRepository = tokenRepository;
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
        this.emailService = emailService;
    }



    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }


    @Override
    public User findByPseudo(String pseudo) {
        return userRepository.findByPseudo(pseudo);
    }


    @Override
    public Token createTokenUsingUser(User user) {
        Token token = Token.builder().user(user).build();
        while (tokenRepository.findByToken(token.getToken()) != null) {
            token.setToken(Token.generateToken());
        }
        return tokenRepository.save(token);
    }


    @Override
    public boolean validateUserEmail(String token) {
        Token dbToken = tokenRepository.findByTokenAndExpirationDateIsAfter(token, LocalDateTime.now());
        if (dbToken == null) {
            return false;
        } else {
            User user = userRepository.findByTokensContains(dbToken);
            if (user.getSuppressionDate() != null) {
                return false;
            }
            user.setEmailValidated(true);
            userRepository.save(user);
            tokenRepository.delete(dbToken);
            return true;
        }
    }


    @Override
    public boolean userIsNotDeleted(User user) {
        return user.getSuppressionDate() == null;
    }


    @Override
    public boolean tokenIsValid(String token) {
        return tokenRepository.findByTokenAndExpirationDateIsAfter(token, LocalDateTime.now()) != null;
    }


    @Override
    public boolean sendSignupConfirmationEmail(User user, String baseUrl) throws MessagingException {

        Token token = createTokenUsingUser(user);
        return emailService.sendSignupConfirmationEmail(user, token);
    }


    @Override
    public void signalMailUseAttempt(User user) throws MessagingException {
        emailService.signalMailUseAttempt(user);
    }


    @Override
    public boolean sendPasswordResetEmail(String email) throws UserDeletedException, NotFoundException, MessagingException {

        User user = findByEmail(email);
        Token token;
        if (user != null) {
            if (user.getSuppressionDate() != null) {
                throw new UserDeletedException();
            } else {
                token = createTokenUsingUser(user);
                return emailService.sendPasswordResetEmail(user, token);
            }
        }
        throw new NotFoundException();
    }


    @Override
    public boolean resetPassword(PasswordResetRequest passwordResetRequest) throws PasswordsNotMatchingException,
            UserDeletedException, TokenNotValidException {

        Token token = tokenRepository.findByToken(passwordResetRequest.getToken());
        User user;

        if (token != null) {
            if (tokenIsValid(passwordResetRequest.getToken())) {
                user = userRepository.findByTokensContains(token);
                if (user != null) {
                    if (user.getSuppressionDate() == null) {
                        if (!passwordResetRequest.getNewPassword().equals(passwordResetRequest.getNewPasswordConfirmation())) {
                            throw new PasswordsNotMatchingException();
                        } else {
                            String newPassword = passwordEncoder.encode(passwordResetRequest.getNewPassword());
                            user.setPassword(newPassword);
                            userRepository.save(user);
                            tokenRepository.delete(token);
                            return true;
                        }
                    } else throw new UserDeletedException();
                }
            } else throw new TokenNotValidException();
        }
        return false;
    }


    @Override
    public User userSignup(UserSignupRequest request) throws NotFoundException, PseudoAlreadyInUseException,
            EmailNotAvailableException, EmailAlreadyOwnedException, SignupEmailException, ConstraintViolationException {

        User userToSave = new User();
        List<Role> roles = roleRepository.findAllByInUseIsTrueAndTitle("ROLE_USER");
        if (roles.isEmpty()) {
            throw new NotFoundException();
        }

        Role dbUserRole = roles.get(0);

        if (findByEmail(request.getEmail()) != null) {
            userToSave = findByEmail(request.getEmail());
            if (userToSave.getSuppressionDate() == null) {
                try {
                    signalMailUseAttempt(userToSave);
                } catch (MessagingException e) {
                    log.error(e.getMessage());
                }
                throw new EmailAlreadyOwnedException();
            } else if (userToSave.getSuppressionDate() != null && (!userToSave.isSelfDeleted())) {
                throw new EmailNotAvailableException();
            }
        }


        userToSave.setFirstName(User.formatFirstOrLastName(request.getFirstName()));
        userToSave.setLastName(User.formatFirstOrLastName(request.getLastName()));
        userToSave.setPseudo(User.formatPseudo(request.getPseudo()));
        userToSave.setEmail(request.getEmail());
        userToSave.setPassword(passwordEncoder.encode(request.getPassword()));
        userToSave.setGetNewsletter(request.getGetNewsletter());

        userToSave.setSelfDeleted(false);
        userToSave.setSuppressionDate(null);
        userToSave.setEmailValidated(false);
        userToSave.setProfileImageValidated(false);
        userToSave.setSignUpDate(LocalDateTime.now());

        if (userToSave.getPseudo().equals("anonyme")) {
            throw new PseudoAlreadyInUseException(request.getPseudo());
        }


        if (findByPseudo(userToSave.getPseudo()) != null) {
            if (!findByPseudo(userToSave.getPseudo()).getEmail().equals(userToSave.getEmail())) {
                throw new PseudoAlreadyInUseException(request.getPseudo());
            }
        }


        userToSave = userRepository.save(userToSave);

        CopyOnWriteArrayList<User> dbRoleUsers = userRepository.findAllByRolesContainsAndSuppressionDateIsNull(dbUserRole,
                DateSortObject.ASC_SIGNUP_DATE);
        dbRoleUsers.add(userToSave);
        dbUserRole.setUsers(dbRoleUsers);
        roleRepository.save(dbUserRole);

        try {
            sendSignupConfirmationEmail(userToSave, "http://localhost:4200/inscription/validation/");
        } catch (MessagingException e) {
            log.debug("SignupEmailException: {}", e.getMessage());
            throw new SignupEmailException();
        }
        return userToSave;
    }


    @Override
    public LoginJwtResponse login(LoginRequest loginRequest) throws AuthenticationException {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return new LoginJwtResponse(jwt,
                userDetails.getId(),
                userDetails.getEmail(),
                roles);
    }



}
