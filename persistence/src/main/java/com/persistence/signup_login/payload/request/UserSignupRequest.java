package com.persistence.signup_login.payload.request;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Lombok;
import lombok.ToString;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Class providing the values needed for signing up :
 * email and password.
 * <p>
 * Getters, toString and the all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */
@Getter
@AllArgsConstructor
@ToString
public class UserSignupRequest {

    /**
     * The {@link String} first name value
     */
    @NotBlank
    private final String firstName;


    /**
     * The {@link String} last name value
     */
    @NotBlank
    private final String lastName;

    /**
     * The {@link String} pseudo value
     */
    @NotBlank
    private final String pseudo;


    /**
     * The {@link String} email value
     */
    @NotBlank
    private final String email;


    /**
     * The {@link String} password value
     */
    @NotBlank
    private final String password;


    /**
     * The boolean value indicating if the user wants to receive the newsletter
     */
    @NotNull
    private final Boolean getNewsletter;


}
