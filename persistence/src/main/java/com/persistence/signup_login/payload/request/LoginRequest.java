package com.persistence.signup_login.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Lombok;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

/**
 * Class providing the values needed for login :
 * email and password.
 * <p>
 * The all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */
@Getter
@AllArgsConstructor
@ToString
public class LoginRequest {

    /**
     * The {@link String} email value
     */
    @NotBlank
    private final String email;


    /**
     * The {@link String} password value
     */
    @NotBlank
    private final String password;

}
