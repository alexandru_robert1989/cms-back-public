package com.persistence.signup_login.payload.response;


import com.domain.entity.Role;
import com.domain.entity.User;
import lombok.Getter;
import lombok.Lombok;

import java.util.List;

/**
 * Class providing the properties of the JWT sent
 * to the User Interface.
 * <p>
 * The getters were implemented implicitly using
 * {@link Lombok} annotations.
 */
@Getter
public class LoginJwtResponse {

    /**
     * A {@link String} value representing the token's value
     */
    private final String token;

    /**
     * A {@link String} value representing the token's type
     */
    private final String type = "Bearer";

    /**
     * A {@link Long} value representing the {@link User}'s id
     */
    private final Long id;

    /**
     * A {@link String} value representing the {@link User}'s email address
     */
    private final String email;

    /**
     * A {@link List} of {@link String} values representing the {@link User}'s {@link Role}s titles
     */
    private final List<String> roles;

    /**
     * Constructor instantiating  JWT properties
     *
     * @param accessToken a {@link String} value representing the token's value
     * @param id          a {@link Long} value representing the {@link User}'s id
     * @param email       a {@link String} value representing the {@link User}'s email address
     * @param roles       a {@link List} of {@link String} values representing the {@link User}'s {@link Role}s titles
     */
    public LoginJwtResponse(String accessToken, Long id, String email, List<String> roles) {
        this.token = accessToken;
        this.id = id;
        this.email = email;
        this.roles = roles;
    }
}
