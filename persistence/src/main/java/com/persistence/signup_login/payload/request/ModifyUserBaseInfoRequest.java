package com.persistence.signup_login.payload.request;


import com.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Lombok;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Class providing the values needed for modifying the base info of a {@link User}:
 * its first name, its last name, its pseudo and its newsletter inscription.
 * <p>
 * Getters, toString and the all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */
@Getter
@AllArgsConstructor
@ToString
public class ModifyUserBaseInfoRequest {

    /**
     * The {@link String} first name value
     */
    @NotBlank
    private final String firstName;


    /**
     * The {@link String} last name value
     */
    @NotBlank
    private final String lastName;

    /**
     * The {@link String} pseudo value
     */
    @NotBlank
    private final String pseudo;

    /**
     * The boolean value indicating if the user wants to receive the newsletter
     */
    @NotNull
    private final Boolean getNewsletter;


    /**
     * The {@link String} email value
     */
    @NotBlank
    private final String email;


}
