package com.persistence.signup_login.payload.request;


import com.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Lombok;
import lombok.ToString;

/**
 * Class providing the values needed for changing a {@link User}s email address
 * <p>
 * The all arguments constructor was implemented implicitly using
 * {@link Lombok} annotations.
 */
@Getter
@AllArgsConstructor
@ToString
public class ModifyEmailAddressRequest {

    /**
     * The {@link User}s password.
     */
    private String password;


    /**
     * The {@link User}'s old email address
     */
    private String oldEmail;


    /**
     * The {@link User}'s new email address
     */
    private String newEmail;

}
