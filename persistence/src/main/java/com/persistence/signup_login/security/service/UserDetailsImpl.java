package com.persistence.signup_login.security.service;

import com.domain.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Class providing the logic needed for creating
 * an object containing the {@link User}'s properties
 * and a list of GrantedAuthority objects, needed when
 * using Spring Security and Authentication.
 */
@Getter
public class UserDetailsImpl implements UserDetails {

    private static final long serialVersionUID = 1L;

    //  com.domain.entity.User properties

    private Long id;
    private String firstName;
    private String lastName;
    private String pseudo;
    private String email;
    @JsonIgnore
    private String password;
    private LocalDateTime signUpDate;
    private boolean profileImageValidated;
    private boolean emailValidated;
    private boolean getNewsletter;
    private Collection<? extends GrantedAuthority> roles;

    /**
     * Constructor allowing the creation of a {@link UserDetailsImpl}
     * object.
     *
     * @param id                    a Long value representing the {@link User}'s id
     * @param firstName             a String value representing the {@link User}'s first name
     * @param lastName              a String value epresenting the {@link User}'s last name
     * @param pseudo                a String value epresenting the {@link User}'s pseudo
     * @param email                 a String value epresenting the {@link User}'s email address
     * @param password              a String value epresenting the {@link User}'s password
     * @param signUpDate            a LocalDateTime value representing the {@link User}'s signup date
     * @param profileImageValidated a boolean value indicating if the {@link User}'s profile image was validated
     * @param emailValidated        a boolean value indicating if the {@link User}'s email address was validated
     * @param getNewsletter         a boolean value indicating if the {@link User} receives the newsletter
     * @param roles                 a list of GrantedAuthority objects representing the {@link User}'s roles
     */
    public UserDetailsImpl(Long id, String firstName, String lastName,
                           String pseudo, String email, String password,
                           LocalDateTime signUpDate, boolean profileImageValidated,
                           boolean emailValidated, boolean getNewsletter,
                           Collection<? extends GrantedAuthority> roles) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pseudo = pseudo;
        this.email = email;
        this.password = password;
        this.signUpDate = signUpDate;
        this.profileImageValidated = profileImageValidated;
        this.emailValidated = emailValidated;
        this.getNewsletter = getNewsletter;
        this.roles = roles;
    }

    /**
     * The builder of the {@link UserDetailsImpl}, using a
     * {@link User}.
     *
     * @param user a {@link User} object
     * @return a {@link UserDetailsImpl} object
     */
    public static UserDetailsImpl build(User user) {

        List<GrantedAuthority> roles = user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getTitle()))
                .collect(Collectors.toList());

        return new UserDetailsImpl(user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getPseudo(),
                user.getEmail(),
                user.getPassword(),
                user.getSignUpDate(),
                user.isProfileImageValidated(),
                user.isEmailValidated(),
                user.isGetNewsletter(),
                roles);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserDetailsImpl user = (UserDetailsImpl) o;
        return Objects.equals(id, user.id);
    }

}
