package com.persistence.signup_login.security.jwt;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class implementing {@link AuthenticationEntryPoint},
 * a Spring Security interface providing a method that sends an error
 * message when a {@link AuthenticationException} is triggered.
 */
@Component
public class AuthEntryPointJwt implements AuthenticationEntryPoint {


    /**
     * {@link AuthenticationEntryPoint} method
     * implementation, sending  an error response
     * message when a {@link AuthenticationException} is triggered.
     *
     * @param request       a {@link HttpServletRequest} request
     * @param response      a {@link HttpServletResponse} response
     * @param authException a {@link AuthenticationException} exception
     * @throws IOException      an {@link IOException}
     * @throws ServletException a {@link ServletException}
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException)
            throws IOException, ServletException {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Erreur d'autorisation");

    }
}
