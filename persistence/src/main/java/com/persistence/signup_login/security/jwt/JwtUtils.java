package com.persistence.signup_login.security.jwt;

import com.persistence.signup_login.security.service.UserDetailsImpl;
import io.jsonwebtoken.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Class containing the logic needed for creating and
 * validating a JWT (JSON Web Token).
 */
@Log4j2
@Component
public class JwtUtils {

    /**
     * The value used to sign the JWT.
     */
    @Value("${api.jwtSecret}")
    private String jwtSecret;

    /**
     * The value used to set the JWT expiration time, in ms.
     */
    @Value("${api.jwtExpirationMs}")
    private int jwtExpirationMs;

    /**
     * Method used for generating and returning a String
     * value representing the encoded value of a JWT.
     *
     * @param authentication a {@link Authentication} object
     * @return a String value
     */
    public String generateJwtToken(Authentication authentication) {

        UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject(userPrincipal.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();

    }


    /**
     * Method returning the username provided by the encoded
     * value of a JWT.
     *
     * @param token a String value
     * @return a String value
     */
    public String getUserNameFromJwtToken(String token) {

        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    /**
     * Method checking the validity of a JWT's encoded
     * value.
     *
     * @param authToken a String value
     * @return a boolean
     */
    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            log.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            log.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            log.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            log.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            log.error("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }
}
