package com.persistence.signup_login.security.service;

import com.domain.entity.User;
import com.persistence.exception.NotFoundException;
import com.persistence.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {


    private final UserRepository userRepository;

    /**
     * If the {@link User} is found by its email,
     * the method returns a {@link UserDetails} object,
     * build using the found {@link User}.
     *
     * @param email a String value representing the {@link User}'s email address
     * @return a {@link UserDetails} object
     * @throws UsernameNotFoundException thrown
     *                                   if the {@link User} is not found or if the {@link User} was suppressed.
     */
    @Transactional
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        User user = userRepository.findByEmail(email);
        if (user != null && user.getSuppressionDate() == null) {
            return UserDetailsImpl.build(user);
        } else throw new NotFoundException("\"Aucun utilisateur avec l'adresse :" + email);


    }


}
