package com.persistence.repository;

import com.domain.entity.Token;
import com.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Interface implementing the {@link JpaRepository} interface, thus supplying the CRUD methods needed
 * for recovering and manipulating {@link Token} properties.
 */
@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {

    /**
     * Find a {@link Token} by its token {@link String} value.
     *
     * @param token the {@link Token}'s token String value
     * @return a {@link Token}
     */
    Token findByToken(String token);


    /**
     * Find a {@link Token} that has a expiration date superior
     * to the {@link LocalDateTime} parameter now's value.
     *
     * @param token the {@link Token}'s token {@link String} value
     * @param now   a {@link LocalDateTime}
     * @return a {@link Token}
     */
    Token findByTokenAndExpirationDateIsAfter(String token, LocalDateTime now);


    /**
     * Find a {@link User}'s tokens ({@link Token}).
     *
     * @param user a {@link User} object
     * @return a list of {@link Token} objects
     */
    CopyOnWriteArrayList<Token> findAllByUser(User user);


    /**
     * Find all the  {@link Token}s having an expiration date superior to the value
     * of the {@link LocalDateTime} now parameter.
     *
     * @param now a {@link LocalDateTime}
     * @return a {@link java.util.List} of {@link Token}s
     */
    CopyOnWriteArrayList<Token> findAllByExpirationDateAfter(LocalDateTime now);


    /**
     * Find all the {@link Token}s having an expiration date inferior to the value
     * of the {@link LocalDateTime} now parameter.
     *
     * @param now a {@link LocalDateTime}
     * @return list of {@link Token}s
     */
    CopyOnWriteArrayList<Token> findAllByExpirationDateBefore(LocalDateTime now);


}
