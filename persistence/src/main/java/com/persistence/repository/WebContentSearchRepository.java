package com.persistence.repository;

import com.domain.entity.WebContent;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.concurrent.CopyOnWriteArrayList;

public interface WebContentSearchRepository extends JpaRepository<WebContent, Long> {


    /**
     * Find all the ppublished  {@link WebContent}s containing the {@link String} keyword
     * in their title/
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param sort    a {@link Sort} object used for sorting data
     * @param keyword a {@link String} value used as keyword
     * @return a {@link java.util.List} of {@link WebContent}s
     */
    CopyOnWriteArrayList<WebContent> findAllByTitleContainsAndPublishedIsTrue
    (String keyword, Sort sort);


    /**
     * Find all the published {@link WebContent}s containing the {@link String} keyword
     * in their raw content.
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param keyword the {@link String} value of the keyword used to find all the {@link WebContent}s containing the keyword in their raw content
     * @param sort    a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link WebContent}s
     */
    CopyOnWriteArrayList<WebContent> findAllByRawContentContainsAndPublishedIsTrue
    (String keyword, Sort sort);


    /**
     * Find not suppressed {@link WebContent}s containing the {@link String} keyword
     * in their title or raw content and matching the values of the parameters isPublished (boolean) and
     * submittedForPublication (boolean).
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param keyword     the {@link String} value of the keyword used to find all the {@link WebContent}s containing the keyword in their title or  raw content
     * @param keywordCopy the {@link String} value of the keyword used to find all the {@link WebContent}s containing the keyword in the title or  raw content
     * @param sort        a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link WebContent}s
     */
    CopyOnWriteArrayList<WebContent> findAllByTitleContainsOrRawContentContainsAndPublishedIsTrue
    (String keyword, String keywordCopy, Sort sort);


}
