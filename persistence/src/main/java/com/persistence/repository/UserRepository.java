package com.persistence.repository;

import com.domain.entity.*;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Interface implementing the {@link JpaRepository} interface, thus supplying the CRUD methods needed
 * for recovering and manipulating {@link User} properties.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Find a {@link User} by its email address.
     *
     * @param email a {@link String} email address value
     * @return a {@link User}
     */
    User findByEmail(String email);


    /**
     * Find a {@link User} by its pseudo.
     *
     * @param pseudo the {@link User}'s {@link String} pseudo value
     * @return a {@link User}
     */
    User findByPseudo(String pseudo);


    /**
     * Find a {@link User} using a {@link Token}.
     *
     * @param token a {@link Token}
     * @return a {@link User}
     */
    User findByTokensContains(Token token);


    /**
     * Find a not suppressed {@link User} using a {@link WebContent}.
     *
     * @param webContent a {@link WebContent}
     * @return a {@link User}
     */
    User findByWebContentsContainsAndSuppressionDateIsNull(WebContent webContent);

    User findByIdAndRolesContains(Long id, Role role);


    /**
     * Find all the not suppressed {@link User}s having the same
     * {@link Role}.
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param role a {@link Role} object
     * @param sort a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link User}s
     */
    CopyOnWriteArrayList<User> findAllByRolesContainsAndSuppressionDateIsNull(Role role, Sort sort);


    /**
     * Find a {@link User} using a {@link Comment}
     *
     * @param comment a {@link Comment}
     * @return a {@link User}
     */
    User findByCommentsContainsAndSuppressionDateIsNull(Comment comment);


    CopyOnWriteArrayList<User> findAllByRolesContains(Role role);

    User findByWebContentsContains(WebContent webContent);

    User findByCommentsContains(Comment comment);

    User findByIdAndRolesContainsAndSuppressionDateIsNull(Long id, Role editorRole);

    CopyOnWriteArrayList<User> findAllByRolesContainsAndSuppressionDateIsNull(Role editorRole);
}
