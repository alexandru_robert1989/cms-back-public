package com.persistence.repository;

import com.domain.entity.Message;
import com.domain.entity.User;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Interface implementing the {@link JpaRepository} interface, thus supplying the CRUD methods needed
 * for recovering and manipulating {@link Message} properties.
 */
@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {


    // TODO implement all MessageRepository methods

    /**
     * Find all the {@link Message}s received by a {@link User}.
     *
     * @param recipient the {@link User} that received the {@link Message}s
     * @param sort      a {@link Sort} object
     * @return a {@link java.util.List} of {@link Message}s
     */
    CopyOnWriteArrayList<Message> findDistinctByRecipientsContains(User recipient, Sort sort);


    /**
     * Find all the not suppressed {@link Message}s received by a {@link User}.
     *
     * @param recipient the {@link User} that received the {@link Message}s
     * @param sort      a {@link Sort} object
     * @return a {@link java.util.List} of {@link Message}s
     */
    CopyOnWriteArrayList<Message> findDistinctByRecipientsContainsAndSuppressionDateIsNull(User recipient, Sort sort);


    /**
     * Find all the not suppressed {@link Message}s received by a {@link User} as a response to a sent {@link Message}.
     *
     * @param recipient the {@link User} that received the {@link Message}s
     * @param sort      a {@link Sort} object
     * @return a {@link java.util.List} of {@link Message}s
     */
    CopyOnWriteArrayList<Message> findDistinctByRecipientsContainsAndSuppressionDateIsNullAndParentMessageIsNotNull(User recipient, Sort sort);


    /**
     * Find all the not suppressed {@link Message}s sent by a {@link User}, that are not a response to a sent {@link Message}.
     *
     * @param recipient the {@link User} that sent the {@link Message}s
     * @param sort      a {@link Sort} object
     * @return a {@link java.util.List} of {@link Message}s
     */
    CopyOnWriteArrayList<Message> findDistinctByRecipientsContainsAndSuppressionDateIsNullAndParentMessageIsNull(User recipient, Sort sort);


    /**
     * Find all the not suppressed {@link Message}s sent by a {@link User}.
     *
     * @param sender the {@link User} that sent the {@link Message}s
     * @param sort   a {@link Sort} object
     * @return a {@link java.util.List} of {@link Message}s
     */
    CopyOnWriteArrayList<Message> findDistinctBySenderAndSuppressionDateIsNull(User sender, Sort sort);


    /**
     * Find all the not suppressed {@link Message}s sent by a {@link User} as a response to a {@link Message}.
     *
     * @param sender the {@link User} that sent the {@link Message}s
     * @param sort   a {@link Sort} object
     * @return a {@link java.util.List} of {@link Message}s
     */
    CopyOnWriteArrayList<Message> findDistinctBySenderAndSuppressionDateIsNullAndParentMessageIsNotNull(User sender, Sort sort);


    /**
     * Find all the not suppressed {@link Message}s sent by a {@link User} for starting a new {@link Message} exchange.
     *
     * @param sender the {@link User} that sent the {@link Message}s
     * @param sort   a {@link Sort} object
     * @return a {@link java.util.List} of {@link Message}s
     */
    CopyOnWriteArrayList<Message> findDistinctBySenderAndSuppressionDateIsNullAndParentMessageIsNull(User sender, Sort sort);


}
