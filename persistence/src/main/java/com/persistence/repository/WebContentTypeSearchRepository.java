package com.persistence.repository;

import com.domain.entity.WebContentType;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Interface implementing the {@link JpaRepository} interface,  supplying methods needed
 * for searching {@link WebContentType}s.
 */
@Repository
public interface WebContentTypeSearchRepository extends JpaRepository<WebContentType, Long> {

    /**
     * Find all the in use (published or not) {@link WebContentType}  instances
     * containing the {@link String} keyword parameter value in their title or description.
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param keyword     the {@link String} value of the keyword used to find all the {@link WebContentType} by their title and description content
     * @param keywordCopy the {@link String} value of the keyword used to find all the {@link WebContentType} by their title and description content
     * @param sort        a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link WebContentType}s
     */
    CopyOnWriteArrayList<WebContentType>
    findAllByInUseIsTrueAndTitleContainsOrDescriptionContains(String keyword, String keywordCopy, Sort sort);

}
