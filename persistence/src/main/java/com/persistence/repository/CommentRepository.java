package com.persistence.repository;

import com.domain.entity.Comment;
import com.domain.entity.User;
import com.domain.entity.WebContent;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Interface implementing the {@link JpaRepository} interface, thus supplying the CRUD methods needed
 * for recovering and manipulating {@link Comment} properties.
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {


    /**
     * Find all the {@link Comment}s of a {@link WebContent}.
     *
     * @param webContent a {@link WebContent}
     * @return a {@link java.util.List} of {@link Comment}
     */
    CopyOnWriteArrayList<Comment> findAllByWebContent(WebContent webContent);



    /**
     * Find all the published {@link Comment}s of a {@link WebContent}.
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param webContent a {@link WebContent} object
     * @param sort       a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link Comment}s
     */
    CopyOnWriteArrayList<Comment> findAllByWebContentAndPublishingDateIsNotNull(WebContent webContent, Sort sort);


    /**
     * Find all published the parent {@link Comment}s of a {@link WebContent}.
     *
     * @param webContent a {@link WebContent}
     * @param sort       a {@link Sort} object
     * @return a {@link java.util.List} of {@link WebContent}
     */
    CopyOnWriteArrayList<Comment> findAllByWebContentAndPublishingDateIsNotNullAndParentCommentIsNull(WebContent webContent, Sort sort);


    /**
     * Find all the  published
     * children (reactions)  {@link Comment}s by their parent {@link Comment}.
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param parentComment a {@link Comment} object
     * @param sort          a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link Comment}s
     */
    CopyOnWriteArrayList<Comment> findAllByParentCommentAndPublishingDateIsNotNull(Comment parentComment, Sort sort);


    List<Comment> findAllByParentCommentId(Long id);


    List<Comment> findAllByParentComment(Comment comment);

    CopyOnWriteArrayList<Comment> findAllByUser(User u);
}
