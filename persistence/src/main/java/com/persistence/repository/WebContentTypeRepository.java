package com.persistence.repository;

import com.domain.entity.WebContent;
import com.domain.entity.WebContentType;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Interface implementing the {@link JpaRepository} interface, thus supplying the CRUD methods needed
 * for recovering and manipulating {@link WebContentType} properties.
 */
@Repository
public interface WebContentTypeRepository extends JpaRepository<WebContentType, Long> {


    /**
     * Method finding all the in use {@link WebContentType}s having the same common title
     *
     * @param title a {@link String} value
     * @return a {@link List} of {@link WebContentType}
     */
    CopyOnWriteArrayList<WebContentType> findAllByTitleAndInUseIsTrue(String title);


    /**
     * Find all the {@link WebContentType}s having the same version id.
     *
     * @param versionId a long value representing the version id of one ore more {@link WebContentType}s
     * @param sort      a {@link Sort} object
     * @return a {@link List} of {@link WebContentType}s
     */
    CopyOnWriteArrayList<WebContentType> findAllByVersionId(long versionId, Sort sort);


    /**
     * Find all the in use {@link WebContentType}s by their version id.
     *
     * @param versionId a long value representing the version id of a {@link WebContentType}
     * @return a {@link List} of {@link WebContentType}s
     */
    CopyOnWriteArrayList<WebContentType> findAllByVersionIdAndInUseIsTrue(long versionId);


    /**
     * Find all the in use not suppressed {@link WebContentType} by their version id
     *
     * @param versionId a long value representing a {@link WebContentType} version id
     * @return a {@link List} of {@link WebContentType}s
     */
    CopyOnWriteArrayList<WebContentType> findAllByVersionIdAndInUseIsTrueAndSuppressionDateIsNull(long versionId);


    /**
     * Method finding all the {@link WebContentType}s having the same common title
     * and a limited modification
     *
     * @param title a {@link String} value
     * @return a {@link List} of {@link WebContentType}
     */
    CopyOnWriteArrayList<WebContentType> findAllByTitleAndHasLimitedModificationIsTrue(String title);


    /**
     * Find the in use version of a {@link WebContentType} by its version id.
     *
     * @param id a long value representing the {@link WebContentType}'s version id
     * @return a {@link java.util.List} of {@link WebContentType}s
     */
    CopyOnWriteArrayList<WebContentType> findAllByVersionIdAndInUseIsTrueOrderByEditionDateDesc(long id);


    /**
     * Find a in use (published or not) {@link WebContentType} version using
     * a {@link WebContent}.
     *
     * @param webContent a {@link WebContent}
     * @return a {@link WebContentType}
     */
    WebContentType findByInUseIsTrueAndWebContentsContains(WebContent webContent);


    /**
     * Find all the in use (published or not) {@link WebContentType}s versions using a
     * {@link WebContent}
     *
     * @param webContent a {@link WebContent}
     * @return a {@link java.util.List} of {@link WebContentType}s
     */
    CopyOnWriteArrayList<WebContentType> findAllByInUseIsTrueAndWebContentsContainsOrderByEditionDateDesc(WebContent webContent);


    /**
     * Find all the in use (published or not) {@link WebContentType}s versions using a
     * {@link WebContent}
     *
     * @param webContent a {@link WebContent}
     * @return a {@link java.util.List} of {@link WebContentType}s
     */
    CopyOnWriteArrayList<WebContentType> findAllByWebContentsContainsAndInUseIsTrueAndSuppressionDateIsNull(WebContent webContent);

    /**
     * Find all in use (published or not) {@link WebContentType}s versions  using
     * a {@link WebContentType}.
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param webContentType a {@link WebContentType}
     * @param sort           a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link WebContentType}s
     */
    CopyOnWriteArrayList<WebContentType> findAllByInUseIsTrueAndWebContentType(WebContentType webContentType, Sort sort);


    /**
     * Find all the published (so in use) {@link WebContentType}s version using
     * a {@link WebContentType}.
     *
     * @param webContentType a {@link WebContentType}
     * @param sort           a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link WebContentType}s
     */
    CopyOnWriteArrayList<WebContentType> findAllByInUseIsTrueAndPublishedIsTrueAndWebContentType(WebContentType webContentType, Sort sort);


    /**
     * Find all the in use (published or not) {@link WebContentType}s versions.
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param sort a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link WebContentType}s
     */
    CopyOnWriteArrayList<WebContentType> findAllByInUseIsTrue(Sort sort);

    CopyOnWriteArrayList<WebContentType> findAllByInUseIsTrue();



    /**
     * Find all the {@link WebContentType}s having  limited modification.
     *
     * @return a {@link java.util.List} of {@link WebContentType}s
     */
    CopyOnWriteArrayList<WebContentType> findAllByHasLimitedModificationIsTrue();


    /**
     * Find all the {@link WebContentType}s having the same common edition date.
     *
     * @param dateTime a {@link LocalDateTime} value
     * @return a {@link List} of {@link WebContentType}s
     */
    CopyOnWriteArrayList<WebContentType> findAllByEditionDate(LocalDateTime dateTime);


    CopyOnWriteArrayList<WebContentType> findAllByInUseIsTrueAndPublishedIsTrue();

    CopyOnWriteArrayList<WebContentType> findAllByInUseIsTrueAndWebContentType(WebContentType webContentType);

    CopyOnWriteArrayList<WebContentType> findAllByWebContentsContains(WebContent webContent);

    CopyOnWriteArrayList<WebContentType> findAllByInUseIsTrueAndIsNavbarItemIsTrue();

    CopyOnWriteArrayList<WebContentType> findAllByInUseIsTrueAndIsFooterItemIsTrue();
}
