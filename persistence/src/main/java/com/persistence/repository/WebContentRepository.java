package com.persistence.repository;

import com.domain.entity.*;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Interface implementing the {@link JpaRepository} interface, thus supplying the CRUD methods needed
 * for recovering and manipulating {@link WebContent} properties.
 */
@Repository
public interface WebContentRepository extends JpaRepository<WebContent, Long> {


    CopyOnWriteArrayList<WebContent> findAllByVersionId(long versionId);

    CopyOnWriteArrayList<WebContent> findAllByVersionIdAndInUseIsTrue(long versionId);

    CopyOnWriteArrayList<WebContent> findAllByVersionIdAndInUseIsTrueAndPublishedIsTrue(long versionId);


    CopyOnWriteArrayList<WebContent> findAllByVersionIdAndInUseIsTrueAndPublishedIsTrueAndSuppressionDateIsNull(long versionId);

    CopyOnWriteArrayList<WebContent> findAllByAcceptsCommentsIsTrueAndPublishedIsTrue();

    CopyOnWriteArrayList<WebContent> findAllByPublishedIsTrue(Sort sort);


    CopyOnWriteArrayList<WebContent> findAllByWebContentTypeIdOrWebContentTypeWebContentTypeIdAndPublishedIsTrueAndInUseIsTrueAndSuppressionDateIsNull
            (long wctId, long parentWctId, Sort sort);

    CopyOnWriteArrayList<WebContent> findAllByWebContentTypeIdOrWebContentTypeWebContentTypeIdAndPublishedIsTrueAndInUseIsTrueAndSuppressionDateIsNull
            (long wctId, long parentWctId);


    CopyOnWriteArrayList<WebContent> findAllByWebContentTypeIdOrWebContentTypeWebContentTypeId(long wctId, long parentWctId, Sort sort);

    CopyOnWriteArrayList<WebContent> findAllByWebContentTypeIdOrWebContentTypeWebContentTypeId(long wctId, long parentWctId);


    CopyOnWriteArrayList<WebContent> findAllByWebContentCategoriesContainsAndPublishedIsTrueAndInUseIsTrueAndSuppressionDateIsNull
            (WebContentCategory webContentCategory, Sort sort);


    CopyOnWriteArrayList<WebContent>
    findAllByPublishedIsTrueAndWebContentTypeIdOrWebContentTypeWebContentTypeIdAndWebContentCategoriesContains
            (Long webContentTypeId, Long parentWebContentTypeId, WebContentCategory webContentCategory, Sort sort);


    CopyOnWriteArrayList<WebContent> findAllByUserAndPublishedIsTrue(User user, Sort sort);


    CopyOnWriteArrayList<WebContent>
    findAllByUserAndPublishedIsTrueAndWebContentTypeIdOrWebContentTypeWebContentTypeId
            (User user, Long webContentTypeId, Long parentWebContentTypeId, Sort sort);


    CopyOnWriteArrayList<WebContent> findAllByUserAndPublishedIsTrueAndWebContentCategoriesContains
            (User user, WebContentCategory webContentCategory, Sort sort);


    CopyOnWriteArrayList<WebContent>
    findAllByUserAndPublishedIsTrueAndWebContentTypeIdOrWebContentTypeWebContentTypeIdAndWebContentCategoriesContains
            (User user, Long webContentTypeId,
             Long parentWebContentTypeId, WebContentCategory webContentCategory, Sort sort);


    CopyOnWriteArrayList<WebContent> findAllByWebContentCategoriesContains(WebContentCategory webContentCategory);


    CopyOnWriteArrayList<WebContent> findAllByWebContentTypeHasLimitedModificationIsTrue();


    CopyOnWriteArrayList<WebContent> findAllByUser(User user);


    CopyOnWriteArrayList<WebContent> findAllByPublishedIsTrue();

    CopyOnWriteArrayList<WebContent> findAllByWebContentTypeIdOrWebContentTypeWebContentTypeIdAndWebContentCategoriesContains(Long id, Long id1, WebContentCategory category);

    CopyOnWriteArrayList<WebContent> findAllByWebContentTypeIdOrWebContentTypeWebContentTypeIdAndUser(Long id, Long id1, User user);

    CopyOnWriteArrayList<WebContent> findAllByWebContentCategoriesContainsAndUser(WebContentCategory category, User user);

    CopyOnWriteArrayList<WebContent> findAllByPublishedIsTrueAndWebContentTypeIdOrWebContentTypeWebContentTypeIdAndWebContentCategoriesContainsAndUser
            (Long id, Long id1, WebContentCategory category, User user);

    CopyOnWriteArrayList<WebContent> findAllByRawContentContains(String keywordOrExpression);

}
