package com.persistence.repository;

import com.domain.entity.WebContent;
import com.domain.entity.WebContentCategory;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Interface implementing the {@link JpaRepository} interface, thus supplying the CRUD methods needed
 * for recovering and manipulating {@link WebContentCategory}es.
 */
@Repository
public interface WebContentCategoryRepository extends JpaRepository<WebContentCategory, Long> {


    /**
     * Find a not suppressed {@link WebContentCategory} by its id.
     *
     * @param id a long value representing a {@link WebContentCategory} id
     * @return a {@link WebContentCategory}
     */
    WebContentCategory findByIdAndSuppressionDateIsNull(long id);


    /**
     * Find all the versions of a {@link WebContentCategory} by their version id.
     *
     * @param versionId a long value representing a version id
     * @return a {@link java.util.List} ov {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByVersionId(long versionId);


    /**
     * Method returning all the in use {@link WebContentCategory}es found using by their version id.
     *
     * @param versionId a long value representing the version id of a {@link WebContentCategory}
     * @return a {@link java.util.List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByVersionIdAndInUseIsTrue(long versionId);


    /**
     * Find all the not suppressed versions of a {@link WebContentCategory} by their version id.
     *
     * @param versionId a long value representing a version id
     * @return a {@link java.util.List} ov {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByVersionIdAndSuppressionDateIsNull(long versionId, Sort sort);


    /**
     * Find all the suppressed versions of a {@link WebContentCategory} by their version id.
     *
     * @param versionId a long value representing a version id
     * @return a {@link java.util.List} ov {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByVersionIdAndSuppressionDateIsNotNull(long versionId);


    /**
     * Method returning all the in use not suppressed {@link WebContentCategory}es found using by their version id.
     *
     * @param versionId a long value representing the version id of a {@link WebContentCategory}
     * @return a {@link java.util.List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByVersionIdAndInUseIsTrueAndSuppressionDateIsNull(long versionId);


    /**
     * Find all the in use (published or not) {@link WebContentCategory}es versions.
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param sort a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByInUseIsTrue(Sort sort);


    /**
     * Find all the in use (published or not, suppressed or not) {@link WebContentCategory}es versions.
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param sort a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByInUseIsTrueAndSuppressionDateIsNull(Sort sort);

    /**
     * Find all the suppressed {@link WebContentCategory}es
     *
     * @param sort a {@link Sort} object
     * @return a {@link java.util.List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByInUseIsTrueAndSuppressionDateIsNotNull(Sort sort);


    /**
     * Find all the in use  published not suppressed {@link WebContentCategory}es versions.
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param sort a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByInUseIsTrueAndSuppressionDateIsNullAndPublishedIsTrue(Sort sort);


    /**
     * Find all the {@link WebContentCategory}es associated to the same precise {@link WebContent}.
     *
     * @param webContent a {@link WebContent}
     * @param sort       a {@link Sort} object
     * @return a {@link java.util.List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByWebContentsContains(WebContent webContent, Sort sort);


    /**
     * Find all the in use not suppressed {@link WebContentCategory}es associated to the same precise {@link WebContent}.
     *
     * @param webContent a {@link WebContent}
     * @param sort       a {@link Sort} object
     * @return a {@link java.util.List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByWebContentsContainsAndInUseIsTrueAndSuppressionDateIsNull(WebContent webContent, Sort sort);


    /**
     * Find all the {@link WebContentCategory}es having the same edition date.
     *
     * @param editionDate a {@link LocalDateTime} value
     * @return a {@link java.util.List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByEditionDate(LocalDateTime editionDate);


    /**
     * Find all the suppressed {@link WebContentCategory}es
     *
     * @param sort a {@link Sort} object
     * @return a {@link java.util.List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllBySuppressionDateIsNotNull(Sort sort);


    CopyOnWriteArrayList<WebContentCategory> findAllByInUseIsTrue();
}
