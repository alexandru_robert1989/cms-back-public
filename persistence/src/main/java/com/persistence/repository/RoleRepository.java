package com.persistence.repository;

import com.domain.entity.Role;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Interface implementing the {@link JpaRepository} interface, thus supplying the CRUD methods needed
 * for recovering and manipulating {@link Role} properties.
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    /**
     * Method returning all the in use {@link Role}s found by their version id
     *
     * @param versionId a long value representing the version id of a {@link Role}
     * @return a {@link java.util.List} of {@link Role}s
     */
    CopyOnWriteArrayList<Role> findAllByInUseIsTrueAndVersionId(long versionId);


    /**
     * Method returning all the in use {@link Role}s found by their exact title.
     *
     * @param title a {@link String} value
     * @return a {@link java.util.List} of {@link Role}s
     */
    CopyOnWriteArrayList<Role> findAllByInUseIsTrueAndTitle(String title);


    /**
     * Find all the in use {@link Role}s.
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param sort a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link Role}s
     */
    CopyOnWriteArrayList<Role> findAllByInUseIsTrue(Sort sort);

    CopyOnWriteArrayList<Role> findAllByInUseIsTrue();


    CopyOnWriteArrayList<Role> findAllByVersionId(long id);
}
