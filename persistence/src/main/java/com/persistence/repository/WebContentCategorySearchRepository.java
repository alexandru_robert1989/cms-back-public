package com.persistence.repository;

import com.domain.entity.WebContentCategory;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Interface implementing the {@link JpaRepository} interface,  supplying the CRUD methods needed
 * for searching  {@link WebContentCategory} properties.
 */
@Repository
public interface WebContentCategorySearchRepository extends JpaRepository<WebContentCategory, Long> {


    /**
     * Find all the in use (published or not) {@link WebContentCategory}es versions
     * containing the {@link String} keyword parameter value in their title.
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param keyword the {@link String} value used to search {@link WebContentCategory}es by their title content
     * @param sort    a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByInUseIsTrueAndTitleContains(String keyword, Sort sort);


    /**
     * Find all the in use (published or not) {@link WebContentCategory}es versions
     * containing the {@link String} keyword parameter value in their description.
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param keyword the {@link String} value used to search {@link WebContentCategory}es by their description content
     * @param sort    a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByInUseIsTrueAndDescriptionContains(String keyword, Sort sort);


    /**
     * Find all the in use (published or not) {@link WebContentCategory}es versions
     * containing the {@link String} keyword parameter value in their title or description.
     * <p>
     * The recovered data is sorted using a {@link Sort} object.
     *
     * @param keyword     the {@link String} value used to search {@link WebContentCategory}es by their title and description content
     * @param keywordCopy the {@link String} value used to search {@link WebContentCategory}es by their title and description content
     * @param sort        a {@link Sort} object used for sorting data
     * @return a {@link java.util.List} of {@link WebContentCategory}es
     */
    CopyOnWriteArrayList<WebContentCategory> findAllByInUseIsTrueAndTitleContainsOrDescriptionContains(String keyword,
                                                                                                       String keywordCopy, Sort sort);
}
