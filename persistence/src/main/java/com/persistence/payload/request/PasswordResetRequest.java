package com.persistence.payload.request;


import com.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Lombok;
import lombok.ToString;

/**
 * Class providing the properties needed for resetting an
 * {@link User}'s password.
 * <p>
 * Getters, toString and the all arguments constructor were implemented implicitly using
 * {@link Lombok} annotations.
 */
@AllArgsConstructor
@Getter
@ToString
public class PasswordResetRequest {

    /**
     * The value of the actual password
     */
    private final String token;

    /**
     * The value of the new password
     */
    private final String newPassword;

    /**
     * The value of the new password
     */
    private final String newPasswordConfirmation;
}
