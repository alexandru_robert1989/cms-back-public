package com.persistence.payload.request;


import com.domain.entity.Message;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Lombok;
import lombok.ToString;

/**
 * Class providing the properties needed for sending a {@link Message}.
 * <p>
 * Getters, toString and the all arguments constructor were implemented implicitly using
 * {@link Lombok} annotations.
 */
@Getter
@ToString
@AllArgsConstructor
public class MessageRequest {

    /**
     * String value that once converted to long represents the id of a {@link Message}.
     */
    private String parentMessageId;


    /**
     * String value representing the {@link Message} subject
     */
    private String subject;


    /**
     * String value representing the {@link Message} text
     */
    private String text;


    /**
     * String value that once converted to long represents the id of a {@link Message}.
     */
    private String messageId;


    /**
     * boolean valude indicating if a {@link Message} should be deleted.
     */
    private boolean deleteMessage;


}
