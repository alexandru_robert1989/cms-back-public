package com.persistence.payload.request.visitor;


import com.domain.entity.WebContent;
import com.domain.entity.WebContentCategory;
import lombok.*;

/**
 * Class providing the properties used for creating requests for recovering {@link WebContentCategory}es.
 * <p>
 * Getters, setters, toString,the default constructor and the all arguments constructor,
 * were implemented implicitly using {@link Lombok} annotations.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class VisitorGetWebContentCategoryRequest {


    /**
     * A {@link WebContentCategory}'s version id
     */
    private Long versionId;


    /**
     * A {@link WebContent}'s version id
     */
    private Long webContentVersionId;


    /**
     * A {@link String} value used for choosing a {@link org.springframework.data.domain.Sort} object, used for sorting the {@link WebContentCategory}es
     * before returning them.
     * <p>
     * The different valid values of sortingType are : title-asc, title-desc, creation-date-asc,
     * creation-date-desc,total-web-contents-asc, total-web-contents-desc.
     * If a invalid value is provided, the data will be sorted using the decreasing creation dates .
     */
    private String sortTitle;


    /**
     * Boolean value indicating if the method should return all the {@link WebContentCategory}es
     */
    private boolean getAll;


    /**
     * Boolean value indicating if the method should return only the {@link WebContentCategory}es used for sorting {@link WebContent}s
     */
    private boolean getSortingItems;


}
