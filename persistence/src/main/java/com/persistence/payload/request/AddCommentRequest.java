package com.persistence.payload.request;


import com.domain.entity.Comment;
import com.domain.entity.WebContent;
import lombok.*;

/**
 * Class providing the properties needed for receiving adding a {@link Comment}
 * <p>
 * Getters, toString and the all arguments constructor were implemented implicitly using
 * {@link Lombok} annotations.
 */
@Getter
@ToString
@AllArgsConstructor
public class AddCommentRequest {


    /**
     * A {@link String} value that once converted to {@link Long} will represent the id of a {@link Comment}
     */
    private String parentCommentId;


    /**
     * A {@link String} value that once converted to {@link Long} will represent the id of a {@link WebContent}
     */
    private String webContentId;


    /**
     * The title of the {@link Comment} that will be created
     */
    private String title;


    /**
     * The text of the {@link Comment} that will be created
     */
    private String text;



}
