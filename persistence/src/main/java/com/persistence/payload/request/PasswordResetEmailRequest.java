package com.persistence.payload.request;


import com.domain.entity.User;
import lombok.*;

import javax.validation.constraints.NotBlank;

/**
 * Class providing the properties needed for receiving an email with a link that
 * allows a {@link User} to reset his password
 * <p>
 * Getters, toString, the default constructor and the all arguments constructor were implemented implicitly using
 * {@link Lombok} annotations.
 */
@Getter
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class PasswordResetEmailRequest {


    /**
     * The {@link String} email value
     */
    @NotBlank
    private String email;
}
