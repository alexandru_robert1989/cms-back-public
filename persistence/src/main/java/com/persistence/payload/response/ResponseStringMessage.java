package com.persistence.payload.response;


/**
 * Class that can be used to return a message as a response to a HTTP request's result.
 * <p>
 * The getters, setters, to String, equalsAndHashCode, noArgConstructor and
 * AllArgsConstructor methods where implemented implicitly using {@link lombok.Lombok} annotations.
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class ResponseStringMessage {

    /**
     * A {@link String} message
     */
    private String message;

    /**
     * The name of the entity concerned by the message
     */
    private String entityName;


    public ResponseStringMessage(String message) {
        this.message = message;
    }

    public ResponseStringMessage(String message, String entityName) {
        this.message = message;
        this.entityName = entityName;
    }
}

