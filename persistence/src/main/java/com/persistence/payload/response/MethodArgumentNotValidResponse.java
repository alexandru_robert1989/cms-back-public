package com.persistence.payload.response;


import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;
import java.util.stream.Collectors;


public class MethodArgumentNotValidResponse {


    public static List<MethodArgumentNotValid> generate(MethodArgumentNotValidException e) {

        List<MethodArgumentNotValid> exceptions = e.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(err -> new MethodArgumentNotValid(err.getObjectName(), err.getField(), err.getDefaultMessage()))
                .collect(Collectors.toList());
        return exceptions;
    }


}
