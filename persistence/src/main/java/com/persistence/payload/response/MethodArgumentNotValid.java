package com.persistence.payload.response;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class MethodArgumentNotValid {

    private String objectName;
    private String fieldName;
    private String defaultMessage;



}
