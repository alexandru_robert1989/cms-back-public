package com.persistence.exception;


import lombok.Lombok;
import lombok.NoArgsConstructor;

/**
 * Exception thrown when restoring an object fails
 * <p>
 * The default constructor, was implemented implicitly using
 * {@link Lombok} annotations.
 */
@NoArgsConstructor
public class RestoringException extends RuntimeException {


    /**
     * Constructor instantiating a {@link RestoringException} using a String message.
     *
     * @param message a {@link String} value
     */
    public RestoringException(String message) {
        super(message);
    }
}
