package com.persistence.exception;


import com.domain.entity.User;
import lombok.NoArgsConstructor;

/**
 * Exception thrown when the session {@link User} is not found.
 */

@NoArgsConstructor
public class AuthenticationException extends RuntimeException {


    public AuthenticationException(String message) {
        super(message);
    }
}
