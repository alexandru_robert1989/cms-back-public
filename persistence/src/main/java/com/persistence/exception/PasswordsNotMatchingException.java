package com.persistence.exception;


import com.domain.entity.User;
import lombok.Lombok;
import lombok.NoArgsConstructor;

/**
 * Exception thrown when the two password values used for signing up or for
 * resetting a {@link User}'s password do not match.
 * <p>
 * The default constructor, was implemented implicitly using
 * {@link Lombok} annotations.
 */
@NoArgsConstructor
public class PasswordsNotMatchingException extends RuntimeException {

}
