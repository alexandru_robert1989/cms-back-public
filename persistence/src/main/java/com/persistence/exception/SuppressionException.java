package com.persistence.exception;


import lombok.Lombok;
import lombok.NoArgsConstructor;

/**
 * Exception thrown when suppressing an object fails
 * <p>
 * The default constructor, was implemented implicitly using
 * {@link Lombok} annotations.
 */
@NoArgsConstructor
public class SuppressionException extends RuntimeException {


    /**
     * Constructor instantiating a {@link SuppressionException} using a String message.
     *
     * @param message a {@link String} value
     */
    public SuppressionException(String message) {
        super(message);
    }
}
