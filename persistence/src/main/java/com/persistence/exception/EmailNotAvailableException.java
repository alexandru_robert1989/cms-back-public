package com.persistence.exception;


import com.domain.entity.User;

/**
 * Exception thrown if a {@link User} wants to signup
 * using an email address that is not available as it belongs to an
 * account suppressed by the website staff.
 */

public class EmailNotAvailableException extends RuntimeException {

    /**
     * Default constructor of exception thrown if a {@link User} wants to signup
     * using an email address that is not available as it belongs to an account suppressed
     * by the website staff.
     */
    public EmailNotAvailableException() {
    }
}
