package com.persistence.exception;

import lombok.NoArgsConstructor;

/**
 * RuntimeException thrown when attempting to modify an object using
 * the property it already has.
 * <p>
 * The no args constructor was implemented using {@link lombok.Lombok} annotations.
 */
@NoArgsConstructor
public class NothingModifiedException extends RuntimeException {

    /**
     * Constructor allowing the add of a message to the exception
     *
     * @param message a {@link String} value
     */
    public NothingModifiedException(String message) {
        super(message);
    }
}
