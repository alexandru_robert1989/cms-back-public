package com.persistence.exception;

import lombok.NoArgsConstructor;

/**
 * RuntimeException thrown when a object is not found
 * in the database.
 * <p>
 * The no args constructor was implemented using {@link lombok.Lombok} annotations.
 */
@NoArgsConstructor
public class NotFoundException extends RuntimeException {

    /**
     * Constructor allowing the declaration of a personalised message.
     *
     * @param message a String message
     */
    public NotFoundException(String message) {
        super(message);
    }


}
