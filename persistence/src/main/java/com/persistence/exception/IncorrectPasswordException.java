package com.persistence.exception;


import com.domain.entity.User;
import lombok.NoArgsConstructor;

/**
 * Exception thrown if a {@link User} supplies an incorrect password
 */
@NoArgsConstructor
public class IncorrectPasswordException extends RuntimeException {
}
