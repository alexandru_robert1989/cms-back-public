package com.persistence.exception;


import lombok.NoArgsConstructor;

/**
 * Exception thrown when a received request is not valid.
 * <p>
 * The default constructor was implemented using {@link lombok.Lombok} annotations.
 */

@NoArgsConstructor
public class InvalidRequestException extends RuntimeException {

    /**
     * Constructor allowing adding a message to the exception.
     *
     * @param message a {@link String} value
     */
    public InvalidRequestException(String message) {
        super(message);
    }
}
