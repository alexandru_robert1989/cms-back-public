package com.persistence.exception;


import com.domain.entity.User;
import lombok.Getter;
import lombok.Lombok;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Exception thrown if a {@link User} wants to signup
 * using a pseudo  that is not available.
 * <p>
 * Getters and  setters were implemented implicitly using
 * {@link Lombok} annotations.
 */
@Getter
@Setter
@NoArgsConstructor
public class PseudoAlreadyInUseException extends RuntimeException {

    /**
     * A {@link String} value representing a pseudo
     */
    private String pseudo;

    /**
     * A {@link String} value representing a message
     */
    private String pseudoAlreadyInUseMessage;


    /**
     * Constructor initializing a {@link PseudoAlreadyInUseException} using the pseudo {@link String}
     * value.
     *
     * @param pseudo a {@link String} value representing a pseudo
     */
    public PseudoAlreadyInUseException(String pseudo) {
        this.pseudoAlreadyInUseMessage = "Le pseudo \"" + pseudo + "\" n'est pas disponible";
    }


}
