package com.persistence.exception;


import com.domain.entity.User;

/**
 * Exception thrown if a {@link User} wants to signup
 * using an email address that is already used by another {@link User}.
 */
public class EmailAlreadyOwnedException extends RuntimeException {

    /**
     * Default constructor of exception thrown if a {@link User} wants to signup
     * using an email address that is already used by another {@link User}.
     */
    public EmailAlreadyOwnedException() {
    }
}
