package com.persistence.exception;

import com.domain.entity.User;
import lombok.Lombok;
import lombok.NoArgsConstructor;

/**
 * Exception thrown when an unauthorised {@link User} tries to recover a {@link User}
 * with a suppression date.
 * <p>
 * The default constructor, was implemented implicitly using
 * {@link Lombok} annotations.
 */
@NoArgsConstructor
public class UserDeletedException extends RuntimeException {

}
