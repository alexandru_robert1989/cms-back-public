package com.persistence.exception;

import com.domain.entity.User;
import lombok.Lombok;
import lombok.NoArgsConstructor;

/**
 * RuntimeException thrown when a {@link User} succeeds signing up but
 * sending the validation email fails.
 * <p>
 * The default constructor, was implemented implicitly using
 * {@link Lombok} annotations.
 */
@NoArgsConstructor
public class SignupEmailException extends RuntimeException {

    /**
     * Constructor initialising a {@link SignupEmailException}
     * using a {@link String} message.
     *
     * @param message a {@link String} message
     */
    public SignupEmailException(String message) {
        super(message);
    }
}
