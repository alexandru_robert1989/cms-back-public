package com.persistence.exception;

import com.domain.entity.Token;
import lombok.Lombok;
import lombok.NoArgsConstructor;

/**
 * Exception thrown if a {@link Token}
 * is not valid.
 * <p>
 * The default constructor, was implemented implicitly using
 * {@link Lombok} annotations.
 */
@NoArgsConstructor
public class TokenNotValidException extends RuntimeException {


}
