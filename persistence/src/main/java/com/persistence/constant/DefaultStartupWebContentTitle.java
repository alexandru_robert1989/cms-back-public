package com.persistence.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DefaultStartupWebContentTitle {

    HOME_PAGE("Accueil", "Home page"),
    LEGAL_MENTIONS_PAGE("Charte d'utilisation du site", "Legal mentions page");

    private final String frenchTitle;
    private final String englishTitle;

}
