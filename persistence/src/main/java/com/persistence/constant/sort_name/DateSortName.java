package com.persistence.constant.sort_name;

import org.springframework.stereotype.Component;

@Component
public class DateSortName {

    /**
     * Sort by creationDate in chronological order.
     */
    public static final String ASC_CREATION_DATE = "asc-creation-date";


    /**
     * Sort by creationDate in reverse chronological order.
     */
    public static final String DESC_CREATION_DATE = "desc-creation-date";


    /**
     * Sort by publishingDate in chronological order.
     */
    public static final String ASC_PUBLISHING_DATE = "asc-publishing-date";


    /**
     * Sort by publishingDate in reverse chronological order.
     */
    public static final String DESC_PUBLISHING_DATE = "desc-publishing-date";


    /**
     * Sort by editionDate in chronological order.
     */
    public static final String ASC_EDITION_DATE = "asc-edition-date";


    /**
     * Sort by editionDate in reverse chronological order.
     */
    public static final String DESC_EDITION_DATE = "desc-edition-date";


    /**
     * Sort by suppressionDate in chronological order.
     */
    public static final String ASC_SUPPRESSION_DATE = "asc-suppression-date";


    /**
     * Sort by editionDate in reverse chronological order.
     */
    public static final String DESC_SUPPRESSION_DATE = "desc-suppression-date";



    /**
     * Sort by signUpDate in chronological order.
     */
    public static final String ASC_SIGNUP_DATE = "asc_signup-date";


    /**
     * Sort by signUpDate in reverse chronological order.
     */
    public static final String DESC_SIGNUP_DATE = "desc_signup-date";
}
