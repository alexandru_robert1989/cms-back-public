package com.persistence.constant.sort_name;

import org.springframework.stereotype.Component;

@Component
public class TitleSortName {

    /**
     * Sort by title in alphabetical order.
     */
    public static final String ASC_TITLE = "asc-title";


    /**
     * Sort by title in reverse alphabetical order.
     */
    public static final String DESC_TITLE = "desc-title";
}

