package com.persistence.constant.sort_name;

import com.domain.entity.Comment;
import org.springframework.stereotype.Component;

@Component
public class CommentSortName {


    /**
     * Sort by total {@link Comment}s in ascending order.
     */
    public static final String TOTAL_COMMENTS_ASC = "total-comments-asc";


    /**
     * Sort by total {@link Comment}s in descending order.
     */
    public static final String TOTAL_COMMENTS_DESC = "total-comments-desc";

}
