package com.persistence.constant.sort_name;

import org.springframework.stereotype.Component;

@Component
public class SizeSortName {


    /**
     * Sort by size in ascending order.
     */
    public static final String SIZE_ASC = "size-asc";


    /**
     * Sort by size in descending order.
     */
    public static final String SIZE_DESC = "size-desc";

}
