package com.persistence.constant.sort_name;

import com.domain.entity.WebContent;
import org.springframework.stereotype.Component;

@Component
public class TotalWebContentsSortName {


    /**
     * Sort by total {@link WebContent}s in ascending order.
     */
    public static final String TOTAL_WEB_CONTENTS_ASC = "total-web-contents-asc";


    /**
     * Sort by total {@link WebContent}s in descending order.
     */
    public static final String TOTAL_WEB_CONTENTS_DESC = "total-web-contents-desc";
}
