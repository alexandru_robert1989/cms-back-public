package com.persistence.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DefaultStartupWebContentTypeTitle {

    HOME("Accueil", "Home"),
    LEGAL_MENTIONS("Charte", "Legal mentions"),
    INDICATIONS("Indications", "Indications"),
    ARTICLES("Articles", "Articles"),
    REFERENCES("Références", "References"),
    SCIENTIFIC_ARTICLES("Articles scientifiques", "Scientifical articles"),
    CULTURE_ARTICLES("Articles culture", "Culture articles"),
    MEDICINE_ARTICLES("Articles médecine", "Medicine articles"),
    ASTROPHYSICS_ARTICLES("Articles astrophysique", "Astrophysics articles"),
    INTERNET_SITES("Sites Internet", "Internet sites"),
    BOOKS("Livres", "Books");

    private final String frenchTitle;
    private final String englishTitle;

}
