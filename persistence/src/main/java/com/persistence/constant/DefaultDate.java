package com.persistence.constant;


import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * Class providing the default datess for different methods
 * used within the API.
 */
@Component
public class DefaultDate {


    /**
     * The default edition date of the objects created as a demo on application startup.
     */
    public static final LocalDateTime DEFAULT_STARTUP_CREATION_DATE = LocalDateTime.of(1999, 1,1,0,0,0);
}
