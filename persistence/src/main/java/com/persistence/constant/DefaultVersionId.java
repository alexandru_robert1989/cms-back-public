package com.persistence.constant;


import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * Class providing the default version id's  of entities created by default
 * on first application startup.
 */
@AllArgsConstructor
@Getter
public enum DefaultVersionId {

    ROLE_USER(1L),
    ROLE_EDITOR(2L),
    ROLE_PUBLISHER(3L),
    ROLE_MODERATOR(4L),
    ROLE_ADMIN(5L),
    ROLE_SUPER_ADMIN(6L),
//types
    HOME_PAGE_TYPE(1L),
    LEGAL_MENTIONS_TYPE(2L),
    INDICATIONS_TYPE(3L),
    ARTICLE_TYPE(4L),
    REFERENCE_TYPE(5L),
    INTERNET_SITE_TYPE(6L),
    BOOK_TYPE(7L),
    SCIENTIFIC_ARTICLES_TYPE(8L),
    CULTURE_ARTICLES_TYPE(9L),
    MEDICINE_ARTICLES_TYPE(10L),
    ASTROPHYSICS_ARTICLES_TYPE(11L),
//web contents
    HOME_PAGE_WEB_CONTENT(1L),
    LEGAL_MENTIONS_WEB_CONTENT(2L);

    private final long id;


}
