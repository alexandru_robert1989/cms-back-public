package com.persistence.constant;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Class providing the default time delays in seconds for different methods
 * used within the API.
 */
@Component
public class DefaultTimeDelay {

    @Value("api.fixedDelay.in.milliseconds=10080000")
    public static long defaultJwtValidityMs;


}
