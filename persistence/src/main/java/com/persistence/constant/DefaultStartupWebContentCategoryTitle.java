package com.persistence.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DefaultStartupWebContentCategoryTitle {

    HISTORY("Histoire", "History"),
    SCIENCE("Science", "Science"),
    MEDICINE("Médecine", "Medicine"),
    CULTURE("Culture", "Culture");

    private final String frenchTitle;
    private final String englishTitle;

}
