package com.persistence.constant;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum StartupPageTemplateName {


    LEGAL_MENTIONS("legalMentions"),
    HOME_PAGE("homePage"),
    DEFAULT_ARTICLE("defaultArticle"),
    DEFAULT_REFERENCE("defaultReference");

    private final String name;
}
