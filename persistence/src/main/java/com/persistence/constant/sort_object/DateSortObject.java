package com.persistence.constant.sort_object;


import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

/**
 * Class providing the default {@link org.springframework.data.domain.Sort} objects used with
 * {@link org.springframework.data.jpa.repository.JpaRepository} GET methods.
 */
@Component
public class DateSortObject {


    /**
     * Sort by creationDate in chronological order.
     */
    public static final Sort ASC_CREATION_DATE = Sort.by("creationDate").ascending();


    /**
     * Sort by creationDate in reverse chronological order.
     */
    public static final Sort DESC_CREATION_DATE = Sort.by("creationDate").descending();


    /**
     * Sort by publishingDate in chronological order.
     */
    public static final Sort ASC_PUBLISHING_DATE = Sort.by("publishingDate").ascending();


    /**
     * Sort by publishingDate in reverse chronological order.
     */
    public static final Sort DESC_PUBLISHING_DATE = Sort.by("publishingDate").descending();


    /**
     * Sort by editionDate in chronological order.
     */
    public static final Sort ASC_EDITION_DATE = Sort.by("editionDate").ascending();


    /**
     * Sort by editionDate in reverse chronological order.
     */
    public static final Sort DESC_EDITION_DATE = Sort.by("editionDate").descending();


    /**
     * Sort by suppressionDate in chronological order.
     */
    public static final Sort ASC_SUPPRESSION_DATE = Sort.by("suppressionDate").ascending();


    /**
     * Sort by editionDate in reverse chronological order.
     */
    public static final Sort DESC_SUPPRESSION_DATE = Sort.by("suppressionDate").descending();


    /**
     * Sort by signUpDate in chronological order.
     */
    public static final Sort ASC_SIGNUP_DATE = Sort.by("signUpDate").ascending();


    /**
     * Sort by signUpDate in reverse chronological order.
     */
    public static final Sort DESC_SIGNUP_DATE = Sort.by("signUpDate").descending();


}
