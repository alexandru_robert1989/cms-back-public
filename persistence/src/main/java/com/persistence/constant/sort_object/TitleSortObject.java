package com.persistence.constant.sort_object;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component
public class TitleSortObject {

    /**
     * Sort by title in alphabetical order.
     */
    public static final Sort ASC_TITLE = Sort.by("title").ascending();


    /**
     * Sort by title in reverse alphabetical order.
     */
    public static final Sort DESC_TITLE = Sort.by("title").descending();
}
