# Spring Boot API 

This projects represents an attempt of creating a new improved version of the API used for my personal website :

https://www.psypec.fr/

Please note that the configuration data used for connecting to an email service and to a database are not provided
for this project. The .properties file of the web module contains fake connexion data, that you can replace with your own 
data. Inside the .properties file you will also need to modify the upload directories paths if you intend on using the upload functionality.

# Modules
## Domain

The domain module contains all the classes used as models for objects persisted in database.
Java classes are mapped to a database's table using the JPA (Java Persistence API) 
ORM (Object Relational Mapping) doctrine.

Additionally, the domain module contains also Data Transfer Objects (DTO), used 
for transferring data between the API and the user interfaces consuming the API.

## Email

The email module contains the logic needed for sending emails using Spring Boot Starter Mail.

## Persistence

The persistence module contains the logic needed for manipulating persisted data.
The module uses Spring Data JPA for manipulating data stored within a MariaDB database.


## Upload

The upload module provides the logic needed for uploading files locally. Using the upload module logic, 
one can store local images that can be later displayed by a user interface. 

## Web

The web module provides the REST Controllers needed for the API to communicate with a user interface. 


